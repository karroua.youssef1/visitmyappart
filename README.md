**Demo and walkthrough**

https://www.youtube.com/watch?v=CYE9_EFDJtM&ab_channel=Youssefkarroua

**VisitMyAppart** est une siteweb qui permet de :

- trouver des personnes (visiteurs) qui iront vister un appartement qui vous interesse , vous pouvez decrire exactement ce que vous voulez (photos , video , question a poser au proprietaire) .
- gagner de l'argent en visitant des appartement pour des clients .

** Architecture :**

![Alt text](./archi/architecture.PNG?raw=true "Architecture")


**Stack technlogiques :**

-Front : Angular

-Back: .Net 7

-BD : postgreSQL

Payment : Stripe SDK .

SignalR for real time notifications


**methode de deploiment : **

-Containerisation avec Docker 
-Cible --> AWS
