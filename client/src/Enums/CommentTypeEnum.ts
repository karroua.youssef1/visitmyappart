

export enum CommentTypeEnum {
    Neutral,
    OfferAccepted,
    OfferRejected,
    ReportRejected,
    ReportAccepted
  }