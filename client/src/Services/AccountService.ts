import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { User } from '../Models/User';
import { Response } from 'src/Models/Response';
import { ConnexionModel } from '../Models/ConnexionModel';
import { ProfilModel } from '../Models/Profil/ProfilModel';
import { ReplaySubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PresenceService } from './PresenceService';
import { Helper } from 'src/Helpers/Helper';
import { TokenService } from './TokenService';
import { GeneralInfosModel } from 'src/Models/Profil/GeneralInfosModel';
import { PaymentService } from './PaymentService';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl;
  private currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();
  connected : Boolean;
  user:User;

  constructor(private tokenService:TokenService,private helper:Helper,
     private http: HttpClient, private presenceService :PresenceService) { }

  login(model: ConnexionModel) {
   return this.http.post(this.baseUrl + 'User/login', model).pipe(
      map((response: User) => {
        const user = response;
        if (user && user != null) {
          console.log('login',user);
          this.setCurrentUser(user);
          this.connected= true;
           this.presenceService.createHubConnection(user);
          this.presenceService.SetNotificationsForUser(+user.id);
          this.tokenService.setCurrentToken(user.token);
        }
        else
        {
          this.connected= false;
        }
        
      })
    )

  }

  register(model: User) {
    return this.http.post(this.baseUrl + 'User/register', model).pipe(
      map((isRegisterd: boolean) => {
        if (isRegisterd) {
          console.log(model);
          console.log(isRegisterd);
        }
        else
        {

        }
      })
    )
  }
 async GetUserById(id:number)
  {
    return await this.http.get<Response<User>>(`${this.baseUrl}User/GetUserById/${id}`).toPromise();

  }
  addRegionAndDescription(model:ProfilModel,pic : File)
  { 
     const formData = new FormData();
     if(!this.helper.isObjectEmptyOrNull(pic))
     {
      formData.append('file', pic, pic.name);
     }
    formData.append('AddDescriptionAndRegionCommand',JSON.stringify(model));
    console.log(this.tokenService.getCurrentToken());
    return this.http.post(this.baseUrl + 'User/AddDesc', formData).pipe(
      map((response: User) => {
        console.log(response);
        if (!this.helper.isObjectEmptyOrNull(response)) {
          this.setCurrentUser(response);
        }
        else
        {
          console.log("error ",response);   
        
        }
        
      })
    )
  }
  async EditGeneralInfos(model :GeneralInfosModel)
  {
    return await this.http.post(this.baseUrl + 'User/EditGeneralInfos', model).pipe(
      map((response: User) => {
        console.log(response);
        if (!this.helper.isObjectEmptyOrNull(response)) {
          this.setCurrentUser(response);
        }
        else
        {
          console.log("error ",response);   
        
        }
        
      }))
  }



  setCurrentUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }

  logout() {
    var myuser:User;
    this.currentUser$.subscribe(u=>myuser=u);
    this.presenceService.RemoveUserToOnlinePresence(+myuser.id);
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
   this.presenceService.stopHubConnection();
   this.tokenService.removeToken();
  }
  async GetHomeVisitors() :Promise<User[]>
  {
    return await this.http.get<User[]>(`${this.baseUrl}User/GetHomeVisitors`).toPromise();

  }

 
}
