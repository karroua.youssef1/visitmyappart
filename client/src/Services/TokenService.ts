import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Helper } from 'src/Helpers/Helper';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  baseUrl = environment.apiUrl;
  private currentTokenSource = new ReplaySubject<string>(1);
  currentToken$ = this.currentTokenSource.asObservable();
  connected : Boolean;

  constructor(private helper:Helper, private http: HttpClient) { }

  

  setCurrentToken(token: string) {
    // user.role = [];
    // const roles = user.role;
    // Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);
    localStorage.setItem("token", token);
    this.currentTokenSource.next(token);
  }

  removeToken()
  {
    localStorage.removeItem("token");
    this.currentTokenSource.next(null);

  }

  getCurrentToken():string
  {
 return localStorage.getItem("token");
  }
  
}
