import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Bit } from 'src/Models/Bit';
import { Response } from 'src/Models/Response';
import { Report } from 'src/Models/Report';
import { Helper } from 'src/Helpers/Helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BitService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient,private helper:Helper) { }

  CreateBit(bit : Bit){
    var offerId : number=0;
   return this.http.post(this.baseUrl + 'Bit/Create',bit);
  }

  RespondToBit(bit : Bit)
  {
    return this.http.put<Response<boolean>>(`${this.baseUrl}Bit/RespondToBit`,bit);

  }

  async GetBit(id:number)
  {
    console.log("debut ge Bit",id);
    console.log(`${this.baseUrl}Bit/GetBit/${id}`);
        return this.http.get<Bit>(`${this.baseUrl}Bit/GetBit/${id}` ).toPromise();

  }

  UpdateBit(bit : Bit)
  {
    return this.http.post(this.baseUrl + 'Bit/Update',bit);
  }

  GetBitsByUserId(id:number)
  {
    return this.http.get<Bit[]>(`${this.baseUrl}Bit/GetBitsByUserId/${id}` ).toPromise();

  }

  DeleteBit(id:number)
  {
    return this.http.delete<Response<boolean>>(`${this.baseUrl}Bit/Delete/${id}` ).toPromise();

  }

  GetAllBits()
  {
    return this.http.get<Bit[]>(`${this.baseUrl}Bit/GetAll`);

  }
  async GetBitsByOfferId(id:number)
  {
    return  this.http.get<Bit[]>(`${this.baseUrl}Bit/GetBitsByOffer/${id}` ).toPromise();

  }
   
    RejectBit(model:Bit)
  {
    return this.http.put<Response<boolean>>(`${this.baseUrl}Bit/RejectBit`,model);

  }

  AcceptBit(model:Bit)
  {
    return this.http.put<Response<boolean>>(`${this.baseUrl}Bit/AcceptBit`,model);

  }

  async AcceptReport(model:Bit)
  {
    return this.http.put<Response<boolean>>(`${this.baseUrl}Report/AcceptReport`,model);

  }

 async RejectReport(model:Bit)
  {
    return this.http.put<Response<boolean>>(`${this.baseUrl}Report/RejectReport`,model);

  }

  CreateReport(model:Report,files:File[])
  {
    const formData = new FormData();
    if(!this.helper.isObjectEmptyOrNull(files))
    {
      files.forEach(element => {
        formData.append('file', element, element.name);

      });
    }
   formData.append('ReportDto',JSON.stringify(model));
    return this.http.post<Response<boolean>>(`${this.baseUrl}Report/Create`,formData);

  }
}
