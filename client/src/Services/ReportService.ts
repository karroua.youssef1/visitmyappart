import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Helper } from 'src/Helpers/Helper';
import { ReportFile } from 'src/Models/Files/ReportFile';
import { Response } from 'src/Models/Response';


@Injectable({
  providedIn: 'root'
})
export class ReportService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient,private helper:Helper) { }


  async GetReportFilesByReportId(reportId:number)
  {
    return  this.http.get<Response<ReportFile[]>>(`${this.baseUrl}Report/DownloadReportFiles/${reportId}` ).toPromise();
  }

}
