import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Response } from 'src/Models/Response';
import { Helper } from 'src/Helpers/Helper';
import { Review } from 'src/Models/Review';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient,private helper:Helper) { }

  async SubmitReview(review : Review){
   return this.http.post<Response<number>>(this.baseUrl + 'Review/Create',review).toPromise();
  }

}
