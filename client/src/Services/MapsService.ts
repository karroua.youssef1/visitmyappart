import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map} from 'rxjs/operators';
import { User } from '../Models/User';
import {Region} from '../Models/Region'
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapsService {
  private currentUserSource = new ReplaySubject<User>(1);
  private currentRegionSource = new ReplaySubject<Region>(1);
  currentUser$ = this.currentUserSource.asObservable();
  currentRegion$ = this.currentRegionSource.asObservable();
  connected : Boolean;

  constructor() { }

  
  setCurrentUser(user: User) {
    // user.role = [];
    // const roles = user.role;
    // Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }
   setPreferedRegion(region : Region)
   {
  localStorage.setItem('region', JSON.stringify(region));
    this.currentRegionSource.next(region);
   }
   getPreferedRegion()
   {
     return this.currentRegion$;
   }

  removePreferedRegion() {
    localStorage.removeItem('region');
    this.currentRegionSource.next(null);
  }

  
}
