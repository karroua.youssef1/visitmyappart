import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReportFile } from 'src/Models/Files/ReportFile';
import { Response } from 'src/Models/Response';
import { PaymentInfos } from 'src/Models/Profil/Payment/PaymentInfos';
import {map} from 'rxjs/operators';
import { PaymentCredit } from 'src/Models/Profil/Payment/PaymentCredit';
import { User } from 'src/Models/User';
import { AccountService } from './AccountService';



@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  baseUrl = environment.apiUrl;


  constructor(private http: HttpClient ,private accountService:AccountService) { }


  async AddCustumer(PaymentInfos:PaymentInfos)
  {
    return await this.http.post(this.baseUrl + 'Payment/AddCustomer', PaymentInfos).pipe(
      map((response: Response<User>) => {
        console.log(response);
        if (response.isOk) {
          this.accountService.setCurrentUser(response.responseModel);
          console.log(response.message); 
          return true;  
        }
        else
        {
          console.log(response.message);  
          return false; 
        
        }
        
      }))  }

  async AddPayment(reportId:number)
  {
    return  this.http.get<Response<ReportFile[]>>(`${this.baseUrl}Payment/DownloadReportFiles/${reportId}` ).toPromise();
  }
  async GetCreditCardOfUser(Id:string)
  {
    return  this.http.get<Response<PaymentCredit>>(`${this.baseUrl}Payment/GetCard/${Id}` ).toPromise();
  }

  async GetRedirectUrl(Id:string)
  {
    return  this.http.get<Response<string>>(`${this.baseUrl}Payment/GetRedirectUrl/${Id}` ).toPromise();
  }
  async isUserEnrolledCorrectly(Id:string)
  {
    return this.http.get<Response<User>>(`${this.baseUrl}Payment/IsUserEnrolledCorrectly/${Id}`).pipe(
      map((response: Response<User>) => {
        console.log(response);
        if (response.isOk) {
          this.accountService.setCurrentUser(response.responseModel);
          console.log("Usr IsEnrolledCorrectly ");
          return true;
        }

        else {
          console.log("error ", response);
          return false;
        }

      })) 
  }

}
