import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Notif } from 'src/Models/Notification';
import { User } from '../Models/User';

@Injectable({
  providedIn: 'root'
})
export class PresenceService {
  hubUrl = environment.apiUrl;
  private hubConnection: HubConnection;
  private onlineUsersSource = new BehaviorSubject<string[]>([]);
  onlineUsers$ = this.onlineUsersSource.asObservable();
  private NotificationsSource = new BehaviorSubject<Notif[]>([]);
  notifications$ = this.NotificationsSource.asObservable();
  notifications : Notif[];

  constructor(private toastr: ToastrService, private router: Router,private http:HttpClient) { }

 async createHubConnection(user: User) {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(this.hubUrl + 'notify')
      .withAutomaticReconnect()
      .build()

    await this.hubConnection
      .start()
      .catch(error => console.log(error));

console.log(this.hubConnection);
    this.hubConnection.on('UserIsOnline', username => {
      this.onlineUsers$.pipe(take(1)).subscribe(usernames => {
        this.onlineUsersSource.next([...usernames, username])
      })
    })

    this.hubConnection.on('UserIsOffline', username => {
      this.onlineUsers$.pipe(take(1)).subscribe(usernames => {
        this.onlineUsersSource.next([...usernames.filter(x => x !== username)])
      })
    })

    this.hubConnection.on('BitSent', username => {
        this.onlineUsers$.pipe(take(1)).subscribe(usernames => {
          this.onlineUsersSource.next([...usernames.filter(x => x !== username)])
        })
      })

    this.hubConnection.on('GetOnlineUsers', (usernames: string[]) => {
      this.onlineUsersSource.next(usernames);
    })

    this.hubConnection.on('broadcastnotification', (notification:Notif) => {
      console.log(notification);
      this.notifications$.subscribe(notifs=>this.notifications=notifs);
      this.notifications.push(notification);
      this.SetNotifications(this.notifications);
      this.toastr.info(notification.titre,notification.message)
        .onTap
        .subscribe(() => this.router.navigateByUrl(notification.link));
    })


    this.hubConnection.on('broadcastnotificationToUser', (notification:Notif) => {
      console.log(notification);
     this.notifications$.pipe(take(1)).toPromise().then(response=>{
      console.log('response',response);
      if(response)
     {
        response.push(notification);
        this.SetNotifications(response);
     }}
      ); 
     
      console.log("titre",notification.titre);
      this.toastr.info(notification.titre,notification.message)
        .onTap
        .subscribe(() => this.router.navigateByUrl(notification.link));
    })
    this.AddUserToOnlinePresence(+user.id);

    
  }
  async SetNotificationsForUser(id:number)
  {  var response = await this.GetNotificationsByUserId(id).toPromise();
if(response)
{
  this.SetNotifications(response);
}
    }
  stopHubConnection() {
    this.hubConnection.stop().catch(error => console.log(error));
  }

  async AddUserToOnlinePresence(idUser:number)
  {
    return this.hubConnection.invoke('OnConnectedAsync', idUser)
    .catch(error => console.log(error));
  }

  async RemoveUserToOnlinePresence(idUser:number)
  {
    return this.hubConnection.invoke('OnDisconnectedAsync',  idUser)
    .catch(error => console.log(error));
  }

  async SendNotification(idUser : number,notif:Notif)
  {
    notif.receiverId=idUser;
    return this.hubConnection.invoke('BroadcastNotification', notif)
    .catch(error => console.log(error));
  }

  async SendNotificationToUser(idUser : number,notif:Notif)
  {    notif.receiverId=idUser;
    return this.hubConnection.invoke('BroadcastNotificationToUser', notif)
    .catch(error => console.log(error));
  }

  SetNotifications(notifs: Notif[]) {
    // user.role = [];
    // const roles = user.role;
    // Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);
    localStorage.setItem('notifs', JSON.stringify(notifs));
    this.NotificationsSource.next(notifs);
  }

  ClearNotifications()
  {   localStorage.removeItem('notifs');
  this.NotificationsSource.next(null);

  }

  SetNotificationsToRead(id:number)
  {
    return this.http.put(`${this.hubUrl}Notification/SetNotificationsToRead/`,id);
  }
   GetNotificationsByUserId(id :number):Observable<Notif[]>
  {console.log("yo");
       return   this.http.get<Notif[]>(`${this.hubUrl}Notification/GetNotificationsByUserId/${id}`);
  }

  async ChangeNotificationState(id:number)
  {

     const resultat= await  this.http.put(`${this.hubUrl}Notification/ChangeState/`,id).toPromise();
      return resultat;
  }
}
