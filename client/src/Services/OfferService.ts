import { Injectable, Predicate } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Offer } from 'src/Models/Offer';
import { Response } from 'src/Models/Response';


@Injectable({
  providedIn: 'root'
})
export class OfferService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  CreateOffer(offer : Offer){
    var offerId : number=0;
    console.log("create",offer);
   return this.http.post(this.baseUrl + 'Offer/Create',offer);
  }

  GetOffer(id:number)
  {
    console.log("debut ge toffer",id);
    console.log(`${this.baseUrl}Offer/GetOffer/${id}`);
        return this.http.get(`${this.baseUrl}Offer/GetOffer/${id}` );

  }


  async GetAllOffersFromMyregion(id:number) :Promise<Response<Offer[]>>
  {
    return await this.http.get<Response<Offer[]>>(`${this.baseUrl}Offer/GetMyRegion/{id}`).toPromise();

  }
  UpdateOffer(offer : Offer)
  {
    return this.http.post(this.baseUrl + 'Offer/Update',offer);
  }

  GetOffersByClientId(id:number)
  {
    return this.http.get(`${this.baseUrl}Offer/GetOffersByClientId/${id}` );

  }
  async GetHomeOffers() :Promise<Offer[]>
  {
    return await this.http.get<Offer[]>(`${this.baseUrl}Offer/GetHomeOffers`).toPromise();

  }
  GetAllOffers()
  {
    return this.http.get(`${this.baseUrl}Offer/GetAll`);

  }

  ApplyFilter<T>(predicate: (value: T, index: number, array: T[]) => unknown) : T[]
  {
   return null;
  }
  
}
