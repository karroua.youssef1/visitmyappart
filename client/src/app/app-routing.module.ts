import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from '../app/Components/Home/home.component';
import {LoginComponent} from '../app/Components/account/login.component';
import {ProfilComponent} from './Components/profil/profil.component';
import { TestComponent } from './Components/test/test/test.component';
import { NotificationsComponent } from './Components/notifications/notifications.component';
import { TimeLineComponent } from './Components/time-line/time-line.component';
import { VisitorCardComponent } from './Components/visitor-card/visitor-card.component';
import { RegisterComponent } from './Components/account/register.component';
import { PaymentInfosComponent } from './Components/profil/payment/payment-infos/payment-infos.component';
import { MyBitsComponent } from './Components/bits/MyBits/my-bits/my-bits.component';
import { PaymentReturnUrlComponent } from './Components/profil/payment/returnUrl/payment-return-url/payment-return-url.component';
import { PaymentRefreshUrlComponent } from './Components/profil/payment/refreshUrl/payment-refresh-url/payment-refresh-url.component';
import { HowItWorksComponent } from './Components/how-it-works/how-it-works.component';
import { OfferComponent } from './Components/OfferRelated/offer/offer.component';
import { OffersPageComponent } from './Components/OfferRelated/offers-page/offers-page.component';



const routes: Routes = [
  {path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profil', component: ProfilComponent},
  { path: 'profil/:filter', component: ProfilComponent},
  { path: 'test', component: TestComponent },
  { path: 'VisitorCard/:idVisitor', component: VisitorCardComponent},
  { path: 'Offer/Create', component: OfferComponent , data: {isEditProfil: '1'} },
  { path: 'Offer/View/:id', component: OfferComponent ,data: {isEditProfil: '0'} },
  { path: 'Offers/:filter', component: OffersPageComponent },
  { path: 'Notifications/All', component: NotificationsComponent },
  { path: 'TM/:idBit', component: TimeLineComponent},
  { path: 'home', component: HomeComponent },
  { path: 'payment', component: PaymentInfosComponent },
  { path: 'MyBits', component: MyBitsComponent },
  { path: 'Return', component: PaymentReturnUrlComponent },
  { path: 'Refresh', component: PaymentRefreshUrlComponent },
  { path: 'HowItWorks', component: HowItWorksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
