import { Component, OnInit, ViewChild, ElementRef, NgZone, Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { Helper } from 'src/Helpers/Helper';
import { Region } from 'src/Models/Region';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.css']
})
export class GoogleMapsComponent implements OnInit  {
 map : google.maps.Map;
 myradius : number;
 center:google.maps.LatLng;
 circle : google.maps.Circle;
 cityCircle:google.maps.Circle;
 mapProperties : any;
 user: User;
 @Output() RegionEvent = new EventEmitter<Region>();
 @Input()
 region : Region;
 @Input()
 isEdit : boolean;
 constructor(private helper:Helper,private accountService: AccountService ) { }

ngOnInit(): void {
  this.getUserInfos();

  console.log("init google maps",this.region);
if(this.helper.isObjectEmptyOrNull(this.region))
{
  this.center=new google.maps.LatLng(this.user.city.lat, this.user.city.lng);
  
this.myradius=Math.sqrt(100) * 300;
this.isEdit=true;
}
else
{
  this.center=new google.maps.LatLng(this.region.x, this.region.y);
this.myradius=this.region.radius;
this.isEdit=true;
}
this.mapProperties = {
  center: this.center,
  zoom: 10,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};
  const map = new google.maps.Map(document.getElementById("map") as HTMLElement,this.mapProperties);
  this.cityCircle = new google.maps.Circle({
    strokeColor: "#0000FF",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#0000FF",
    fillOpacity: 0.35,
    map,
    center: this.center,
    radius: this.myradius,
    draggable:true,
    editable:this.isEdit
  });
  this.cityCircle.addListener("center_changed",this.UpdateRegion.bind(this));
  this.cityCircle.addListener("radius_changed",this.UpdateRegion.bind(this));
  console.log(this.cityCircle);
this.map=map;

this.RegionEvent.emit({radius:this.cityCircle.getRadius(),x:this.cityCircle.getCenter().lat(),y:this.cityCircle.getCenter().lng()});


}
getUserInfos()
  {
    this.accountService.currentUser$.subscribe(u=>{this.user=u});
  }
UpdateRegion()
 {
  console.log("ok ok");
  console.log(this.cityCircle)
  console.log("radiuis",this.cityCircle.getRadius().toString());
  console.log(this.cityCircle.getCenter().lat());
  console.log(this.cityCircle.getCenter().lng());
  console.log("before emit")

  this.RegionEvent.emit({radius:this.cityCircle.getRadius(),x:this.cityCircle.getCenter().lat(),y:this.cityCircle.getCenter().lng()});
 }

}
