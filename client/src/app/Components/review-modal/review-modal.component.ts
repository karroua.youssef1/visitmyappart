import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Helper } from 'src/Helpers/Helper';
import { Response } from 'src/Models/Response';
import { Review } from 'src/Models/Review';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PresenceService } from 'src/Services/PresenceService';
import { ReviewService } from 'src/Services/ReviewService';

@Component({
  selector: 'app-review-modal',
  templateUrl: './review-modal.component.html',
  styleUrls: ['./review-modal.component.css']
})
export class ReviewModalComponent implements OnInit {
  reviewForm: UntypedFormGroup;
  title :string;
  buttonText :string;
  validationErrors: string[] = [];
  myTextarea:any;
  currentUser : User;
  idOffer :number;
  rating :number;
  comment:string;
  visitorId:number;
  userId:number;
  @Output() ReviewSubmitted = new EventEmitter<string>();
  constructor(private helper:Helper,private reviewService:ReviewService, private modalService : NgbModal ,public activeModal: NgbActiveModal, private accountService : AccountService,private formBuilder: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.intitializeForm();
  }

  intitializeForm()
  {
    this.reviewForm = this.formBuilder.group({
      Comment: [''] 
    })
  }
  InitializeVaidationErrors()
  {
    this.validationErrors=[];
  }
  get f() { return this.reviewForm.controls; }

  async SendReview()
  {
    this.InitializeVaidationErrors();
    let  r :Review = {comment:this.f.Comment.value,rating:this.rating,visitorId:this.visitorId,clientId:this.userId};
     let response :Response<number>= await this.reviewService.SubmitReview(r);
     if(response.isOk)
  {
    console.log("Review Sent Succefuly");
    this.helper.ResultPopUp("Review Sent","Thank you ,your review has been sent !",true);
    this.ReviewSubmitted.emit("ok");
    this.activeModal.close();
  }
  else
  {
    console.log("Review not sent succefuly");
    this.validationErrors.push(response.message);
  }
  }

  ChangeRating($event)
  {
this.rating=$event;
  }
}
