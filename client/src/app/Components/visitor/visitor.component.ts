import { Component, Input, OnInit } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';

@Component({
  selector: 'app-visitor',
  templateUrl: './visitor.component.html',
  styleUrls: ['./visitor.component.css']
})
export class VisitorComponent implements OnInit {
  @Input() Visitor :User;
   image :SafeUrl ='';


  constructor(private helper:Helper) { }

  ngOnInit(): void {
    this.getVisitorProfilPic();
  }
  ngOnChanges()
  {
    if(!this.helper.isObjectEmptyOrNull(this.Visitor?.profilPicture))
        {
          this.image = this.helper.GetImageSafeUrl(this.Visitor?.profilPicture.base64Bytes,this.Visitor.profilPicture.type);
          console.log(this.image);

        }
  }
  getVisitorProfilPic()
  {
    console.log(this.image);

  if(!this.helper.isObjectEmptyOrNull(this.Visitor?.profilPicture))
        {
          this.image = this.helper.GetImageSafeUrl(this.Visitor?.profilPicture.base64Bytes,this.Visitor.profilPicture.type);
          console.log(this.image);

        }
      }
  ShowAllReviews()
  {

  }

}
