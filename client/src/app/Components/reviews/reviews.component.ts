import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/Models/Review';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  @Input() reviews :Review [];
  constructor() { }

  ngOnInit(): void {
  }

}
