import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  user:User;
  isConnected:boolean;
  constructor(private helper:Helper,private router: Router,private accountService: AccountService ) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
    if(this.helper.isObjectEmptyOrNull(this.user))
    {
    this.isConnected=false;
    }
    else
    {
      this.isConnected=true;

    }
  }
  Register()
  {   

    this.router.navigateByUrl("/register");
  }
}
