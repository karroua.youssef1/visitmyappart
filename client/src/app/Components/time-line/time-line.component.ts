import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BitStateEnum } from 'src/Enums/BitStateEnum';
import { CommentTypeEnum } from 'src/Enums/CommentTypeEnum';
import { Helper } from 'src/Helpers/Helper';
import { Bit } from 'src/Models/Bit';
import { Comment } from 'src/Models/Comment';
import { Offer } from 'src/Models/Offer';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { BitService } from 'src/Services/BitService';
import { OfferService } from 'src/Services/OfferService';
import { Response } from 'src/Models/Response';
import { Notif } from 'src/Models/Notification';
import { ModalComponent } from '../modal/modal.component';
import { PresenceService } from 'src/Services/PresenceService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentModalComponent } from '../comment-modal/comment-modal.component';
import { ViewportScroller } from '@angular/common';
import { Report } from 'src/Models/Report';
import { ReportStateEnum } from 'src/Enums/ReportStateEnum';
import { ReportFile } from 'src/Models/Files/ReportFile';
import { OfferStateEnum } from 'src/Enums/OfferStateEnum';
import { environment } from 'src/environments/environment';
import { ReportService } from 'src/Services/ReportService';

@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.css']
})
export class TimeLineComponent implements OnInit,AfterViewInit {
  loadRaportComponent:any[] = [];
  idBit :number;
  bit : Bit;
  AllComments: Comment[];
  NeutralComments : Comment[];
  AcceptedComment : Comment;
  RejectedComment : Comment;
  FirstBitComment :Comment;
  ReportRejectedComment :Comment;
  bitState = BitStateEnum;
  contentReport :string;
  Offer:Offer;
  validationErrors: string[] = [];
  isAuthorized:boolean;
  currentUser:User;
  ReportFiles :any[] = [];
  BitReportFiles : ReportFile[];
  @ViewChild("buttons") buttons: ElementRef;
  @ViewChild("report") report: ElementRef;
  @ViewChild("files") files: ElementRef;
  @ViewChild("TM") TM: ElementRef;
  isClient :boolean;
  isVisitor:boolean;
  isOfferDone :boolean=false;
  isRapportAccepted : boolean=false;
  frontUrl = environment.frontUrl;
  ReportFilesExist : boolean=false;
  isRapportValid : boolean=false;
  @ViewChild("scrollMe")  myScrollContainer: ElementRef;
  offerCreatedString:string;







  constructor(private scroller: ViewportScroller,private reportService:ReportService,private notifService:PresenceService, private modalService : NgbModal , private router: Router,public helper:Helper, private bitService:BitService,private route: ActivatedRoute,private accountService:AccountService,private offerService:OfferService) { }

  async ngOnInit(): Promise<void> {
    this.GetCurrentUser();
    this.GetBitIdFromRoute();
    await this.GetBitInfos();
   this.isAuthorized=this.CheckifAuthorized() ;
   console.log("isAuthorized",this.isAuthorized);
   if(this.isAuthorized)
    {this.GetRoleOfUser();
    this.InitializeTimeLineComments();
    await this.GetOffer();
    

  }
  else
  {
    this.router.navigateByUrl('/home');
  }
//this.GetReportFiles();

  }

  ngAfterViewInit() {        
    this.scrollToBottom();        
} 
  async GetReportFiles()
   {
    if(!this.helper.isObjectEmptyOrNull(this.bit.report) )
    { let result :Response<ReportFile[]> =await this.reportService.GetReportFilesByReportId(this.bit.report.id);
      if(result.isOk)
      {
        console.log("Report Files are here");
        this.BitReportFiles=result.responseModel;

      }
      else
      {
        console.log("ReportFiles are not here");

      }
      console.log("report files from bit",this.BitReportFiles);
   }
  }

  async DownloadAll()
  {
    await this.GetReportFiles();
    this.BitReportFiles.forEach(element => {
      this.helper.DownloadFile(element.base64Bytes,element.type,element.name);
      console.log("size of file",element.size);
    });
  }

  GetRoleOfUser()
  {
    if(!this.helper.isObjectEmptyOrNull(this.currentUser))
    {
    if(+this.currentUser.id==this.bit.visitorId)
     {
      this.isVisitor=true;
      this.isClient=false;
     }
      else if(+this.currentUser.id==this.bit.clientId)
     {
       this.isVisitor=false;
       this.isClient=true;
       console.log("its a client");
     }
     else
     {
       this.isVisitor=false;
       this.isClient=false;
     }
   }
  }
  DownloadFile(i:number)
  {
    this.helper.DownloadFile(this.BitReportFiles[i].base64Bytes,this.BitReportFiles[i].type,this.BitReportFiles[i].name);
  }
 
   UpdateReportValidStatus()
   {
    if(this.ReportFiles.length>0 && this.contentReport !="")
    {
      this.isRapportValid=true;    }
    else
    {
      this.isRapportValid=false;  
    }
   }
   scrollToBottom()
  {
    this.helper.scrollToAHeight(document.body.scrollHeight,600);
    }

  addFiles($event)
  {
    this.prepareFilesList($event);
    this.UpdateReportValidStatus();
    this.scrollToBottom();
  }
  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      item.progress = 0;
      this.ReportFiles.push(item);
    }
    console.log("list des fichiers ",this.ReportFiles);
  
    this.uploadFilesSimulator(0);

  }

  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.ReportFiles.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.ReportFiles[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.ReportFiles[index].progress += 5;
          }
        }, 100);
      }
    }, 200);
  }

  CheckifAuthorized():boolean
  {
    if(!this.helper.isObjectEmptyOrNull(this.currentUser))
    {
      if(+this.currentUser.id===this.bit.clientId || +this.currentUser.id===this.bit.visitorId)
      {
        return true;
      }
      else
      {return false;}
    }
    else
    {
      return false;
    }

  }

  GetCurrentUser()
  {

   this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
 
  }
  GetBitIdFromRoute()
  {
    this.route.paramMap.subscribe(params => { 
      this.idBit = +params.get('idBit'); 
      console.log("debut timeline",this.idBit);
  });
  }
  ClearValidationErrors()
  {
    this.validationErrors=[];
  }
  async GetOffer()
  {
    await this.offerService.GetOffer(this.bit.offreId).toPromise().then(
      (response :Offer) => {                          
        console.log('response received');
        this.Offer = response;
        this.offerCreatedString=this.Offer.dateCreated.toString(); 
        console.log(this.Offer);
        if(this.Offer.state==OfferStateEnum.Done)
        {
          this.isOfferDone=true;
          console.log("the offer is done");
        }
      },
      (error) => {                              
        console.error('Request failed with error')
        this.validationErrors.push(error);

      })

  }
  async GetBitInfos()
  {
 this.bit =await this.bitService.GetBit(this.idBit);
 if(!this.helper.isObjectEmptyOrNull(this.bit.report) )
    {
    if(this.bit.report.state==ReportStateEnum.Accepted)
        {
          this.isRapportAccepted=true;
        }
        this.ReportFilesExist=this.bit.report.reportFilesExist;
    }
 
  console.log("ma bit ",this.bit);
  }

  InitializeTimeLineComments()
  {
  this.GetAllComments();
  this.GetNeutralComments();
  this.GetAcceptedComment();
  this.GetRejectedComment();
  this.GetFirstBitComment();
  this.GetReportRejectedComment();
  }
  GetReportRejectedComment()
  {
    this.ReportRejectedComment=this.AllComments.find(x=>x.type==CommentTypeEnum.ReportRejected);

  }

  GetAllComments()
  {
    console.log("biiiit",this.bit);
    this.AllComments=this.bit.comments;
    console.log("all comments",this.AllComments);
  }

  GetNeutralComments()
  {
    this.NeutralComments=this.AllComments.filter(x=>x.type==CommentTypeEnum.Neutral);
    //remove the first comment
    this.NeutralComments.shift();  
    console.log("neutral",this.NeutralComments);
  }

  GetRejectedComment()
  {
    this.RejectedComment=this.AllComments.find(x=>x.type==CommentTypeEnum.OfferRejected);
  }

  GetAcceptedComment()
  {
    this.AcceptedComment=this.AllComments.find(x=>x.type==CommentTypeEnum.OfferAccepted);
  }

  GetFirstBitComment()
  {
   this.FirstBitComment=this.AllComments[0];
  }


  SaveComment(comment:string)
  {
this.ClearValidationErrors();
  }

  Reject()
  {

    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Reject the Bit";
    modalRef.componentInstance.buttonText="Reject";
    modalRef.componentInstance.idOffer=this.Offer.id;
    modalRef.componentInstance.bit=this.bit;

    modalRef.componentInstance.BitRejected.subscribe((receivedEntry) => {
     this.ngOnInit();     
      })
    }
    Accept()
  {

    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Accept the Bit";
    modalRef.componentInstance.buttonText="Accept";
    modalRef.componentInstance.idOffer=this.Offer.id;
    modalRef.componentInstance.bit=this.bit;
    modalRef.componentInstance.BitAccepted.subscribe((receivedEntry) => {
      this.ngOnInit();     
       })
  }

  Respond()
  {
   
    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Respond to the Bit";
    modalRef.componentInstance.buttonText="Respond";
    modalRef.componentInstance.idOffer=this.Offer.id;
    modalRef.componentInstance.bit=this.bit;

    modalRef.componentInstance.BitRespondedTo.subscribe((receivedEntry) => {
      this.ngOnInit();     
       })
 this.scrollToBottom();
  }

  AddReport()
  {    

    this.loadRaportComponent.push(1);
    this.buttons.nativeElement.style.display="none";

 }
 
 CancelReport()
 {
   this.loadRaportComponent=[];
   this.buttons.nativeElement.style.display="block";


}

ChangeContent($event)
{
  this.contentReport=$event;
  console.log("change content",this.contentReport);
  this.UpdateReportValidStatus();
}

SaveReport()
{   
  let report :Report = {bitId:this.bit.id,content:this.contentReport,bit:this.bit,state:ReportStateEnum.Open,senderId:+this.currentUser.id};
console.log("content report",this.contentReport);
  this.bitService.CreateReport(report,this.ReportFiles).subscribe((response : Response<boolean>)  => {
    const isok : boolean =response.isOk;
    if(isok)
    {
      var notif = new Notif();
      notif=this.helper.CreateNotificationModel("A report has been created","Hey,A repord has been sent to you By the visitor","/TM/"+this.bit.id);
      this.notifService.SendNotificationToUser(this.bit.clientId,notif);
      console.log("Report Created")
      this.ResultPopUp("Report Created","The Report has been created",true);
      this.ngOnInit();

    }
    else
    {
      this.validationErrors.push(response.message);
      console.log("Report Not Created" ,response.message);
    }
    
  }, error => {
          this.validationErrors.push(error.error.message);
          console.log("Report Not Created",error);
  
  });


}
ResultPopUp(message:string,title:string,isOk:boolean)
{
  const modalRef = this.modalService.open(ModalComponent);
  modalRef.componentInstance.message=message;
  modalRef.componentInstance.title=title;
  modalRef.componentInstance.isError=!isOk;

}

async DeleteBit()
{
  let response :Response<boolean> =await this.bitService.DeleteBit(this.bit.id);
  if(response.isOk)
  {
    console.log("Bit deleted succefly");
    this.ResultPopUp("Bit Deleted","The Bit has been deleted",true);
    this.router.navigateByUrl("/Home");

  }
  else
  {
    console.log("Bit not deleted succefly");
    this.ResultPopUp("Bit not Deleted","The Bit has not been deleted correctly",false);
  }
}
  /**
   * Delete file from files list
   * @param index (File index)
   */
   deleteFile(index: number) {
    if (this.ReportFiles[index].progress < 100) {
      console.log("Upload in progress.");
      return;
    }
    this.ReportFiles.splice(index, 1);
    console.log("update files",this.ReportFiles);
    this.UpdateReportValidStatus();
  }
async AcceptReport()
{
(await this.bitService.AcceptReport(this.bit)).subscribe((response : Response<boolean>)  => {
  const isok : boolean =response.isOk;
  if(isok)
  {
    var notif = new Notif();
    notif=this.helper.CreateNotificationModel("Your Report has been Accepted","Hey,Your Report has been Accepted","/TM/"+this.bit.id);
    this.notifService.SendNotificationToUser(this.bit.visitorId,notif);
    console.log("Report Accepted")
    this.ResultPopUp("Report Accepted","The Report has been accepted",true);
    this.ngOnInit();

  }
  else
  {
    this.validationErrors.push(response.message);
    console.log("Report Not Accepted correctly" ,response.message);
  }
  
}, error => {
        this.validationErrors.push(error.error.message);
        console.log("Report Not Accepted correctly",error);

});
}
RejectReport()
{
  const modalRef = this.modalService.open(CommentModalComponent);
  modalRef.componentInstance.title="Reject the Report";
  modalRef.componentInstance.buttonText="RejectReport";
  modalRef.componentInstance.idOffer=this.Offer.id;
  modalRef.componentInstance.bit=this.bit;

  modalRef.componentInstance.ReportRejected.subscribe((receivedEntry) => {
   this.ResultPopUp("Report Rejected","The Report has been rejcted , we will review this and come back to you soon",false);
   this.ngOnInit();     
    })
}
}
