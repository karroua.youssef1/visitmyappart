import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  isConnected:boolean=false;
  isVisitor;
  user:User;

  constructor(private accountService:AccountService,private helper:Helper) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
    if(this.helper.isObjectEmptyOrNull(this.user))
    {
     this.isConnected=false;
    }
    else
    {
      this.isConnected=true;
      if(this.user.role.toLowerCase()=="visitor")
      {
      this.isVisitor=true;
      }
      else
      {
        this.isVisitor=false;
      }
    }
    AOS.init();
  }

}
