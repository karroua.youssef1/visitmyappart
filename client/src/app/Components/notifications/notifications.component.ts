import { Component, OnInit } from '@angular/core';
import { Notif } from 'src/Models/Notification';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PresenceService } from 'src/Services/PresenceService';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  notifications : Notif[];
  user: User;
  constructor(private accountService:AccountService,private notificationService:PresenceService) { }

  ngOnInit(): void {
    this.GetUserInfos();
    this.GetAllNotifications();
  }
  
  GetUserInfos()
  {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
  }

  GetAllNotifications()
  {
    this.notificationService.notifications$.subscribe(n=>this.notifications=n);
  }

}
