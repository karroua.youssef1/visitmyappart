import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/Services/AccountService';
import { User } from 'src/Models/User';
import { Helper } from 'src/Helpers/Helper';
import { Offer } from 'src/Models/Offer';
import { OfferService } from 'src/Services/OfferService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  registerMode = false;
  registerOk = false;
  user:User =new User();
  homeOffers:Offer[];
  homeVisitors :User[];
  isConnected:boolean;

  constructor(private helper:Helper,private accountService: AccountService,private offerService:OfferService) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
    console.log(this.user);
    if(this.helper.isObjectEmptyOrNull(this.user))
    {
    this.isConnected=false;
    }
    else
    {
      this.isConnected=true;

    }
    this.BuildHome();
  }

  async BuildHome()
  {
    await this.getHomeOffers();
    await this.getHomeVisitors();
  }
  async getHomeOffers()
  {
    let homeOffers :Offer[] =await this.offerService.GetHomeOffers();
    this.homeOffers=homeOffers;
    console.log(this.homeOffers);
   }

   async getHomeVisitors()
   {
    let visitors :User[]= await this.accountService.GetHomeVisitors();
    if(!this.helper.isObjectEmptyOrNull(visitors))
    {
     this.homeVisitors=visitors;
    }
    else
    {

    }
console.log(this.homeVisitors);
   }


   OffersByDate()
   {
    if(this.homeOffers.length<3)
    {
      this.homeOffers=this.homeOffers.sort((objA, objB) => new Date(objB.dateCreated).getTime() - new Date(objA.dateCreated).getTime());

    }
    else
    {
     this.homeOffers=this.homeOffers.sort((objA, objB) => new Date(objB.dateCreated).getTime() - new Date(objA.dateCreated).getTime()).slice(0,3);
    }
   }

  registerToggle() {
    this.registerMode = !this.registerMode;
    console.log(this.registerMode);
  }
  setRegisterState( state : boolean)
  {

    this.registerOk=state;
  }
  cancelRegisterMode(event: boolean) {
    this.registerMode = event;
  }

}
