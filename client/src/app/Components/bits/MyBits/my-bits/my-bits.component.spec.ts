import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyBitsComponent } from './my-bits.component';

describe('MyBitsComponent', () => {
  let component: MyBitsComponent;
  let fixture: ComponentFixture<MyBitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyBitsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyBitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
