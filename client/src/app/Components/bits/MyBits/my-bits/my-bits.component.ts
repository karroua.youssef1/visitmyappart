import { Component, OnInit } from '@angular/core';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';

@Component({
  selector: 'app-my-bits',
  templateUrl: './my-bits.component.html',
  styleUrls: ['./my-bits.component.css']
})
export class MyBitsComponent implements OnInit {
  currentUser : User;

  constructor(private accountService:AccountService) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
  }

}
