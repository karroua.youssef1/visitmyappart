import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Helper } from 'src/Helpers/Helper';
import { Bit } from 'src/Models/Bit';
import { BitService } from 'src/Services/BitService';
import { BitStateEnum } from 'src/Enums/BitStateEnum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentModalComponent } from '../comment-modal/comment-modal.component';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';

@Component({
  selector: 'app-bits',
  templateUrl: './bits.component.html',
  styleUrls: ['./bits.component.css']
})
export class BitsComponent implements OnInit {
  frontUrl = environment.frontUrl;
  bits:Bit[];
  ShowBits : Bit[];
  @Input()
  idOffer :number;
  @Output() HasABitInHere = new EventEmitter<boolean>();
  @Output() BitsCount = new EventEmitter<number>();
  @Input()
  inputBits :Bit[];
  @Input()
  idUser :number;

  @Input() isClient :boolean=true;

  currentUser:User;

  constructor(private router: Router,private accountService:AccountService,private bitService:BitService , public helper:Helper,private modalService : NgbModal) { }

  async ngOnInit() {
    this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
    console.log(this.idUser);

    if(!this.helper.isObjectEmptyOrNull(this.inputBits))
    {
     this.bits=this.inputBits;
     console.log("its inside");
    }
    else if(!this.helper.isObjectEmptyOrNull(this.idOffer))
    {
     await  this.GetBitsByOfferId();
    }
    else if(!this.helper.isObjectEmptyOrNull(this.idUser))
    {
      await this.GetBitsByUserId();
      this.isClient=false;
      this.ShowBits=this.bits;
      return;
    }
    this.UpdateOpenBits(); 
    console.log("show bits",this.ShowBits);
  }

  async GetBitsByOfferId()
  {
    this.bits=await this.bitService.GetBitsByOfferId(this.idOffer);
      console.log("bits come one",this.bits);

  }

  async GetBitsByUserId()
  {
    this.bits=await this.bitService.GetBitsByUserId(this.idUser);
    console.log('bits',JSON.stringify(this.bits));

    
  }

  Reject(model:Bit)
  {

    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Reject the Bit";
    modalRef.componentInstance.buttonText="Reject";
    modalRef.componentInstance.idOffer=this.idOffer;
    modalRef.componentInstance.bit=model;

    modalRef.componentInstance.BitRejected.subscribe((receivedEntry) => {
     this.ngOnInit();     
      })

  }


  
  Accept(model:Bit)
  { 
    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Accept the Bit";
    modalRef.componentInstance.buttonText="Accept";
    modalRef.componentInstance.idOffer=this.idOffer;
    modalRef.componentInstance.bit=model;
    modalRef.componentInstance.BitAccepted.subscribe((receivedEntry) => {
      this.ngOnInit();     
       })
  }
GetStateName(model:Bit) :string
{

  return BitStateEnum[model.state];
}
  Respond(model:Bit)
  {

    const modalRef = this.modalService.open(CommentModalComponent);
    modalRef.componentInstance.title="Respond to the Bit";
    modalRef.componentInstance.buttonText="Respond";
    modalRef.componentInstance.idOffer=this.idOffer;
    modalRef.componentInstance.bit=model;

    modalRef.componentInstance.BitRespondedTo.subscribe((receivedEntry) => {
      this.ngOnInit();    
      this.router.navigateByUrl('/TM/',receivedEntry); 
       })

  }
  RejectBitInLocalList(id:number)
  {
    let item = this.bits.find(item => item.id ==id);
    item.state=BitStateEnum.Rejected;
    let itemIndex = this.bits.findIndex(item => item.id ==id);
    this.bits[itemIndex] = item;
    this.UpdateOpenBits();

  }

  UpdateOpenBits()
  {

    this.ShowBits=this.bits.filter(x=>(Number)(x.state)===(Number)(BitStateEnum.Open));

  }


}
