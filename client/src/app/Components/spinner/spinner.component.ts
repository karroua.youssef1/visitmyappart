import { Component } from '@angular/core';
import { LoaderService } from 'src/Services/loader.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {
  isLoading:boolean;
  constructor(public loader: LoaderService) { 
  }
}