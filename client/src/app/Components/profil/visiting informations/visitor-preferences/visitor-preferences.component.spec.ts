import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorPreferencesComponent } from './visitor-preferences.component';

describe('VisitorPreferencesComponent', () => {
  let component: VisitorPreferencesComponent;
  let fixture: ComponentFixture<VisitorPreferencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitorPreferencesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VisitorPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
