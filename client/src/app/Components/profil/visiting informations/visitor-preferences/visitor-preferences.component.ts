import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Helper } from 'src/Helpers/Helper';
import { ProfilModel } from 'src/Models/Profil/ProfilModel';
import { Region } from 'src/Models/Region';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { MapsService } from 'src/Services/MapsService';
import { GoogleMapsComponent } from 'src/app/Components/Maps/google-maps/google-maps.component';

@Component({
  selector: 'app-visitor-preferences',
  templateUrl: './visitor-preferences.component.html',
  styleUrls: ['./visitor-preferences.component.css']
})
export class VisitorPreferencesComponent implements OnInit {
  @Input() user :User;
  VisitingPreferenceForm: UntypedFormGroup; 
  isEdit : boolean=false;
  validationErrors: string[] = [];
  profilPicInput : File;
  image :SafeUrl ='';
  region:Region;
  myTextarea:any;
  @ViewChild('region') maps: GoogleMapsComponent;




  constructor(private helper : Helper,private sanitizer: DomSanitizer,private accountService: AccountService,private mapsService :MapsService ,private router: Router,private formBuilder: UntypedFormBuilder ) { }

  ngOnInit(): void {
    if(!this.helper.isObjectEmptyOrNull(this.user.profilPicture))
{
  this.image = this.helper.GetImageSafeUrl(this.user.profilPicture.base64Bytes,this.user.profilPicture.type);
  console.log(this.image);

}
console.log('image',this.image);

this.VisitingPreferenceForm = this.formBuilder.group({
  Description: [this.user.description != "" ? this.user.description : "", Validators.required]});
  }
  
  Edit()
  {
    this.isEdit=true;
  }
  get GetControls() { return this.VisitingPreferenceForm.controls; }
  UpdateRegion(newValue: Region)
  {
    console.log("new value",newValue);
     this.region=newValue;
  }

  editFile(files : File[])
  {
console.log("file upload");
console.log(files);
this.profilPicInput=files[0];
const reader = new FileReader();
const preview = document.querySelector('img');
  reader.addEventListener("load", function assignImageSrc(evt) {
    // convert image file to base64 string
    preview.src = <string>evt.target.result;
    this.removeEventListener("load", assignImageSrc);
  }, false);

  if (this.profilPicInput) {
    reader.readAsDataURL(this.profilPicInput);
  }
  }
  saveVisitingPreferences()
  {  
    // this.mapsService.getPreferedRegion().subscribe(r=>this.region=r);
    const model : ProfilModel = {id: Number(this.user.id),description:this.GetControls.Description.value,region:this.region};
    console.log(model);
    this.accountService.addRegionAndDescription(model,this.profilPicInput).subscribe(response => {
      this.router.navigateByUrl('Offers/All');
      console.log("region and description OK");
      this.helper.ResultPopUp("Informations saved","Thank you",true);
      
    }, error => {
            this.validationErrors = error;
            console.log("region and description KO",error);
            this.helper.ResultPopUp("An error has occured , please trye again","Error",false);
    });
    
  }

  Cancel()
  {
this.isEdit=false;
 }


}
