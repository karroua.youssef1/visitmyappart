import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Helper } from 'src/Helpers/Helper';
import { City } from 'src/Models/City';
import { Country } from 'src/Models/Country';
import { GeneralInfosModel } from 'src/Models/Profil/GeneralInfosModel';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { AdressInputComponent } from 'src/app/Components/forms/adress-input/adress-input.component';

@Component({
  selector: 'app-general-infos',
  templateUrl: './general-infos.component.html',
  styleUrls: ['./general-infos.component.css']
})
export class GeneralInfosComponent implements OnInit {

  @Input() user :User;
  GeneralForm: UntypedFormGroup; 
  isEdit : boolean=false;
  validationErrors: string[] = [];
  selectedCountry:Country=new Country();
  selectedCity:City=new City();
  @ViewChild('city') cityComponent: AdressInputComponent;


  constructor(private helper : Helper,private accountService: AccountService ,private router: Router,private formBuilder: UntypedFormBuilder) {
  
   }

  ngOnInit(): void {
    console.log(this.user);
    this.GeneralForm = this.formBuilder.group({
      City: [this.user.city != null ? this.user.city.name : "", Validators.required],
      Country: [this.user.country != "" ? this.user.country : "", Validators.required]
    });
  }
  SetCountry(event :  google.maps.places.PlaceResult)
  {
    this.selectedCountry.long_name= event.address_components[0].long_name;
    this.selectedCountry.short_name= event.address_components[0].short_name;
    console.log("Country ",this.selectedCountry);
    this.cityComponent.country=this.selectedCountry;
    this.cityComponent.ngOnInit();
    this.cityComponent.ngAfterViewInit();
  }
   
  SetCity(event :  google.maps.places.PlaceResult)
  {
    this.selectedCity.name= event.address_components[0].long_name;
    this.selectedCity.lng= event.geometry.location.lng()
    this.selectedCity.lat= event.geometry.location.lat();

    console.log("City",this.selectedCity);
  }
  Edit()
  {
    this.isEdit=true;
  }
  get GetControls() { return this.GeneralForm.controls; }
  Cancel()
  {
  this.isEdit=false;
  }
  async SaveGeneralInfos()
  {  
    const model : GeneralInfosModel = {id: Number(this.user.id),city:this.selectedCity,country:this.selectedCountry.long_name};
    (await this.accountService.EditGeneralInfos(model)).subscribe(response => {
      this.router.navigateByUrl('/profil');
      console.log("General Infos OK");
      this.isEdit=false;
      this.helper.ResultPopUp("Information Saved !","Thank you",true);
      
    }, error => {
            this.validationErrors = error;
            console.log("General Infos KO",error);
            this.helper.ResultPopUp("An error has occured , please try again","Error",false);
    });
    
  }

}
