import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { Helper } from 'src/Helpers/Helper';




@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  
  filter : string ;
 navs = {
  'v-pills-general-tab':'v-pills-general',
  'v-pills-profile-tab':'v-pills-profile',
  'v-pills-payment-tab':'v-pills-payment'
};
   user :User;
  constructor(private helper : Helper,private route: ActivatedRoute,private accountService: AccountService,private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => { 
      this.filter = params.get('filter'); 
  });
  this.Redirect(this.filter);
this.getUserInfos();
if( this.helper.isObjectEmptyOrNull(this.user))
{
this.router.navigateByUrl("/home");
}
console.log("profil",this.user);

}

Redirect(filter:string)
{
  if(filter==null)
  {return;}
  else if(filter.toLowerCase()=="payment")
  {this.ShowContent('v-pills-payment-tab');}
  else if (filter.toLowerCase()=="visit")
  {this.ShowContent('v-pills-profile-tab');}

}
  getUserInfos()
  {
    this.accountService.currentUser$.subscribe(u=>{this.user=u});
  }

  CreateOffer()
  {
    this.router.navigateByUrl('/Offer/Create');

  }
  ShowContent(idHeader:string)
  {
    this.ShowNav(idHeader);
    this.ShowNavContent(this.navs[idHeader]);
 
   Object.keys(this.navs).forEach(element => {
    console.log("element"+element);
      if(element!=idHeader)
      { this.HideNav(element);
        this.HideNavContent(this.navs[element]);
      }
    });
  }
  HideNav(id:string)
  {
    let element =document.getElementById(id);
    if(element!=null) element.classList.remove("active");
  }
  ShowNav(id:string)
  {
    let element =document.getElementById(id);
    if(element!=null) element.classList.add("active");
  }
  HideNavContent(id:string)
  {
    let element =document.getElementById(id);
    if(element!=null) element.classList.remove("show","active");
  }
  ShowNavContent(id:string)
  {
    let element =document.getElementById(id);
    if(element!=null) element.classList.add("show","active");
  }

 



}
