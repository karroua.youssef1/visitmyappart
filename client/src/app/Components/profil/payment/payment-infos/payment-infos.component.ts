import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import {  UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PaymentCredit } from 'src/Models/Profil/Payment/PaymentCredit';
import { PaymentInfos } from 'src/Models/Profil/Payment/PaymentInfos';
import { User } from 'src/Models/User';
import { Response } from 'src/Models/Response';
import { PaymentService } from 'src/Services/PaymentService';
import { Helper } from 'src/Helpers/Helper';

@Component({
  selector: 'app-payment-infos',
  templateUrl: './payment-infos.component.html',
  styleUrls: ['./payment-infos.component.css']
})
export class PaymentInfosComponent implements OnInit {
  paymentCard :PaymentCredit = new PaymentCredit();
  @Input() user :User;
  CardForm: UntypedFormGroup;
  maxDate: Date;
  validationErrors: string[] = [];
  number :string="yo" ;
  name :string="yooo" ;
  cvc :string ="yoooooo";
  expiry :string=" " ;
  isEdit : boolean=false;
  isVisitor:boolean=false;
  @ViewChild('EditOrAddButton') EditOrAddButton :ElementRef;



  constructor(private paymentService: PaymentService,
    private fb: UntypedFormBuilder, private router: Router,private helper:Helper) { }
    
  ngOnInit(): void {
this.getInfos();
this.intitializeForm();
if(this.user.role.toLowerCase()=="visitor") this.isVisitor=true;

  }
  intitializeForm() {
    this.CardForm = this.fb.group({
      number: [this.paymentCard.cardNumber, [Validators.minLength(16), Validators.maxLength(16),Validators.required]],
      name: [this.paymentCard.name, Validators.required],
      expiry: ['', [Validators.minLength(4), Validators.maxLength(4),Validators.required]],
      cvc: [this.paymentCard.cvc,[Validators.minLength(3), Validators.maxLength(3),Validators.required]],
    })
  }

  handleInputChange(e:any){
   console.log("yo");
        switch(e.target.name) { 
          case "name": { 
            this.paymentCard.name=e.target.value;
            break; 
          } 
          case "number": { 
            this.paymentCard.cardNumber=e.target.value;
             break; 
          } 
          case "cvc": { 
            this.paymentCard.cvc=e.target.value;
             break; 
          } 
          case "expiry": { 
            this.paymentCard.expiry=e.target.value;
             break; 
          } 
          default: { 
             //statements; 
             break; 
          } 
       } 
  }
   // convenience getter for easy access to form fields
    getValueOfForm(index:string) { return this.CardForm.controls[index].value; }

  Cancel() {
        this.getInfos();
    this.isEdit=false;

  }
  async RedirectToPaymentConfiguration()
  {
    let response :Response<string> = await this.paymentService.GetRedirectUrl(this.user.id);
   console.log(response);
    if(response.isOk)
    window.open(response.responseModel, '_blank');
 else
 {
  
 }
  }
  Edit()
  {
    this.isEdit=true;
  }
 async SavePaymentInfos()
  {

    this.paymentCard.expirationMonth=this.paymentCard.expiry.slice(0,2);
    this.paymentCard.expirationYear=this.paymentCard.expiry.slice(2,4);
    const model : PaymentInfos = {Id:this.user.id,Name: this.user.name,Email:this.user.email,CreditCard:this.paymentCard};
    console.log(model);
    (await this.paymentService.AddCustumer(model)).subscribe(response => {
      if(response)
      {
      this.router.navigateByUrl('/profil');
      console.log("payment infos OK");
      this.isEdit=false;
      this.helper.ResultPopUp("Information Saved !","Thank you",true);
    }
    else
    {
      console.log("payment infos KO");
      this.helper.ResultPopUp("An error has occured , please try again !","Error",false);
    }
      
    }, error => {
            this.validationErrors = error;
            console.log("payment infos KO",error);
            this.helper.ResultPopUp("An error has occured , please try again !","Error",false);
    }); 
  
  }

    async getInfos()
    {
      let result :Response<PaymentCredit> = await this.paymentService.GetCreditCardOfUser(this.user.id);
      if(result.isOk)
      {
           
            this.paymentCard=result.responseModel;
            this.paymentCard.expiry=this.paymentCard.expirationMonth+this.paymentCard.expirationYear;
            this.isEdit=false;
      }
      else
      {

          this.EditOrAddButton.nativeElement.innerText='Add';
            this.paymentCard=new PaymentCredit();
      }

    }

}
