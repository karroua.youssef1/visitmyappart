import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PaymentService } from 'src/Services/PaymentService';



@Component({
  selector: 'app-payment-return-url',
  templateUrl: './payment-return-url.component.html',
  styleUrls: ['./payment-return-url.component.css']
})
export class PaymentReturnUrlComponent implements OnInit {
currentUser : User;
  constructor(private router: Router,private paymentService :PaymentService , private accountService : AccountService,private helper:Helper) { }

  async ngOnInit(): Promise<void> {

    this.GetCurrentUserInfo();
    await this.IsEnrolledCorrectly();

  }
  GetCurrentUserInfo()
  {
    this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
  }
  async IsEnrolledCorrectly()
  {
     (await this.paymentService.isUserEnrolledCorrectly(this.currentUser.id)).subscribe(response=> {
if(response)
      this.helper.ResultPopUp("thank you","thank you",true);
      else
      {
        this.helper.ResultPopUp("Oups","it seems that you didn't finish your stripe payment configuration",false,"profil/payment","click here to finish your payment informations");
      }
      
    });
  }


}
