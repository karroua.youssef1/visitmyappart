import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentReturnUrlComponent } from './payment-return-url.component';

describe('PaymentReturnUrlComponent', () => {
  let component: PaymentReturnUrlComponent;
  let fixture: ComponentFixture<PaymentReturnUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentReturnUrlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaymentReturnUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
