import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentRefreshUrlComponent } from './payment-refresh-url.component';

describe('PaymentRefreshUrlComponent', () => {
  let component: PaymentRefreshUrlComponent;
  let fixture: ComponentFixture<PaymentRefreshUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentRefreshUrlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaymentRefreshUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
