import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PaymentService } from 'src/Services/PaymentService';

@Component({
  selector: 'app-payment-refresh-url',
  templateUrl: './payment-refresh-url.component.html',
  styleUrls: ['./payment-refresh-url.component.css']
})
export class PaymentRefreshUrlComponent implements OnInit {
  currentUser : User;

  constructor(private router: Router,private paymentService :PaymentService , private helper:Helper, private accountService : AccountService) { }

  ngOnInit(): void {
    this.GetCurrentUserInfo()
    this.helper.ResultPopUp("Oups","it seems that you didn't finish your payment configuration",false,"url","do it again ?");

  }
  GetCurrentUserInfo()
  {
    this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
  }

}
