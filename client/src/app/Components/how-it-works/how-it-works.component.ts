import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.css']
})
export class HowItWorksComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit():void {
    document.getElementById('client').hidden=false;
    document.getElementById('visitor').hidden=true;
  }

  Show(id:string)
  {
    let element : HTMLElement = document.getElementById(id);
    element.hidden=!element.hidden;
  
  }

}
