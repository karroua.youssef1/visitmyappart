import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/Models/User';

@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.component.html',
  styleUrls: ['./visitors.component.css']
})
export class VisitorsComponent implements OnInit {

  @Input() visitors :User[];
  constructor() { }

  ngOnInit(): void {
  }

}
