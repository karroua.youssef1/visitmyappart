import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitModalComponent } from './bit-modal.component';

describe('BitComponent', () => {
  let component: BitModalComponent;
  let fixture: ComponentFixture<BitModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
