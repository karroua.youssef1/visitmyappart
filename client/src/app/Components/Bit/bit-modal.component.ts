import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Bit } from 'src/Models/Bit';
import { Notif } from 'src/Models/Notification';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { BitService } from 'src/Services/BitService';
import { PresenceService } from 'src/Services/PresenceService';
import { ModalComponent } from '../modal/modal.component';
import { Response } from 'src/Models/Response';
import { BitStateEnum } from 'src/Enums/BitStateEnum';
import { Helper } from 'src/Helpers/Helper';
import { Comment } from 'src/Models/Comment';
import { CommentTypeEnum } from 'src/Enums/CommentTypeEnum';


@Component({
  selector: 'app-bit',
  templateUrl: './bit.component.html',
  styleUrls: ['./bit.component.css']
})
export class BitModalComponent implements OnInit {

bit : Bit ;
currentUser : User
bitForm: UntypedFormGroup;
validationErrors: string[] = [];
@Output() BitCreated = new EventEmitter<boolean>();
myTextarea:any;
isEditBit:boolean;
idOffer :number;
idClient :number;
comms : Comment[];


  constructor(private helper:Helper,private notifService : PresenceService,private modalService : NgbModal ,public activeModal: NgbActiveModal, private accountService : AccountService,private formBuilder: UntypedFormBuilder,private bitService:BitService) { }

  ngOnInit(): void {
    this.getUserInfos();
    this.intitializeForm();
    console.log("id modal offer",this.idOffer);
    console.log("id modal client",this.idClient);
    this.initializeCommentsArray();


  }
initializeCommentsArray()
{
  this.comms=[];
}
  intitializeForm()
  {
    this.bitForm = this.formBuilder.group({
      Comment: ['', Validators.required] ,
      Id:  this.idOffer
    })
  }
  
  get f() { return this.bitForm.controls; }

  getUserInfos()
  {
    this.accountService.currentUser$.subscribe(u=>{this.currentUser=u});
  }
  ResultPopUp(message:string,title:string,isOk:boolean)
  {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.message=message;
    modalRef.componentInstance.title=title;
    modalRef.componentInstance.isError=!isOk;

  }
  SendBit()
  {  
    if(!this.CheckVisitingAndPaymentInfosAreComeplete()) return;
  
    this.validationErrors=[];
        this.initializeCommentsArray();
    let comm : Comment = this.CreateCommentModel(+this.currentUser.id,this.f.Comment.value,this.currentUser.name,this.idClient,CommentTypeEnum.Neutral);
this.comms.push(comm);
this.bit =  {visitorId :+this.currentUser.id,comments:this.comms,offreId:this.idOffer,clientId:this.idClient,state:BitStateEnum.Open};
this.bitService.CreateBit(this.bit).subscribe((response : Response<boolean>)  => {
  const isok : boolean =response.isOk;
  console.log("response",response);
  if(isok)
  { var notif = new Notif();
    notif=this.CreateNotificationModel("2 someone submited a bit for your offer","Hey , someone submited a bit go check it ","/Offer/View/"+this.idOffer);
    this.notifService.SendNotificationToUser(this.idClient,notif);
    this.activeModal.close();
    this.ResultPopUp("Bit Created","Your Bit has been Sent",isok)
    console.log("Bit Created")
    this.BitCreated.emit(true);

  }
  else
  {
    this.validationErrors.push(response.message);
    console.log("Bit Not Created" ,response.message);
  }
  
}, error => {
        this.validationErrors.push(error.error.message);
        console.log("Bit Not Updated",error);

});
  }

  CreateNotificationModel(titre:string,message:string,link:string):Notif
  {
    return this.helper.CreateNotificationModel(titre,message,link);
  }
  CreateCommentModel(senderId:number,content:string,senderName:string,ReceiverId:number,type:CommentTypeEnum,ReceiveName?:string):Comment
  {
   return this.helper.CreateCommentModel(senderId,content,senderName,ReceiverId,type,ReceiveName);
  }
  CheckVisitingAndPaymentInfosAreComeplete() :Boolean
  {
    if(!this.currentUser.paymentInformationsCompleted)
    { let message :string="Complete your payment informations to be able to create your offero send bits";
      this.helper.ResultPopUp(message,"Visiting preferences",false,"profil/payment","Ici!");
      return false;
    }
    if(!this.currentUser.visitingPreferencesCompleted)
    { let message :string="Complete your visiting preferences informations to be able to send bits";
      this.helper.ResultPopUp(message,"Visiting preferences",false,"profil/visit","Ici!");
      return false;
    }
    return true;
  }

}