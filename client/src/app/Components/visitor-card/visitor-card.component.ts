import { Component, Input, OnInit } from '@angular/core';
import { Helper } from 'src/Helpers/Helper';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { Response } from 'src/Models/Response';
import { ActivatedRoute } from '@angular/router';
import { SafeUrl } from '@angular/platform-browser';
import { ReviewModalComponent } from '../review-modal/review-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-visitor-card',
  templateUrl: './visitor-card.component.html',
  styleUrls: ['./visitor-card.component.css']
})
export class VisitorCardComponent implements OnInit {

  Visitor:User;
  CurrentUser:User;
  @Input()
  idVisitor:number;
  isShowReviews:boolean=false;
  CanHeRate :boolean=true;


  constructor(private modalService : NgbModal,private route: ActivatedRoute,private accountService:AccountService,private helper:Helper) { }

  ngOnInit(): void {
    this.getCurrentUser();
  this.getIdVisitor();
    if(!this.helper.isObjectEmptyOrNull(this.idVisitor))
    {   
       this.GetVisitorInfos();
       console.log("he has already rated ? ",this.CanHeRate);

    }
    else
    {
      
    }
  }
  CanHeRateMethod():boolean
  {
    let result:boolean=true;
    if(+this.Visitor.id===+this.CurrentUser.id) result =false;
    if(!this.helper.isObjectEmptyOrNull(this.Visitor?.visitorReviews))
    {
      this.Visitor.visitorReviews.forEach(element => {
        if(+this.CurrentUser.id===element.clientId)
        {
          result= false;
        }
      });
    }
    return result;
  }
  getIdVisitor()
  {
    this.route.paramMap.subscribe(params => { 
      this.idVisitor = +params.get('idVisitor'); 
  });
  }
  ShowAllReviews()
  {
this.isShowReviews=!this.isShowReviews;
  }
  Rate()
  {

    const modalRef = this.modalService.open(ReviewModalComponent);
    modalRef.componentInstance.title="Rate the Visitor";
    modalRef.componentInstance.buttonText="Rate";
    modalRef.componentInstance.visitorId=this.Visitor.id;
    modalRef.componentInstance.userId=this.CurrentUser.id;
    modalRef.componentInstance.ReviewSubmitted.subscribe((receivedEntry) => {
     this.ngOnInit();     
      })

  }
  getCurrentUser()
  {
    this.accountService.currentUser$.subscribe(u=>this.CurrentUser=u);
  }
  async GetVisitorInfos()
  {
     let value : Response<User> = await this.accountService.GetUserById(this.idVisitor);
     if(value.isOk)
     {
      this.Visitor=value.responseModel;
        this.CanHeRate=this.CanHeRateMethod();
        console.log("ok",value);
      
     }
     else
     {
      console.log("ko" , value.message);

     }
  }

}
