import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeLowerContentComponent } from './home-lower-content.component';

describe('HomeLowerContentComponent', () => {
  let component: HomeLowerContentComponent;
  let fixture: ComponentFixture<HomeLowerContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeLowerContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeLowerContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
