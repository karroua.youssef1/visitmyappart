import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NgbModal,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from 'src/Services/AccountService';
import { BitService } from 'src/Services/BitService';
import { Response } from 'src/Models/Response';
import { Notif } from 'src/Models/Notification';
import { PresenceService } from 'src/Services/PresenceService';
import { Bit } from 'src/Models/Bit';
import { Comment } from 'src/Models/Comment';
import { Helper } from 'src/Helpers/Helper';
import { CommentTypeEnum } from 'src/Enums/CommentTypeEnum';
import { User } from 'src/Models/User';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.css']
})
export class CommentModalComponent implements OnInit {
  commentForm: UntypedFormGroup;
  title :string;
  buttonText :string;
  validationErrors: string[] = [];
  myTextarea:any;
  bit :Bit;
  currentUser : User;
  idOffer :number;
  @Output() BitRejected = new EventEmitter<number>();
  @Output() BitAccepted = new EventEmitter<number>();
  @Output() BitRespondedTo = new EventEmitter<number>();
  @Output() ReportRejected = new EventEmitter<number>();




  constructor(private helper:Helper,private bitSerVice:BitService,private notifService:PresenceService, private modalService : NgbModal ,public activeModal: NgbActiveModal, private accountService : AccountService,private formBuilder: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.getUserInfos();
    this.intitializeForm();
  }

  intitializeForm()
  {
    this.commentForm = this.formBuilder.group({
      Comment: [''] ,
      Id:  this.bit.id
    })
  }

  InitializeVaidationErrors()
  {
    this.validationErrors=[];
  }
  
  get f() { return this.commentForm.controls; }


  SendComment()
  {
    switch(this.buttonText) { 
      case "Accept": { 
        this.AcceptBit();
         //statements; 
         break; 
      } 
      case "Reject": { 
        this.RejectBit();
         //statements; 
         break; 
      } 
      case "Respond": { 
        this.RespondToBit();
        //statements; 
        break; 
       
     } 
     case "RejectReport": { 
      this.RejectReport();
      //statements; 
      break; 
     
   } 
      default: { 
         //statements; 
         break; 
      } 
   } 
  }

 async RejectReport()
  {
    let comm :Comment = this.CreateCommentModel(+this.currentUser.id,this.f.Comment.value,this.currentUser.name,this.bit.visitorId,CommentTypeEnum.ReportRejected);
    this.bit.comments.push(comm);
  (await this.bitSerVice.RejectReport(this.bit)).subscribe((response : Response<boolean>)  => {
    const isok : boolean =response.isOk;
    if(isok)
    {
      var notif = new Notif();
      notif=this.helper.CreateNotificationModel("The client rejected your report","Hey,The client rejected your report , our team will review the situation and come back to you ","/TM/"+this.bit.id);
      this.notifService.SendNotificationToUser(this.bit.visitorId,notif);
      console.log("Report Rejected")
      this.activeModal.close();
      this.ReportRejected.emit(this.bit.id);

    }
    else
    {
      this.validationErrors.push(response.message);
      console.log("Bit Not Rejected correctly" ,response.message);
    }
    
  }, error => {
          this.validationErrors.push(error.error.message);
          console.log("Bit Not Rejected correctly",error);
  
  });
  }
  RespondToBit()
  {

    this.InitializeVaidationErrors();
    let receiverId :number;
    if(+this.currentUser.id===this.bit.clientId)
    {
 receiverId=this.bit.visitorId;
    }
    else
    {
      receiverId=this.bit.clientId;

    }
    let comm :Comment = this.CreateCommentModel(+this.currentUser.id,this.f.Comment.value,this.currentUser.name,receiverId,CommentTypeEnum.Neutral);
    this.bit.comments.push(comm);
    this.bitSerVice.RespondToBit(this.bit).subscribe((response : Response<boolean>)  => {
      const isok : boolean =response.isOk;
      if(isok)
      {
        this.BitRespondedTo.emit(this.bit.id);
        var notif = new Notif();
        notif=this.helper.CreateNotificationModel("Somone sent you a message","Hey,Someone sent you a response","/Offer/View/"+this.idOffer);
        this.notifService.SendNotificationToUser(receiverId,notif);
        console.log("Bit Responded to")
        this.activeModal.close();
        this.ResultPopUp("Response Sent","The response has been sent",true);

      }
      else
      {
        this.validationErrors.push(response.message);
        console.log("Bit Not Responded to" ,response.message);
      }
      
    }, error => {
            this.validationErrors.push(error.error.message);
            console.log("Bit Not Responded to",error);
    
    });
  }
  getUserInfos()
  {
    this.accountService.currentUser$.subscribe(u=>{this.currentUser=u});
  }
  AcceptBit()
  {

    this.InitializeVaidationErrors();
    let comm :Comment = this.CreateCommentModel(+this.currentUser.id,this.f.Comment.value,this.currentUser.name,this.bit.visitorId,CommentTypeEnum.OfferAccepted);
    this.bit.comments.push(comm);
    this.bitSerVice.AcceptBit(this.bit).subscribe((response : Response<boolean>)  => {
      const isok : boolean =response.isOk;
      if(isok)
      {
        this.BitAccepted.emit(this.bit.id);
        var notif = new Notif();
        notif=this.helper.CreateNotificationModel("Your bit has been Accepted","Hey,Your bit has been Accepted","/Offer/View/"+this.idOffer);
        this.notifService.SendNotificationToUser(this.bit.visitorId,notif);
        console.log("Bit Accepted")
        this.activeModal.close();
        this.ResultPopUp("Bit Accepted","The Bit has been accepted",true);

      }
      else
      {
        this.validationErrors.push(response.message);
        console.log("Bit Not Accepted" ,response.message);
      }
      
    }, error => {
            this.validationErrors.push(error.error.message);
            console.log("Bit Not Accepted",error);
    
    });
    

  }
  RejectBit()
  {
    this.InitializeVaidationErrors();
    let comm :Comment = this.CreateCommentModel(+this.currentUser.id,this.f.Comment.value,this.currentUser.name,this.bit.visitorId,CommentTypeEnum.OfferRejected);
    this.bit.comments.push(comm);
    this.bitSerVice.RejectBit(this.bit).subscribe((response : Response<boolean>)  => {
      const isok : boolean =response.isOk;
      if(isok)
      {
        this.BitRejected.emit(this.bit.id);
        var notif = new Notif();
        notif=this.helper.CreateNotificationModel("Your bit has been Rejected","Hey,Your bit has been Rejected","/Offer/View/"+this.idOffer);
        this.notifService.SendNotificationToUser(this.bit.visitorId,notif);
        console.log("Bit Rejected")
        this.activeModal.close();
        this.ResultPopUp("Bit Rejected","The Bit has been rejected",true);

      }
      else
      {
        this.validationErrors.push(response.message);
        console.log("Bit Not Rejected" ,response.message);
      }
      
    }, error => {
            this.validationErrors.push(error.error.message);
            console.log("Bit Not Rejected",error);
    
    });
    
  }
  CreateCommentModel(senderId:number,content:string,senderName:string,ReceiverId:number,type:CommentTypeEnum,ReceiveName?:string):Comment
  {
   return this.helper.CreateCommentModel(senderId,content,senderName,ReceiverId,type,ReceiveName);
  }

  ResultPopUp(message:string,title:string,isOk:boolean)
  {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.message=message;
    modalRef.componentInstance.title=title;
    modalRef.componentInstance.isError=!isOk;

  }

 
}
