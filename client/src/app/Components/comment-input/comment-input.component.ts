import { Component, EventEmitter, Input, OnInit, Output, Self } from '@angular/core';

@Component({
  selector: 'app-comment-input',
  templateUrl: './comment-input.component.html',
  styleUrls: ['./comment-input.component.css']
})
export class CommentInputComponent implements OnInit {
 @Input() title :string;
 @Input() dateCreated :string="";
 @Input() Href :string="";
 @Input() content:string;
 @Input() isDisabled : boolean=true;
 @Input() isRequired : boolean=false;
 @Output() outputContent = new EventEmitter<string>();


 constructor() { 
}
  ngOnInit(): void {
  }
  ChangeOutputContent(e)
  {
    this.outputContent.emit(e.target.value);
  }



}
