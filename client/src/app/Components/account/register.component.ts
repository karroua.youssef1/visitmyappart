import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { AccountService } from '../../../Services/AccountService'
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/Models/User';
import { Country } from 'src/Models/Country';
import { AdressInputComponent } from '../forms/adress-input/adress-input.component';
import { City } from 'src/Models/City';
 

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  @Output() registerOK = new EventEmitter();

  registerForm: UntypedFormGroup;
  maxDate: Date;
  validationErrors: string[] = [];
  role:string;
  selectedCountry:Country=new Country();
  selectedCity:City=new City();
  @ViewChild('city') cityComponent: AdressInputComponent;
  roleSelected:string;


  constructor(private accountService: AccountService, 
    private fb: UntypedFormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.intitializeForm();
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() -18);
  }

  intitializeForm() {
    this.registerForm = this.fb.group({
      Gender: ['male'],
      UserName: ['', Validators.required],
      Email: ['', Validators.required],
      Role:  ['Client'],
      DateOfBirth: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      Password: ['', [Validators.required, 
        Validators.minLength(4), Validators.maxLength(16)]],
      confirmPassword: ['', [Validators.required, this.matchValues('Password')]],
    })
  }

  matchValues(matchTo: string): ValidatorFn {
    return (control: AbstractControl) => {
      return control?.value === control?.parent?.controls[matchTo].value 
        ? null : {isMatching: true}
    }
  }
  RadioClick(radio:string)
  {
 this.roleSelected=radio;
  }

  register() {
    console.log("debut register");
    let user:User=this.registerForm.value;
    this.role=this.registerForm.controls["Role"].value;
    user.country=this.selectedCountry.long_name;
    user.city=this.selectedCity;
    console.log(user);
    this.accountService.register(user).subscribe(response => {
      this.registerOK.emit(true);
      this.router.navigate(['/login',{firstTime:true,role:this.role}]);
      console.log("register OK");
    }, error => {
      this.registerOK.emit(false);
            this.validationErrors = error;
            console.log("register KO");
    })
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

  SetCountry(event :  google.maps.places.PlaceResult)
  {
    this.selectedCountry.long_name= event.address_components[0].long_name;
    this.selectedCountry.short_name= event.address_components[0].short_name;
    console.log("Country ",this.selectedCountry);
    this.cityComponent.country=this.selectedCountry;
    this.cityComponent.ngOnInit();
    this.cityComponent.ngAfterViewInit();
  }
  SetCity(event :  google.maps.places.PlaceResult)
  {
    this.selectedCity.name= event.address_components[0].long_name;
    this.selectedCity.lng= event.geometry.location.lng()
    this.selectedCity.lat= event.geometry.location.lat();

    console.log("City",this.selectedCity);
  }
}
