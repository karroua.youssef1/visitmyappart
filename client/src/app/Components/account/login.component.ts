import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {ConnexionModel} from '../../../Models/ConnexionModel'

import { AccountService } from '../../../Services/AccountService';
import { AlertService } from '../../../Services/AlertService';
import { PaymentService } from 'src/Services/PaymentService';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
  })
export class LoginComponent implements OnInit {
    loginForm: UntypedFormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    CurrentConnexionModel : ConnexionModel;
    validationErrors: string[] = [];
    isFirstTime=false;
    isVisitor=false;


    constructor(
        private formBuilder: UntypedFormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService,
        private paymentService:PaymentService
       
    ) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            Email: ['', Validators.required],
            Password: ['', Validators.required]
        });
   this.isFirstTime=JSON.parse(this.route.snapshot.paramMap.get('firstTime'));
   if(this.route.snapshot.paramMap.get('role').toLowerCase()=="visitor") this.isVisitor=true;
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        console.log(this.isFirstTime + ""+this.isVisitor);
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }


    
    login() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();
        this.validationErrors=[];

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.CurrentConnexionModel={Email:this.f.Email.value,Password:this.f.Password.value};

        this.accountService.login(this.CurrentConnexionModel)
            .pipe(first())
            .subscribe(
                data => {
                    console.log("data",data);
                if(this.isFirstTime && this.isVisitor) this.router.navigateByUrl('profil/visit')
                else
               {
                 //   this.router.navigate('home');
                    this.router.navigateByUrl('/Offers/All');
                }

                },
                error => {
                    this.validationErrors.push(error.error) ;
                    console.log(error);
                    this.alertService.error(error);
                    this.loading = false;
                });


    }
    cancel() {
    }
}