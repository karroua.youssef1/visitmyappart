import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Offer } from 'src/Models/Offer';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { OfferService } from 'src/Services/OfferService';
import { BitModalComponent } from '../../Bit/bit-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Bit } from 'src/Models/Bit';
import { Helper } from 'src/Helpers/Helper';
import { OfferStateEnum } from 'src/Enums/OfferStateEnum';
import { Adress } from 'src/Models/Adress';




@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})


export class OfferComponent implements OnInit {
  offer:Offer;
  offerForm : UntypedFormGroup;
  isEditProfil:boolean;
  currentUser:User;
  validationErrors: string[] = [];
  id:number;
  state : string='CREATE';
  bits :Bit[];
  isVisitor : boolean=false;
  isClient:boolean=false;
  title:string="Create your offer";
  HasBitInHere :boolean;
  BitOfVisitor : Bit[];
  numberOfBits :number;
  OfferOpen :boolean;
  Adress:Adress=new Adress();
  HasLinkedPayPal:boolean;


  constructor(private helper:Helper, private modalService : NgbModal  ,private route: ActivatedRoute,private accountService : AccountService,private fb: UntypedFormBuilder, private router: Router , private offerService : OfferService) { }


  
  ngOnInit(): void {
    console.log(this.state);
    this.route.paramMap.subscribe(params => { 
      this.id = +params.get('id'); 
  });
  console.log("id",this.id);
    this.route.data.subscribe(v => this.isEditProfil=JSON.parse(v.isEditProfil));
    this.accountService.currentUser$.subscribe(u=>this.currentUser=u);
    console.log("offer",this.currentUser);
    if(this.id!=0)
    {
       this.GetOffer();
    }
    else
    {
      this.intitializeForm(this.offer);

    }
    if( this.helper.isObjectEmptyOrNull(this.currentUser))
    {
    this.router.navigateByUrl("/login");
    }
    console.log(this.offer);

    
  }

  SetAdress(event :  google.maps.places.PlaceResult)
  {
    console.log(event);
    this.Adress=this.helper.CreateAdressModel(event.geometry.location.lat(),event.geometry.location.lng(),event.formatted_address,event.url);
    console.log("Adress",this.Adress);
  }
 
  isConnected():boolean
  {
    if(!this.helper.isObjectEmptyOrNull(this.currentUser))
    {
      return true;
    }
    {
     return false;
    }
  }

  RedirectToPaypal()
  {
   let url :string ="https://www.sandbox.paypal.com/connect?flowEntry=static&client_id=AThNri3bGhvcC1xPEsFMCXH-JiBi8prJwor0Evt1WU6Ky56KxEE-UYEcSJ3tlCGrnaInLQxm9yer8-d5&scope=openid&redirect_uri=http://localhost:5000/PayPal/GetUserInfos"
    window.open(url, '_blank');

  }

  SeeMaps(url:string)
  {
    window.open(url, '_blank');

  }
  SetRoleOfCurrentUser()
  {
    if(!this.helper.isObjectEmptyOrNull(this.currentUser))
    {
      if(+this.currentUser.id===+this.offer.client.id )
      {
       this.isClient=true;
      }
      else if(this.offer.state==OfferStateEnum.Sealed)
      {
        if(!this.helper.isObjectEmptyOrNull(this.offer.visitor))
        {
      if(+this.currentUser.id===+this.offer.visitor.id )
      {
         this.isVisitor=true;
      }
    }
    }
    }

  }
  async GetOffer()
  {
    await this.offerService.GetOffer(this.id).toPromise().then(
      (response :Offer) => {                           
        console.log('response received');
        this.offer = response; 
        console.log(this.offer);
        this.intitializeForm(this.offer);
        this.SetRoleOfCurrentUser();
        this.DoesUserHasABitInHere();
        this.isOfferOpen();


      },
      (error) => {                             
        console.error('Request failed with error')
        this.validationErrors.push(error);
        this.intitializeForm(this.offer);

      })

  }
  isOfferOpen()
  {
    if(this.offer.state===OfferStateEnum.Open)
    {
      this.OfferOpen=true;
    }
    else
    {
      this.OfferOpen=false;
    }
  }
  intitializeForm(offer? :Offer) {
    console.log("initialize form",offer);
    if(offer!=null && offer !=undefined )
    {
      this.offerForm = this.fb.group({
        Titre: [offer.titre, Validators.required],
        Adress: [offer.adress.adressContent, Validators.required],
        Description: [offer.description, Validators.required],
        LimitDate:  [new Date(offer.limitDate), Validators.required],
        PriceOffered: [offer.priceOffered, Validators.required],
        link: [offer.link],
        Client :offer.client,
        Id:  offer.id
      })
      this.Adress=offer.adress;
      console.log(offer.adress.adressContent);
    }
    else
    {this.offerForm = this.fb.group({
      Titre: ['', Validators.required],
      Adress: ['', Validators.required],
      Description: ['', Validators.required],
      LimitDate:  ['', Validators.required],
      PriceOffered: ['', Validators.required],
      link: [''],
      Client : this.currentUser,
      Id:  0
    })}
    
  }
  get f() { return this.offerForm.controls; }

  DoesUserHasABitInHere()
  {
   if(this.offer.bits.filter(x=>x.visitorId===+this.currentUser.id).length>0)
   {
    this.HasBitInHere=true;
    this.BitOfVisitor=this.offer.bits.filter(x=>x.visitorId===+this.currentUser.id);
   }
   else
   {
this.HasBitInHere=false;
   }
  }
  Save()
  {
    if(!this.currentUser.paymentInformationsCompleted)
    { let message :string="Complete your payment informations to be able to create your offer";
      this.helper.ResultPopUp(message,"Payment informations",false,"profil/payment","Ici!");
      return;
    }
    console.log(this.offerForm.value);
    this.f.PriceOffered.setValue(+this.f.PriceOffered.value);
    console.log(this.state);
    if(this.state=='CREATE')
    {
    this.CreateOffer();
    
  }
  if(this.state=='UPDATE')
  {
    this.UpdateOffer();
  }
  }

  CreateOffer()
  {
    this.validationErrors=[];
    var id;
    this.offerForm.controls["Adress"].setValue(this.Adress);
    this.offerService.CreateOffer(this.offerForm.value).subscribe(response => {
      id=response;
      console.log("Offer Created",id);
      this.helper.ResultPopUp("Your Offer has been Created","Offer Created",true);
      const url ='/Offer/View/'+id
      this.router.navigateByUrl(url);

    }, error => {
            this.validationErrors.push(error.message);
            console.log("Offer Not Created",error);
            this.helper.ResultPopUp("Your Offer was not Created","Offer Not created Created",false);

    });
  }

  UpdateOffer()
  {
    this.validationErrors=[];
    var id;
    this.Adress.id=this.offer.adress.id;
    this.offerForm.controls["Adress"].setValue(this.Adress);
    this.offerService.UpdateOffer(this.offerForm.value).subscribe(response => {
      id=response;
      this.router.navigateByUrl('/home');
      console.log("Offer Updated");
    }, error => {
            this.validationErrors.push(error.message);
            console.log("Offer Not Updated",error);
    });
  }
  Cancel()
  {
    this.router.navigateByUrl('/home');

  }
  Edit()
  {
    this.isEditProfil=!this.isEditProfil;
    this.state='UPDATE';
  }
  Bit()
  {
    const modalRef = this.modalService.open(BitModalComponent);
    modalRef.componentInstance.idOffer=this.offer.id;
    modalRef.componentInstance.idClient=this.offer.client.id;
    modalRef.componentInstance.BitCreated.subscribe((receivedEntry) => {
     if(receivedEntry)
     {
      this.reloadCurrentRoute();
    console.log("ok");} 
      })
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
}
  Home()
  {
    this.router.navigateByUrl('/home');

  }

}
