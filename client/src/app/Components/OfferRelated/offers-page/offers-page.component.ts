import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Offer } from 'src/Models/Offer';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { OfferService } from 'src/Services/OfferService';
import { Helper } from 'src/Helpers/Helper';
import { OfferStateEnum } from 'src/Enums/OfferStateEnum';
import {PageEvent} from '@angular/material/paginator';


@Component({
  selector: 'app-offers-page',
  templateUrl: './offers-page.component.html',
  styleUrls: ['./offers-page.component.css']
})
export class OffersPageComponent implements OnInit {
  length = 50;
  pageSize = 10;
  pageIndex = 0;
  isOpenOffers = false;
  isOffersInMyRegion = false;
  isMyOffers = false;
  disabled = false;
  pageEvent: PageEvent;
  filter : string ;
  @Input() toRename :boolean=false; 
  filteredOffers:Offer[];
  ShownOffers:Offer[];
  AllOffers :Offer[];
  filters:Array<string>=new Array<string>();
  user:User;
  isVisitor:boolean=false;



  constructor(private helper:Helper,private router:Router, private route: ActivatedRoute,protected accountService : AccountService,private offerService : OfferService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => { 
      this.filter = params.get('filter'); 
  });
  console.log(this.filter);
  this.GetCurrentUserInfo();
  this.GetOffers();
}

GetPaginatedOffers()
{
  this.ShownOffers=this.filteredOffers.slice(this.pageIndex,this.pageSize);
}
  GetOfferListFiltred()
  {
    switch(this.filter.toLowerCase()) { 
      case 'mine': { 
        if(this.helper.isObjectEmptyOrNull(this.user))
        {
          this.router.navigateByUrl('/login');
        }
        else
        {
          this.isMyOffers=true;
          this.GetMyOffers(this.AllOffers);
        }

         break; 
      } 
      case 'all': { 
        this.GetAllOffers(this.AllOffers)
        break; 
      } 
      case 'region': { 
        if(this.helper.isObjectEmptyOrNull(this.user))
        {
          this.router.navigateByUrl('/login');
        }
        else
        {
          this.isOffersInMyRegion=true;
          this.GetAllOffersFromMyregion(this.AllOffers);
        }

        break; 
      } 
      case 'open': { 
        this.isOpenOffers=true;
        this.GetOpenOffers(this.AllOffers);
        break; 
      } 
      default: { 
         this.OffersByDate(this.AllOffers); 
         break; 
      } 
   } 

  }

  OffersByDate(input:Offer[])
  {
    this.filteredOffers=input.sort((objA, objB) => objB.dateCreated.getTime() - objA.dateCreated.getTime());
  }
  GetOpenOffers(input:Offer[])
  {
    this.filteredOffers=input.filter(x=>x.state===OfferStateEnum.Open);
  }
  GetAllOffers(input:Offer[])
  {
    this.filteredOffers=input;
  }

  Filter(filter:string)
  {
    this.filter=filter;
    console.log(this.filter);
    this.GetOfferListFiltred();
  }

   GetAllOffersFromMyregion(input:Offer[])
  { 
    console.log("region user",this.filteredOffers);

    var prefferedRegion = new google.maps.LatLng(this.user.prefferedRegion.x, this.user.prefferedRegion.y);
   this.filteredOffers= input.filter(x=>(this.helper.GetDistanceBetweenLocations(new google.maps.LatLng(prefferedRegion, 174.735292),new google.maps.LatLng(x.adress.x, x.adress.y)))<=this.user.prefferedRegion.radius)
   console.log("region offers",this.filteredOffers);
  }

  GetCurrentUserInfo()
  {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
    if(!this.helper.isObjectEmptyOrNull(this.user))
    {
      if(this.user.role.toLowerCase()==="visitor")
      this.isVisitor=true;
    }
  }
  GetMyOffers(input:Offer[])
  {

    this.filteredOffers=input.filter(x=>+x.client.id===+this.user.id);

  }

  CheckFilter(value:string)
  {

    switch(value) { 
      case 'my': { 
       if(!this.isMyOffers)
       {
        this.filters.push('my');
       }
       else
       {
     this.filters=this.filters.filter(x=>x!='my');
       }

         break; 
      } 
      case 'open': { 
        if(!this.isOpenOffers)
        {
          this.filters.push('open');
      }
      else
       {  
          this.filters=this.filters.filter(x=>x!='open'); 
       }
        break; 
      } 
      case 'region': {
        if(!this.isOffersInMyRegion)
        {
          this.filters.push('region');
        }
        else {  
          this.filters=this.filters.filter(x=>x!='region'); 
        }
        break; 
      } 
      default: { 
         break; 
      } 
   }
   console.log("its here"); 
   this.FilterLoop();
  }

  FilterLoop()
  {
console.log(this.filters);
if(this.filters.length===0)
{ this.GetAllOffers(this.AllOffers);}
else
{
  let temporaryOffers:Offer[]=this.AllOffers;
    this.filters.forEach(element => {
      switch(element)
      {
        case 'my':
          this.GetMyOffers(temporaryOffers);      
          break;
      
          case 'open' :
            this.GetOpenOffers(temporaryOffers);
          break;

          case 'region' :
            this.GetAllOffersFromMyregion(temporaryOffers);
          break;

            default:
              break;      
      }
      temporaryOffers=this.filteredOffers;
    });
  }
    this.GetPaginatedOffers();
  }

  GetOffers()
  {
    this.offerService.GetAllOffers().subscribe(response=>{
      this.filteredOffers=response as Offer[];
      console.log("All Offers are Here",this.filteredOffers);
      this.AllOffers=this.filteredOffers;
      this.GetOfferListFiltred();
      this.GetPaginatedOffers();
    }, error => {
            
            console.log("All Offer Are note here",error);
    });

  }
  handlePageEvent(e: PageEvent) {
    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    console.log(this.pageIndex);
   this.ShownOffers=this.filteredOffers.slice(this.pageIndex*10,this.pageSize*(this.pageIndex+1));
  }

  Create()
  {
    this.router.navigateByUrl('Offer/Create');
  }

}
