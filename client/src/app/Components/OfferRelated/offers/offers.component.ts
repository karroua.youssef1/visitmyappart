import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Offer } from 'src/Models/Offer';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { OfferService } from 'src/Services/OfferService';
import { Helper } from 'src/Helpers/Helper';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {


@Input() offers:Offer[];
user:User;
  constructor(private helper:Helper, private route: ActivatedRoute,private accountService : AccountService,private offerService : OfferService) { }

  ngOnInit(): void {

  }


  GetCurrentUserInfo()
  {
    this.accountService.currentUser$.subscribe(u=>this.user=u);
  }

}
