import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
message :string;
isError : boolean;
title : string;
link:string="";
linkMessage:string="";
isLink:Boolean=false;



  constructor( public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    if(this.link!=""){this.isLink=true;}
  }

  

}
