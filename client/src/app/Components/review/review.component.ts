import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Helper } from 'src/Helpers/Helper';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

@Input()  rating :number=0;
@Input() isEdit:boolean=true;
@Input() size : number=50;
@Output() ratingChange = new EventEmitter<number>();

  constructor(private helper:Helper) { }

  ngOnInit(): void {
console.log(this.size);
  }



  onRateChange(rating:number)
  {
    this.ratingChange.emit(rating);

  }

}
