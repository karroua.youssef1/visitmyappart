import { Component, Input, OnInit, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})
export class TextInputComponent implements ControlValueAccessor,OnInit {
  @Input() label: string;
  @Input() name: string;
  @Input() isEdit: boolean;
  @Input() type = 'text';

  constructor(@Self() public ngControl: NgControl) { 
    this.ngControl.valueAccessor = this;
  }
  ngOnInit(): void {
    if(this.isEdit===null || this.isEdit===undefined )
    {
      this.isEdit=true;
    }
    if(this.name===null || this.name===undefined )
    {
      this.name=this.label;
    }
}

  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }
  
}
