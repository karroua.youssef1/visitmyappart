import { Component, EventEmitter, Input, OnInit, Output, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { Helper } from 'src/Helpers/Helper';
import { Country } from 'src/Models/Country';


@Component({
  selector: 'app-adress-input',
  templateUrl: './adress-input.component.html',
  styleUrls: ['./adress-input.component.css']
})
export class AdressInputComponent implements ControlValueAccessor,OnInit {
  @Input() label: string;
  @Input() isEdit: boolean;
  @Input() type = 'text';
  @Input() val ;
  @Input() country:Country=new Country();
  @ViewChild('addresstext') addresstext: any;
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @Output() setCountry: EventEmitter<Country> = new EventEmitter();

  queryWait: boolean;


  constructor(@Self() public ngControl: NgControl,private helper:Helper) { 
    this.ngControl.valueAccessor = this;
  }
  ngOnInit(): void {
    if(this.helper.isObjectEmptyOrNull(this.isEdit))
    {
      this.isEdit=true;
    }
    if(this.helper.isObjectEmptyOrNull(this.country))
    {
      this.country.long_name="Canada";
      this.country.short_name="CA"
    }
}
private getPlaceAutocomplete() {
  var autocomplete :google.maps.places.Autocomplete;
  switch(this.adressType.toLowerCase()) { 
    case '(cities)': { 
      console.log("city");
      console.log(this.country.short_name);

      autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
        {   componentRestrictions: { country: this.country.short_name },
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
        });
      break; 
    } 
    case 'country': { 
      autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
        {
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
        });
      break; 
    } 
    default: { 
      autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
        {
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
        }); 
       break; 
    } 
 } 
 
  google.maps.event.addListener(autocomplete, 'place_changed', () => {
    var place = autocomplete.getPlace();
      this.invokeEvent(place);
  });
}
ngAfterViewInit() {
  this.getPlaceAutocomplete();
}
invokeEvent(place: google.maps.places.PlaceResult) {

  this.setAddress.emit(place);
}
  writeValue(obj: any): void {
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }
  
}
