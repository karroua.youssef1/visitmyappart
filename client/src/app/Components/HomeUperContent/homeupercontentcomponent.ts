import { Component, Input, OnInit } from '@angular/core';
import { Offer } from 'src/Models/Offer';
import { User } from 'src/Models/User';

@Component({
  selector: 'app-homeupercontent',
  templateUrl: './homeupercontent.component.html',
  styleUrls: ['./homeupercontent.component.css']
})
export class HomeUpperContentComponent implements OnInit {

  @Input() offers:Offer[];
  @Input() visitors:User[];

  constructor() { }

  ngOnInit(): void {
  }

}
