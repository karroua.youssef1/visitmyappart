import { Component, OnInit, Pipe } from '@angular/core';
import { Router } from '@angular/router';
import { Notif } from 'src/Models/Notification';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PresenceService } from 'src/Services/PresenceService';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})

export class NavBarComponent implements OnInit {
theUser: User;
isConnected:boolean;
notifs : Notif[];
count :number;
  constructor(private presenceService : PresenceService,public accountService: AccountService,private router: Router ) { }

  ngOnInit(): void {
    this.presenceService.notifications$.subscribe(n=>this.notifs=n);
    this.accountService.currentUser$.subscribe(u=>this.theUser=u);
    this.presenceService.notifications$.subscribe(n=>this.count=(n.filter(u=>u.isOpen==false).length));
    console.log("notifs",this.notifs);
    this.notifs.sort(function(a,b){return +(a.id<b.id)||-(a.id>b.id)});
    console.log("notifs sorted",this.notifs);

  }

 
LogOut()
{
  this.accountService.logout();
  this.presenceService.ClearNotifications();  
}

LogIn()
{
  this.router.navigateByUrl('/login');
}
SignUp()
{
  this.router.navigateByUrl('/register');

}

ChangeState(id:number)
{
  this.presenceService.ChangeNotificationState(id).then(val=>{if(val)
  {
   this.notifs.find(n=>n.id===id).isOpen=! this.notifs.find(n=>n.receiverId===id).isOpen;
   this.presenceService.SetNotifications(this.notifs);
   this.router.navigateByUrl(this.notifs.find(n=>n.id===id).link);
  }
})
}
ClearNotificationCount()
{
  this.notifs.forEach(u=>u.isOpen=true);
this.presenceService.SetNotifications(this.notifs);
this.presenceService.SetNotificationsToRead(+this.theUser.id).subscribe((response=>console.log("ok")),(error=>console.log("ko")));
}


MyOffers()
{
  const url = 'Offers/Mine'
  this.router.navigateByUrl(url);
}
AllOffers()
{
  const url = 'Offers/All'
  this.router.navigateByUrl(url);
}
}
