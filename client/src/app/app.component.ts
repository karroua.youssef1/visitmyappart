import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Notif } from 'src/Models/Notification';
import { User } from 'src/Models/User';
import { AccountService } from 'src/Services/AccountService';
import { PresenceService } from 'src/Services/PresenceService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'client';
  offers : any;
  adress:any;


  constructor(private toastr: ToastrService,private presenceService :PresenceService,private http:HttpClient,private accountService: AccountService)
  {

  }
  ngOnInit(): void {
    console.log("call app.component.ts");
    this.setCurrentUser();
    }
  getSubjects() {
    this.http.get('https://localhost:5001/api/Offer').subscribe({
      next: response => this.offers = response,
      error: error => console.log(error)
    })
  }

  setCurrentUser() {
    const user: User = JSON.parse(localStorage.getItem('user'));
    const notifs: Notif[] = JSON.parse(localStorage.getItem('notifs'));

    if (user) {
      this.accountService.setCurrentUser(user); 
     this.presenceService.createHubConnection(user);
       }
       if(notifs)
       {
        this.presenceService.SetNotifications(notifs);
       }
}

SetNotifications()
{
  const notifs: Notif[] = JSON.parse(localStorage.getItem('notifs'));
  if (notifs) {
    this.presenceService.SetNotifications(notifs);
     }
}
}
