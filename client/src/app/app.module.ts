import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './Modules/SharedModules';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {RegisterComponent} from '../app/Components/account/register.component'
import {LoginComponent} from '../app/Components/account/login.component'
import {HomeComponent} from '../app/Components/Home/home.component'
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { TextInputComponent } from './Components/forms/text-input/text-input/text-input.component';
import { DateInputComponent } from './Components/forms/date-input/date-input/date-input.component';
import { ProfilComponent } from './Components/profil/profil.component';
import { EmailInputComponent } from './Components/forms/email-input/email-input/email-input.component';
import { GoogleMapsModule } from '@angular/google-maps';
import {MatTabsModule} from '@angular/material/tabs';
import { GoogleMapsComponent } from './Components/Maps/google-maps/google-maps.component';
import { TestComponent } from './Components/test/test/test.component';
import { AdressInputComponent } from './Components/forms/adress-input/adress-input.component';
import { BitModalComponent } from './Components/Bit/bit-modal.component';
import { ModalComponent } from './Components/modal/modal.component';
import { ToastrModule } from 'ngx-toastr';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {NgPipesModule} from 'ngx-pipes';
import { NotificationsComponent } from './Components/notifications/notifications.component';
import { BitsComponent } from './Components/bits/bits.component';
import { FileComponent } from './Components/file/file.component';
import { CommentModalComponent } from './Components/comment-modal/comment-modal.component';
import { TimeLineComponent } from './Components/time-line/time-line.component';
import { CommentInputComponent } from './Components/comment-input/comment-input.component';
import { DropzoneComponent } from './Components/dropzone/dropzone.component';
import { ProgressComponent } from './Components/progress/progress.component';
import { VisitorCardComponent } from './Components/visitor-card/visitor-card.component';
import  { JwtModule } from "@auth0/angular-jwt";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReviewModalComponent } from './Components/review-modal/review-modal.component';
import { ReviewComponent } from './Components/review/review.component';
import { ReviewsComponent } from './Components/reviews/reviews.component';
import { VisitorsComponent } from './Components/visitors/visitors.component';
import { VisitorComponent } from './Components/visitor/visitor.component';
import { FooterComponent } from './Components/footer/footer.component';
import { SpinnerComponent } from './Components/spinner/spinner.component';
import { LoadingInterceptor } from 'src/interceptors/loading.interceptor';
import { PaymentInfosComponent } from './Components/profil/payment/payment-infos/payment-infos.component';
import { NgCreditCardModule } from "angular-credit-card";
import { GeneralInfosComponent } from './Components/profil/general/general-infos/general-infos.component';
import { VisitorPreferencesComponent } from './Components/profil/visiting informations/visitor-preferences/visitor-preferences.component';
import { MyBitsComponent } from './Components/bits/MyBits/my-bits/my-bits.component';
import { PaymentReturnUrlComponent } from './Components/profil/payment/returnUrl/payment-return-url/payment-return-url.component';
import { PaymentRefreshUrlComponent } from './Components/profil/payment/refreshUrl/payment-refresh-url/payment-refresh-url.component';
import { Footer2Component } from './Components/footer2/footer2.component';
import { HomeUpperContentComponent } from './Components/HomeUperContent/homeupercontentcomponent';
import { SliderComponent } from './Components/slider/slider.component';
import { NavBar2Component } from './Components/nav-bar2/nav-bar2.component';
import { HowItWorksComponent } from './Components/how-it-works/how-it-works.component';
import { HomeLowerContentComponent } from './Components/home-lower-content/home-lower-content.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { OfferComponent } from './Components/OfferRelated/offer/offer.component';
import { OffersComponent } from './Components/OfferRelated/offers/offers.component';
import { OffersPageComponent } from './Components/OfferRelated/offers-page/offers-page.component';




export function tokenGetter() { 
  console.log("token logger" ,localStorage.getItem("token"));

  return localStorage.getItem("token"); 
}

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    RegisterComponent,
    HomeComponent,
    TextInputComponent,
    DateInputComponent,
    LoginComponent,
    ProfilComponent,
    EmailInputComponent,
    OfferComponent,
    GoogleMapsComponent,
    TestComponent,
    OffersComponent,
    AdressInputComponent,
    BitModalComponent,
    ModalComponent,
    NotificationsComponent,
    BitsComponent,
    FileComponent,
    CommentModalComponent,
    TimeLineComponent,
    CommentInputComponent,
    DropzoneComponent,
    ProgressComponent,
    VisitorCardComponent,
    ReviewModalComponent,
    ReviewComponent,
    ReviewsComponent,
    OffersPageComponent,
    VisitorsComponent,
    VisitorComponent,
    FooterComponent,
    SpinnerComponent,
    PaymentInfosComponent,
    GeneralInfosComponent,
    VisitorPreferencesComponent,
    MyBitsComponent,
    PaymentReturnUrlComponent,
    PaymentRefreshUrlComponent,
    Footer2Component,
    HomeUpperContentComponent,
    SliderComponent,
    NavBar2Component,
    HowItWorksComponent,
    HomeLowerContentComponent,
     ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    GoogleMapsModule,
    ToastrModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    NgPipesModule,
    MatTabsModule,
    NgCreditCardModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:5000","localhost:5001"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
