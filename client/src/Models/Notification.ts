

export class Notif {
    id : number; 
    titre: string;
    message : string;
    senderId:number;
    receiverId:number;
    isOpen : boolean;
    link:string;
    dateCreated:Date;

}