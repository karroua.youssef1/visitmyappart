import { City } from "../City";

export class GeneralInfosModel {
    id: number;
    country: string;
    city:City;
}