import { Region } from "../Region";

export class ProfilModel {
    id: number;
    description: string;
    region:Region;
}