import { PaymentCredit } from "./PaymentCredit";

export class PaymentInfos {
    Email :string;
    Id :string;
    Name :string;
    CreditCard : PaymentCredit;
}