import { BitStateEnum } from "src/Enums/BitStateEnum";
import { Comment } from "./Comment";
import { ProfilePicture } from "./Files/ProfilePicture";
import { Report } from "./Report";

export class Bit {
    id?:number;
    offreId: number;
    visitorId : number;
    comments : Comment[];
    clientId : number;
    visitorName? : string;
    profilPic ? : ProfilePicture;
    report? : Report;
    state: BitStateEnum;
    isThereAreport? :boolean;
  
         

}