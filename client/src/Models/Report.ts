
import { ReportStateEnum } from "src/Enums/ReportStateEnum";
import { Bit } from "./Bit";
import { ReportFile } from "./Files/ReportFile";

export class Report 
{   id? :number;
    content :string;
    senderId :number;
    fileAttachments? : ReportFile[];
    bit:Bit;
    bitId:number;
    state :ReportStateEnum;
    reportFilesExist? :boolean

}