

export class File 
{
     bytes :number[];
     name :string;
     fileExtension :string;
     size :number;
     type:string;
   }

