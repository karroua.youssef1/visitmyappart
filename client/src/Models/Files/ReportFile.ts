import { SafeUrl } from "@angular/platform-browser";
import { Helper } from "src/Helpers/Helper";
import { Report } from "../Report";
import { User } from "../User";
import { File } from "./File";

export class ReportFile extends File
{
    reportId :number;
    user :User;
    report : Report;
    base64Bytes : string;


}