import { SafeUrl } from "@angular/platform-browser";
import { Helper } from "src/Helpers/Helper";
import { User } from "../User";
import { File } from "./File";

export class ProfilePicture extends File
{
    userId :number;
    user :User;
    base64Bytes : string;

}