
import { ReportStateEnum } from "src/Enums/ReportStateEnum";
import { Bit } from "./Bit";
import { ReportFile } from "./Files/ReportFile";

export class Review 
{
    comment :string;
    rating :number;
    visitorId : number;
    clientId:number;
    clientFullName?:string;

}