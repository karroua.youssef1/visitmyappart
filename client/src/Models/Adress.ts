
export class Adress {
    id?:number;
    offreId?: number;
    adressContent : string;
    x  : number;
    y : number;
    mapsUrl: string;
}