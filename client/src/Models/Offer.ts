import { Bit } from "./Bit";
import { User } from "./User";
import {OfferStateEnum} from "../Enums/OfferStateEnum";
import { Adress } from "./Adress";

export class Offer {
    id :number;
    titre :string;
    adress : Adress ;
    x : number;
    y:number;
    description:string;
    limitDate : Date ;
    priceOffered : number;
    client : User;
    visitor : User;
    link:string;
    isDeal:boolean;
    bits : Array<Bit>;
    state: OfferStateEnum;
    numberOfBits :number;
    dateCreated :Date;
}