import { CommentTypeEnum } from "src/Enums/CommentTypeEnum";


export class Comment {
    id?:number;
    content: string;
    senderId : number;
    ReceiverId : number;
    ReceiveName? : string;
    senderName ? : string;
    type:CommentTypeEnum;
}