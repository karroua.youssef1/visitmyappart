import { City } from "./City";
import { ProfilePicture } from "./Files/ProfilePicture";
import { Region } from "./Region";
import { Review } from "./Review";

export class User {
    id: string;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    email : string ;
    name:string ;
    description : string;
    token: string;
    role :string;
    city:City;
    country:string;
    prefferedRegion : Region;
    profilPicture : ProfilePicture;
    visitorReviews :Review[];
    reviewAverage:number;
    reviewsCount:number;
    visitingPreferencesCompleted:Boolean;
    paymentInformationsCompleted:Boolean;
}