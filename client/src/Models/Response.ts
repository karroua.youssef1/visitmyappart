

export class Response<Type> {
    isOk: boolean;
    message: string;
    responseModel: Type;
}