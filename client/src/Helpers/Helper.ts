import { Injectable } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CommentTypeEnum } from 'src/Enums/CommentTypeEnum';
import { Comment } from 'src/Models/Comment';
import { Notif } from 'src/Models/Notification';
import { saveAs } from 'file-saver';
import { ModalComponent } from 'src/app/Components/modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Adress } from 'src/Models/Adress';
import { UntypedFormGroup } from '@angular/forms';



@Injectable({
  providedIn: 'root'
})
export class Helper {

  constructor(private modalService : NgbModal ,private sanitizer: DomSanitizer) { }

  isObjectEmptyOrNull(object : Object):boolean
  {
   if(object instanceof File)
   {
    if (object===null || object===undefined)
  {
    return true;
  }
  else
  { return false;}
   }
  if (object===null || object===undefined || JSON.stringify(object)==="{}")
  {
    return true;
  }
  else
  {
return false;
  }
  }

  GetImageSafeUrl(base64Bytes :string,type:string):SafeUrl
  {
    let objectURL = 'data:'+type+';base64,' + base64Bytes;
    return this.sanitizer.bypassSecurityTrustUrl(objectURL);
   
  }

  DownloadFile(base64Bytes :string,type:string,name:string)
  {
    const byteCharacters = atob(base64Bytes);
    const file = this.base64ToBlob(base64Bytes,type);
    saveAs(file, name);
    
  }


  ResultPopUp(message:string,title:string,isOk:boolean,link?: string,linkMessage?: string)
  {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.message=message;
    modalRef.componentInstance.title=title;
    modalRef.componentInstance.isError=!isOk;
    if(!this.isObjectEmptyOrNull(link))
    {
      modalRef.componentInstance.link=link;
      modalRef.componentInstance.linkMessage=linkMessage;
    }

  }

  public CreateAdressModel(x:number,y:number,adressContent:string, mapsUrl:string,offerId ? :number):Adress
  {
    let result:Adress;
    if(this.isObjectEmptyOrNull(offerId))
    {
      result = {x:x,y:y,adressContent:adressContent,mapsUrl:mapsUrl};

    }
    else
    {
       result = {offreId:offerId,x:x,y:y,adressContent:adressContent,mapsUrl:mapsUrl};

    }
 return result;
  }

  public GetDistanceBetweenLocations(a:google.maps.LatLng,b:google.maps.LatLng):number
  {
    return google.maps.geometry.spherical.computeDistanceBetween(a, b);

  }
  
  public base64ToBlob(b64Data, contentType='', sliceSize=512) {
    b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
    let byteCharacters = atob(b64Data);
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        let byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
}
  CreateNotificationModel(titre:string,message:string,link:string):Notif
  {
    var notif = new Notif();
    notif.titre=titre;
    notif.message=message;
    notif.link =link;
    return notif;
  }

  CreateCommentModel(senderId:number,content:string,senderName:string,ReceiverId:number,type:CommentTypeEnum,ReceiveName?:string):Comment
  {
    var comm = new Comment();
    comm.senderId=senderId;
    comm.content=content;
    comm.senderName=senderName;
    comm.ReceiverId=ReceiverId;
    comm.ReceiveName=ReceiveName;
    comm.type=type;
    return comm;
  }

  scrollToAHeight(height :number,ms :number)
  {
    setTimeout(() => {
      window.scrollTo(0, height);
    }, ms);
  }
  GetFormControls(profilForm: UntypedFormGroup)
  {
  return profilForm.controls;
  }

}
