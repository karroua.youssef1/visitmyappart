using Core.Application.Contracts.Persistance;
using System;
using System.Threading.Tasks;
using Persistance.Context;

namespace Persistance.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TheContext _context;
        private IOfferRepository _offerRepository;
        private IClientRepository _clientRepository;
        private IVisitorRepository _visitorRepository;
        private IBitRepository _bitRepository;
        private IReviewRepository _reviewRepository;
        private IRegionRepository _regionRepository;
        private INotificationRepository _notificationRepository;
        private ICommentRepository _commentRepository;
        private IReportRepository _reportRepository;
        private IReportFileRepository _reportFileRepository;
        private IAdressRepository _adressRepository;
        public UnitOfWork(TheContext context)
        {
            _context = context;
        }
        public IAdressRepository AdressRepository => 
            _adressRepository ??= new AdressRepository(_context);
        public IReportFileRepository ReportFileRepository => 
            _reportFileRepository ??= new ReportFileRepository(_context);
        public ICommentRepository CommentRepository => 
            _commentRepository ??= new CommentRepository(_context);
        public IOfferRepository OfferRepository => 
            _offerRepository ??= new OfferRepository(_context);
        public INotificationRepository NotificationRepository => 
            _notificationRepository ??= new NotificationRepository(_context);
        public IClientRepository ClientRepository => 
            _clientRepository ??= new ClientRepository(_context);
        public IVisitorRepository VisitorRepository => 
            _visitorRepository ??= new VisitorRepository(_context);
          public IBitRepository BitRepository => 
            _bitRepository ??= new BitRepository(_context);
        public IReviewRepository ReviewRepository => 
            _reviewRepository ??= new ReviewRepository(_context);
        public IRegionRepository RegionRepository => 
            _regionRepository ??= new RegionRepository(_context);
                public IReportRepository ReportRepository => 
            _reportRepository ??= new ReportRepository(_context);
        
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task Save() 
        {
        await _context.SaveChangesAsync();
        }
    }
}
