using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain;

namespace Persistance.Repositories
{

public class CommentRepository : GenericRepository<Comment>, ICommentRepository
{
    private readonly TheContext _dbContext;

    public CommentRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

    
}

}
