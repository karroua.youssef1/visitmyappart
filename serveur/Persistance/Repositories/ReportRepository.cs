using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Domain.Files;

namespace Persistance.Repositories
{

public class ReportRepository : GenericRepository<Report>, IReportRepository
{
    private readonly TheContext _dbContext;

    public ReportRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
        public async  Task<List<ReportFile>> GetReportFilesByReportId(int reportId)
    {

    return await  _dbContext.reportFiles.Where(q => q.ReportId == reportId).ToListAsync();
    }
}

}
