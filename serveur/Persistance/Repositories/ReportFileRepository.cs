using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain.Files;

namespace Persistance.Repositories
{
public class ReportFileRepository : GenericRepository<ReportFile>, IReportFileRepository
{
    private readonly TheContext _dbContext;

    public ReportFileRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
}

}
