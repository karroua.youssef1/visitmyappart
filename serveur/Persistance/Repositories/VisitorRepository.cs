using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain.Users;

namespace Persistance.Repositories
{

public class VisitorRepository : GenericRepository<User>, IVisitorRepository
{
    private readonly TheContext _dbContext;

    public VisitorRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
}

}
