using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain;

namespace Persistance.Repositories
{

public class RegionRepository : GenericRepository<Region>, IRegionRepository
{
    private readonly TheContext _dbContext;

    public RegionRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

}

}
