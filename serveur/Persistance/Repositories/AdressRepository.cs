using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain;


namespace Persistance.Repositories
{

public class AdressRepository : GenericRepository<Adress>, IAdressRepository
{
    private readonly TheContext _dbContext;

    public AdressRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

}

}
