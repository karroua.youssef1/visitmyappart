using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain;

namespace Persistance.Repositories
{

public class ReviewRepository : GenericRepository<Review>, IReviewRepository
{
    private readonly TheContext _dbContext;

    public ReviewRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
}

}
