using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;


namespace Persistance.Repositories
{

public class BitRepository : GenericRepository<Bit>, IBitRepository
{
    private readonly TheContext _dbContext;

    public BitRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

    public async  Task<List<Bit>> GetBitsByOfferId(int offerId)
    {

    return await  _dbContext.bits.Where(q => q.OffreId == offerId).ToListAsync();
    }

    public async  Task<Bit> GetBitById(int id)
    {

    return await  _dbContext.bits.Where(q => q.Id == id).FirstOrDefaultAsync();
    }
    public async Task<List<Bit>> GetBitsByUserId(int id)
     {
        return await  _dbContext.bits.Where(q => q.VisitorId == id).ToListAsync();
    }

}

}
