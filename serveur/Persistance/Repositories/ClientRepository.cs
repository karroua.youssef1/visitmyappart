using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Domain.Users;

namespace Persistance.Repositories
{

public class ClientRepository : GenericRepository<User>, IClientRepository
{
    private readonly TheContext _dbContext;

    public ClientRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
}

}
