using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Microsoft.EntityFrameworkCore;
using Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Persistance.Repositories
{

public class OfferRepository : GenericRepository<Offer>, IOfferRepository
{
    private readonly TheContext _dbContext;

    public OfferRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddOffers(List<Offer> offers)
    {
        await _dbContext.AddRangeAsync(offers);
    }

    public async Task<bool> OfferExists(int clientId, int offerId)
    {
        return await _dbContext.offers.AnyAsync(q => q.client.Id == clientId
                                    && q.Id == offerId);
    }

    public async Task<List<Offer>> GetOffers()
    { 
        var offersResponse = await _dbContext.offers.ToListAsync();
        return offersResponse;
    }

    public async  Task<Offer> GetOfferById(int id)
    {
    return await  _dbContext.offers.Where(q => q.Id == id).FirstOrDefaultAsync() as Offer;
    }
    public async  Task<List<Offer>> GetAnumberOfOffersOrderByDate(int number)
    {
    return await  _dbContext.offers.OrderByDescending(u => u.DateCreated).Take(3).ToListAsync();
    }

    public async Task<List<Offer>> GetClientOffers(int clientId)
    {
        return await _dbContext.offers.Where(q => q.client.Id == clientId).ToListAsync();
    }

}

}
