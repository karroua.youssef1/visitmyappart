using Core.Application.Contracts.Persistance;
using Persistance.Context;
using Microsoft.EntityFrameworkCore;
using Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Persistance.Repositories
{

public class NotificationRepository : GenericRepository<Notification>, INotificationRepository
{
    private readonly TheContext _dbContext;

    public NotificationRepository(TheContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Domain.Notification>> GetNotificationsById(int userId)
    {
        return await _dbContext.notifications.Where(q => q.ReceiverId == userId).OrderBy(notif => notif.DateCreated).ToListAsync();
    }

     public void UpdateNotificationState(int id)
        {
            Domain.Notification notif =_dbContext.notifications.Where(q => q.Id == id).FirstOrDefault();
            notif.isOpen= !notif.isOpen;
            _dbContext.Entry(notif).State = EntityState.Modified;
        }

    public async  Task UpdateNotificationsStateToRead(int idUser)
    {
           List<Domain.Notification> notifs = await _dbContext.notifications.Where(q => q.ReceiverId == idUser).ToListAsync() as List<Domain.Notification>;
           foreach (Domain.Notification notif in notifs)
           {
            notif.isOpen= true;
            _dbContext.Entry(notif).State = EntityState.Modified;
           }
            
    }

}

}
