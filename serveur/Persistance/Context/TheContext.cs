using Domain;
using Domain.Files;
using Microsoft.EntityFrameworkCore;

namespace Persistance.Context
{
    public class TheContext : AuditableDbContext
    {
        public TheContext(DbContextOptions<TheContext> options): base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Offer>().ToTable("Offers");
            modelBuilder.Entity<Report>().ToTable("Reports");
            modelBuilder.Entity<ReportFile>().ToTable("ReportFiles");
            modelBuilder.Entity<Bit>().ToTable("Bits");
            modelBuilder.Entity<Review>().ToTable("Reviews");
            modelBuilder.Entity<Notification>().ToTable("Notifications");
            modelBuilder.Entity<Region>().ToTable("Regions");
            modelBuilder.Entity<Comment>().ToTable("Comments");
            modelBuilder.Entity<City>().ToTable("Cities");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Offer> offers { get; set; }
        public DbSet<ReportFile> reportFiles { get; set; }
        public DbSet<Comment> comments { get; set; }
        public DbSet<Notification> notifications { get; set; }
        public DbSet<Region> regions { get; set; }
        public DbSet<Bit> bits { get; set; }
        public DbSet<Review> reviews { get; set; }
        public DbSet<Report> reports { get; set; }
        public DbSet<City> cities { get; set; }

    }
}
