using Core.Application.Contracts.Persistance;
using Core.Application.Contracts.Persistance.Common;
using Persistance.Repositories;
using Persistance.Context;
using Domain.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Microsoft.AspNetCore.Identity;

namespace Persistance
{
    public static class PersistanceServicesRegistration
    {
        public static IServiceCollection ConfigurePersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
             services.AddDbContext<TheContext>(options => {
        options.UseLazyLoadingProxies().UseNpgsql(configuration.GetConnectionString("VisitMyAppartDatabase"), b => b.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
        });

        services.AddIdentity<User, CustomIdentityRole>().AddEntityFrameworkStores<TheContext>().AddDefaultTokenProviders();


        services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

        services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IOfferRepository, OfferRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IVisitorRepository, VisitorRepository>();
            services.AddScoped<IBitRepository, BitRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
            services.AddScoped<IRegionRepository, RegionRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            return services;
        }
    }
}
