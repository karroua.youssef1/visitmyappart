﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class correctionRegion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionsId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "PrefferedRegionsId",
                table: "AspNetUsers",
                newName: "PrefferedRegionId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_PrefferedRegionsId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_PrefferedRegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionId",
                table: "AspNetUsers",
                column: "PrefferedRegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "PrefferedRegionId",
                table: "AspNetUsers",
                newName: "PrefferedRegionsId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_PrefferedRegionId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_PrefferedRegionsId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionsId",
                table: "AspNetUsers",
                column: "PrefferedRegionsId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
