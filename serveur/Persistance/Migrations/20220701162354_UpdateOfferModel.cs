﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class UpdateOfferModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Offers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LimitDate",
                table: "Offers",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "PriceOffered",
                table: "Offers",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "link",
                table: "Offers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "y",
                table: "Offers",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "Bits",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PriceOffered",
                table: "Bits",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "LimitDate",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "PriceOffered",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "link",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "y",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "Bits");

            migrationBuilder.DropColumn(
                name: "PriceOffered",
                table: "Bits");
        }
    }
}
