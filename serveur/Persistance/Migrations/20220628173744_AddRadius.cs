﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class AddRadius : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Regions_AspNetUsers_UserId",
                table: "Regions");

            migrationBuilder.DropIndex(
                name: "IX_Regions_UserId",
                table: "Regions");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Regions");

            migrationBuilder.AddColumn<double>(
                name: "radius",
                table: "Regions",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "PrefferedRegionsId",
                table: "AspNetUsers",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_PrefferedRegionsId",
                table: "AspNetUsers",
                column: "PrefferedRegionsId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionsId",
                table: "AspNetUsers",
                column: "PrefferedRegionsId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Regions_PrefferedRegionsId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_PrefferedRegionsId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "radius",
                table: "Regions");

            migrationBuilder.DropColumn(
                name: "PrefferedRegionsId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Regions",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Regions_UserId",
                table: "Regions",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Regions_AspNetUsers_UserId",
                table: "Regions",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
