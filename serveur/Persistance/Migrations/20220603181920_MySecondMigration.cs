﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistance.Migrations
{
    public partial class MySecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bits_Offers_OfferId",
                table: "Bits");

            migrationBuilder.DropForeignKey(
                name: "FK_Bits_Visitors_VisitorId",
                table: "Bits");

            migrationBuilder.DropForeignKey(
                name: "FK_Visitors_Reviews_ReviewId",
                table: "Visitors");

            migrationBuilder.DropIndex(
                name: "IX_Visitors_ReviewId",
                table: "Visitors");

            migrationBuilder.DropIndex(
                name: "IX_Bits_OfferId",
                table: "Bits");

            migrationBuilder.DropColumn(
                name: "OfferId",
                table: "Bits");

            migrationBuilder.AddColumn<bool>(
                name: "isDeal",
                table: "Offers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "VisitorId",
                table: "Bits",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OffreId",
                table: "Bits",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Bits_OffreId",
                table: "Bits",
                column: "OffreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bits_Offers_OffreId",
                table: "Bits",
                column: "OffreId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Bits_Visitors_VisitorId",
                table: "Bits",
                column: "VisitorId",
                principalTable: "Visitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bits_Offers_OffreId",
                table: "Bits");

            migrationBuilder.DropForeignKey(
                name: "FK_Bits_Visitors_VisitorId",
                table: "Bits");

            migrationBuilder.DropIndex(
                name: "IX_Bits_OffreId",
                table: "Bits");

            migrationBuilder.DropColumn(
                name: "isDeal",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "OffreId",
                table: "Bits");

            migrationBuilder.AlterColumn<int>(
                name: "VisitorId",
                table: "Bits",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "OfferId",
                table: "Bits",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visitors_ReviewId",
                table: "Visitors",
                column: "ReviewId");

            migrationBuilder.CreateIndex(
                name: "IX_Bits_OfferId",
                table: "Bits",
                column: "OfferId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bits_Offers_OfferId",
                table: "Bits",
                column: "OfferId",
                principalTable: "Offers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bits_Visitors_VisitorId",
                table: "Bits",
                column: "VisitorId",
                principalTable: "Visitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Visitors_Reviews_ReviewId",
                table: "Visitors",
                column: "ReviewId",
                principalTable: "Reviews",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
