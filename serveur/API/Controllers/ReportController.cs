using Core.Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Core.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Application.Features.Reports.Requests.Commands;
using Core.Application.Features.Reports.Requests.Queries;
using Core.Application.Helpers;
using Microsoft.AspNetCore.Http;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
  //  [Authorize]
    public class ReportController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ReportController(IMediator mediator )
        {
            _mediator = mediator;
        }
 
        // POST api/Report/Create
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<Response<bool>>> Create()
        {
            ReportDto  model=GeneralHelper.DeserializeToModel<ReportDto>(Request.Form["ReportDto"]);
            var files = Request.Form.Files;
           model.FileAttachments=GeneralHelper.mapFilesFormtoReportFilesDto((List<IFormFile>)files);

            var command = new CreateReportCommand { report = model };
            var response = await _mediator.Send(command);   
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }

            
        }

        [HttpPut]
        [Route("AcceptReport")]
        [DisableRequestSizeLimit] 
        public async Task<ActionResult<BitDto>> AcceptReport([FromBody] BitDto model)
        {
               var command = new AcceptReportCommand { bit = model };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }

            
        }


        [HttpPut]
        [Route("RejectReport")]
        [DisableRequestSizeLimit] 

        public async Task<ActionResult<BitDto>> RejectReport([FromBody] BitDto model)
        {
               var command = new RejectReportCommand { bit = model };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }

            
        }

        [HttpGet]
        [Route("DownloadReportFiles/{id}")]
        [DisableRequestSizeLimit] 
        public async Task<ActionResult<Response<List<ReportFileDto>>>> DownloadReportFiles(int id)
        {
               var command = new DownloadReportFilesRequest { idReport = id };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }

            
        }
        

}
}
