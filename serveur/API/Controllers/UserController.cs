using Core.Application.DTOs;
using Core.Application.Features.Account.Requests.Commands;
using Core.Application.Features.Account.Requests.Queries;
using MediatR;
using Core.Application.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Core.Application.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
  
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;

        }

[HttpPost]
[Route("Register")]
public async Task<ActionResult<bool>> Register(UserDto userSent)
        {
            var isRegister = await _mediator.Send(new RegisterUserCommand(){user=userSent});
           
            return Ok(isRegister);

           
        }
[HttpPost]
[Route("Login")]
public async Task<ActionResult<UserDto>> Login(ConnexionModel model )
        {
            var user = await _mediator.Send(new LoginUserRequest(){Email=model.Email,Password=model.Password});
           
            if(user != null )
            {
           return Ok(user);

            }
            else
            {
                return NotFound("Email or Password incorrect ");
            }
        }


[HttpGet]
[Route("GetUserById/{id}")]
public async Task<ActionResult<UserDto>> GetUserById(int id )
        {
            Response<UserDto> user = await _mediator.Send(new GetUserByIdRequest(){idUser=id});
            if(user.isOk)
            {
           return Ok(user);

            }
            else
            {
                return NotFound(user.Message);
            }
        }

     [HttpPost]
[Route("AddDesc")]
//[Authorize("Visitor")]
public async Task<ActionResult<UserDto>> addRegionAndDescription([FromForm]AddDescriptionAndRegionCommand model )
        {

      model=GeneralHelper.DeserializeToModel<AddDescriptionAndRegionCommand>(Request.Form["AddDescriptionAndRegionCommand"]);
      var files = Request.Form.Files;
           model.profilPicture=GeneralHelper.mapFileFormtoProfilPictureDto((List<IFormFile>)files,model.id);
            var resultat = await _mediator.Send(model);
            if(resultat!=null)
            {
           return Ok(resultat);

            }
            else
            {
                return NotFound("Probleme lors de l'enregistrement");
            }
        } 

 [HttpPost]
[Route("EditGeneralInfos")]
public async Task<ActionResult<UserDto>> EditGeneralInformations(EditGeneralInformationsCommand model )
        {
            var resultat = await _mediator.Send(model);
            if(resultat!=null)
            {
           return Ok(resultat);

            }
            else
            {
                return NotFound("Probleme lors de l'enregistrement");
            }
        } 

        [HttpGet]
        [Route("GetHomeVisitors")]
        public async Task<ActionResult<List<UserDto>>> GetHomeVisitors()
        {

        Response<List<UserDto>> response = await _mediator.Send(new GetHomeVisitorsRequest());
                    if(response.isOk)
                    {
                return Ok(response.ResponseModel);

                    }
                    else
                    {
                        return NotFound(response.Message);
                    }
        }
[HttpPost]
[Route("UpdateUser")]
      public async Task<ActionResult<Response<bool>>> UpdateUser(UserDto newUser)
      {

           var command = new UpdateUserCommand {user=newUser};
            var response = await _mediator.Send(command);  
               return Ok(response);
      }
   
      
    }
}
