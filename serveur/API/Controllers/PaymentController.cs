using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Core.Application.Models;
using Core.Application.Features.Payments.Requests.Commands;
using Core.Application.Features.Payments.Requests.Queries;
using Core.Application.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
   // [Authorize]
    public class PaymentController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PaymentController(IMediator mediator)
        {
            _mediator = mediator;
        }

         [HttpPost("AddCustomer")]
        public async Task<ActionResult<Response<UserDto>>> AddCustomer(
        AddCustomer customer)
        {
            var command = new AddOrUpdateCustomerCommand {addCustomer=customer};
            var response = await _mediator.Send(command);  
               return Ok(response);

        }

        [HttpPost("add")]
        public async Task<ActionResult<Response<bool>>> AddPayment(
            AddPayment payment
            )
        {
            var command = new AddPaymentCommand {addPayment=payment};
            var response = await _mediator.Send(command);  
               return Ok(response);
        }
        [HttpGet("GetCard/{id}")]
        public async Task<ActionResult<Response<AddPaymentCard>>> GetCreditCardInfo(
            int id
            )
        {
            var command = new GetCreditCardInfoRequest {id=id};
            var response = await _mediator.Send(command);  
               return Ok(response);
        }

        [HttpGet("GetRedirectUrl/{id}")]
        public async Task<ActionResult<Response<string>>> GetRedirectUrl(
        string id)
        {
            var command = new AddAccountLinkCommand {userId=id};
            var response = await _mediator.Send(command);  
               return Ok(response);

        }
         [HttpGet("IsUserEnrolledCorrectly/{id}")]
        public async Task<ActionResult<Response<UserDto>>> IsUserEnrolledCorrectly(
        string id)
        {
            var command = new IsUserEnrolledCorrectlyRequest {userId=id};
            var response = await _mediator.Send(command);  
               return Ok(response);

        }
      
    }
}
