using Core.Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Core.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Features.Bits.Requests.Queries;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
   // [Authorize]
    public class BitController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BitController(IMediator mediator)
        {
            _mediator = mediator;
        }
 

 
       


        // POST api/Offer/Create
        [HttpPost]
       // [Authorize]
        [Route("Create")]
        public async Task<ActionResult<Response<bool>>> Create([FromBody] BitDto model)
        {
            var command = new CreateBitCommand { bit = model };
            var response = await _mediator.Send(command);   
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }

            
        }


        [HttpGet]
        [Route("GetBitsByOffer/{id}")]
        public async Task<ActionResult<List<BitDto>>> GetBitsByOffer(int id )
        {
            var command = new GetBitsByOfferRequest { idOffer = id };
            var response = await _mediator.Send(command);            
               return Ok(response);

            
        }


        [HttpGet]
        [Route("GetBit/{id}")]
        public async Task<ActionResult<BitDto>> GetBit(int id )
        {
            var command = new GetBitByIdRequest { id = id };
            var response = await _mediator.Send(command);            
               return Ok(response);

            
        }
        [HttpGet]
        [Route("GetBitsByUserId/{id}")]
        public async Task<ActionResult<BitDto>> GetBitsByUserId(int id )
        {
            var command = new GetBitsByUserIdRequest { id = id };
            var response = await _mediator.Send(command);            
               return Ok(response);         
        }

        
       

          //PUT api/Bit/RejectBit
        [HttpPut]
        [Route("RejectBit")]
       // [Authorize]

        public async Task<ActionResult<Response<bool>>> RejectBit([FromBody] BitDto model)
        {
            var command = new RejectBitCommand { bit = model };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }
            
        }


             //PUT api/Bit/RejectBit
        [HttpPut]
        [Route("AcceptBit")]
       // [Authorize]
        public async Task<ActionResult<Response<bool>>> AcceptBit([FromBody] BitDto model)
        {
            var command = new AcceptBitCommand { bit = model };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }
            
        }


       //PUT api/Bit/RejectBit
        [HttpPut]
       // [Authorize]
        [Route("RespondToBit")]
        public async Task<ActionResult<Response<bool>>> RespondToBit([FromBody] BitDto model)
        {
            var command = new RespondToBitCommand { bit = model };
            var response = await _mediator.Send(command);
            if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }
            
        }

        // DELETE api/Bit/5
        [HttpDelete]
        [Route("Delete/{id}")]
       // [Authorize]
        public async Task<ActionResult<Response<bool>>> Delete(int id)
        {
            var command = new DeleteBitCommand { Id = id };
            
           var response= await _mediator.Send(command);
             if(response.isOk)   
            {     
               return Ok(response);
             } 
               else
               {
               return NotFound(response); 
              }
        }
    }
}
