using Core.Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Application.Features.Notification.Requests.Queries;
using Core.Application.Features.Notification.Requests.Commands;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]

    public class NotificationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public NotificationController(IMediator mediator )
        {
            _mediator = mediator;
        }
 

 
        // GET: api/<SubjectController> http://localhost:5000/Offer/GetOffer/2
        [Route("GetNotificationsByUserId/{id}")]
        [HttpGet]
  //[Authorize]

        public async Task<ActionResult<List<NotificationDto>>> GetNotificationsByUserId(int id)
        { 
            var command = new NotificationsByUserIdRequest { idUser = id };
            var response = await _mediator.Send(command);  
               return Ok(response);
           // var offers = await _mediator.Send(new GetOffersListRequest());
        }

         [HttpPut]
        public async Task<ActionResult<bool>> ChangeState(int id)
        { 
            var command = new ChangeStateCommand { idNotif = id };
            var response = await _mediator.Send(command);  
               return Ok(response);
           // var offers = await _mediator.Send(new GetOffersListRequest());
        }

        [Route("SetNotificationsToRead")]
           [HttpPut]
        public async Task<ActionResult<bool>> SetNotificationsToRead([FromBody] int id)
        { 
            var command = new SetNotificationsToReadCommand { idUser = id };
            var response = await _mediator.Send(command);  
               return Ok(response);
           // var offers = await _mediator.Send(new GetOffersListRequest());
        }

    }
}
