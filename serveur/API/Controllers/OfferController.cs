using Core.Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Application.Features.Offers.Requests.Commands;
using Core.Application.Features.Offers.Requests.Queries;
using Core.Application.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
   // [Authorize]
    public class OfferController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OfferController(IMediator mediator )
        {
            _mediator = mediator;
        }
 

 
        // GET: api/<SubjectController> http://localhost:5000/Offer/GetOffer/2
        [Route("GetOffer/{id}")]
        [HttpGet]
        public async Task<ActionResult<OfferDto>> GetOffer(int id)
        { 
            var command = new GetOfferByIdRequest { idOffer = id };
            var response = await _mediator.Send(command);  
               return Ok(response);
           // var offers = await _mediator.Send(new GetOffersListRequest());
        }

        // GET: api/<SubjectController> http://localhost:5000/Offer/GetOffer/2
        [Route("GetOffersByClientId/{id}")]
        [HttpGet]
        public async Task<ActionResult<List<OfferDto>>> GetOffersByClientId(int id)
        { 
            var command = new GetOffersByClientIdRequest { idUser = id };
            var response = await _mediator.Send(command);  
               return Ok(response);
        }
        // GET: api/<SubjectController>
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult<List<OfferDto>>> GetAll()
        { 
            var command = new GetAllOffersRequest();
            var response = await _mediator.Send(command);  
               return Ok(response);          
        }


        [HttpGet]
        [Route("GetHomeOffers")]
        public async Task<ActionResult<List<UserDto>>> GetHomeOffers()
        {

        Response<List<OfferDto>> response = await _mediator.Send(new GetHomeOffersRequest());
                    if(response.isOk)
                    {
                return Ok(response.ResponseModel);

                    }
                    else
                    {
                        return NotFound(response.Message);
                    }
        }


        // POST api/Offer/Create
        [HttpPost]
        [Route("Create")]
       // [Authorize]
        public async Task<ActionResult<int>> Create([FromBody] OfferDto model)
        {
            var command = new CreateOfferCommand { offer = model };
            var repsonse = await _mediator.Send(command);
               return Ok(repsonse);
        }

          // POST api/Offer/Create
        [HttpPost]
        [Route("Update")]
       // [Authorize]
        public async Task<ActionResult<int>> Update([FromBody] OfferDto model)
        {
            var command = new UpdateOfferCommand { offerDto = model };
            var repsonse = await _mediator.Send(command);
            
            
               return Ok(repsonse);

            
        }

    }
}
