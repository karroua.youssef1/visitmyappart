using Core.Application.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Core.Application.Models;
using System.Threading.Tasks;
using Core.Application.Features.Reviews.Requests.Commands;
using Persistance.Context;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
   // [Authorize]
    public class ReviewController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ReviewController(IMediator mediator , TheContext context)
        {
            _mediator = mediator;
        }

        // POST api/Review/Create
        [HttpPost]
      //  [Authorize]
        [Route("Create")]
        public async Task<ActionResult<Response<int>>> Create([FromBody] ReviewDto model)
        {
            var command = new CreateReviewCommand { review = model };
            var response = await _mediator.Send(command);   
            if(response.isOk)   
            {     
               return Ok(response);
             } 
            else
            {
               return NotFound(response); 
            }

            
        }


    }
}
