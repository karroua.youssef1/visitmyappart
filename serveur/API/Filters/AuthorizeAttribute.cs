using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
     private readonly string[] allowedroles;  
        public AuthorizeAttribute(params string[] roles)  
        {  
            this.allowedroles = roles;  
        }  
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        try{
            string role="";
        bool isConnected = (bool)context.HttpContext.Items["IsAuthorized"];
        if(context.HttpContext.Items["Role"]!=null)
        {
        role= (string)context.HttpContext.Items["Role"];
       
        }

        if (!isConnected || !this.isAuhtorizedRole(role))
        {
            // not logged in
            context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
        
        }
        catch
            {

           context.Result = new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };

            }
    }
    private bool isAuhtorizedRole(string role)
    {
        bool result=false;
  if(allowedroles.Length==0)
  {
    result= true;
  }
 foreach (var r in allowedroles)  
        {  
        if (r.ToLower() == role.ToLower()) result=true;  
        }  

        return result;
    }
}