using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  MediatR;
using Core.Application.DTOs;
using Core.Application.Features.Account.Requests.Queries;


namespace API.Middleware
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
         private readonly IMediator _mediator;


        public JwtMiddleware(RequestDelegate next ,IMediator mediator)
        {
            _next = next;
            _mediator=mediator;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                await AttachUserToContext(context, token);

            await _next(context);
        }

        private async Task AttachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("superSecretKey@345");
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                int userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
                 var response = await _mediator.Send(new GetUserByIdRequest(){idUser=userId});
                  UserDto user=response.ResponseModel;
                // attach user to context on successful jwt validation
                context.Items["IsAuthorized"] = true;
                context.Items["Role"] = user.Role;

            }
            catch
            {             context.Items["IsAuthorized"] = false;
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}