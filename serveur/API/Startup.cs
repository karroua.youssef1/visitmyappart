using API.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Persistance;
using Infrastructure;
using Core.Application;
using NotificationServer;
using Notification.Models;

namespace serveur
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
                    services.ConfigureApplicationServices(Configuration);
            services.AddControllers().AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);

            services.ConfigurePersistenceServices(Configuration);
            services.ConfigureNotificationServices();
            services.ConfigureInfrastructureServices(Configuration);
            services.AddCors(options =>
    {
          options.AddPolicy("AllowAllHeaders",
                builder =>
            {
                    builder.WithOrigins("http://localhost:4200/")
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
    });
    services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {     
            // This middleware serves generated Swagger document as a JSON endpoint
             app.UseSwagger();
             // This middleware serves the Swagger documentation UI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseMiddleware<JwtMiddleware>();

              //  app.UseCors(p=>p.AllowAnyHeader().AllowAnyHeader().AllowAnyOrigin());
         //   app.UseCors(config => config.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseCors(builder =>
                {
                    builder.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .WithMethods("GET", "POST","PUT","DELETE")
                        .AllowCredentials();
                });            //app.UseCors("http://localhost:4200/");
           //  app.UseAuthentication();

             // app.UseCors("AllowAllHeaders");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

          //  app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            //  app.UseSignalR(builder =>  
            // {  
            //     builder.MapHub<NotificationHub>("/notify");  
            // }); 

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notify");  
            });

            

        }
    }
}
