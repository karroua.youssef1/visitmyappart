using Microsoft.AspNetCore.SignalR; 
using Core.Application.Contracts.Notifications;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Notification.Models  
{  
    public class NotificationHub : Hub,IHubClient  
    {  
        private readonly IPresenceTracker _tracker;
        public NotificationHub(IPresenceTracker tracker)
        {
            _tracker = tracker;
        }

   public async Task BroadcastNotification(Domain.Notification notif)
   {
      await Clients.All.SendAsync("broadcastnotification", notif);

   }

   public async Task BroadcastNotificationToUser(Domain.Notification notification)
   {
    List<string> connectionIds=_tracker.GetConnectionsForUser((int)notification.ReceiverId).Result;
    if(connectionIds != null)
    {
    await Clients.Clients(connectionIds).SendAsync("broadcastnotificationToUser", notification);
    }
   }

   public async Task BroadcastNotificationToSpecificUsers(Domain.Notification notif,List<int> userIds)
   { 
        List<string> connectionIds = new List<string>();
        foreach (int item in userIds)
        {
            connectionIds.Add(_tracker.GetConnectionsForUser(item).Result.FirstOrDefault());
        }

    await Clients.Clients(connectionIds).SendAsync("broadcastnotificationToUser", notif);
   }

      public  async Task OnConnectedAsync(int id)
        {
            var isOnline = await _tracker.UserConnected(id, Context.ConnectionId);
            var currentUsers = await _tracker.GetOnlineUsers();
            await Clients.Caller.SendAsync("GetOnlineUsers", currentUsers);
        }

        public  async Task OnDisconnectedAsync(int id)
        {
             await _tracker.UserDisconnected(id, Context.ConnectionId);
             Exception ex = new Exception();
            await base.OnDisconnectedAsync(ex);
        }
    }  
}  