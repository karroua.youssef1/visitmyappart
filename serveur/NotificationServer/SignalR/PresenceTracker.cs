using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Application.Contracts.Notifications;

namespace Notification.Models
{
    public class PresenceTracker : IPresenceTracker
    {
        private static readonly Dictionary<int, List<string>> OnlineUsers =
            new Dictionary<int, List<string>>();

        public Task<bool> UserConnected(int idUser, string connectionId)
        {
            bool isOnline = false;
            lock (OnlineUsers)
            {
                if (OnlineUsers.ContainsKey(idUser))
                {
                    OnlineUsers[idUser].Add(connectionId);
                }
                else
                {
                    OnlineUsers.Add(idUser, new List<string> { connectionId });
                    isOnline = true;
                }
            }

            return Task.FromResult(isOnline);
        }

        public Task<bool> UserDisconnected(int idUser, string connectionId)
        {
            bool isOffline = false;
            lock (OnlineUsers)
            {
                if (!OnlineUsers.ContainsKey(idUser)) return Task.FromResult(isOffline);

                OnlineUsers[idUser].Remove(connectionId);
                if (OnlineUsers[idUser].Count == 0)
                {
                    OnlineUsers.Remove(idUser);
                    isOffline = true;
                }
            }

            return Task.FromResult(isOffline);
        }

        public Task<int[]> GetOnlineUsers()
        {
            int[] onlineUsers;
            lock (OnlineUsers)
            {
                onlineUsers = OnlineUsers.OrderBy(k => k.Key).Select(k => k.Key).ToArray();
            }

            return Task.FromResult(onlineUsers);
        }

        public  Task<List<string>> GetConnectionsForUser(int idUser)
        {
            List<string> connectionIds;
            lock (OnlineUsers)
            {
                connectionIds = OnlineUsers.GetValueOrDefault(idUser);
            }

            return Task.FromResult(connectionIds);
        }
    }
}