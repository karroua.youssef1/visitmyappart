
using Microsoft.Extensions.DependencyInjection;
using Core.Application.Contracts.Notifications;
using Notification.Models;

namespace NotificationServer
{
    public static class NotificationServicesRegistration
    {
        public static IServiceCollection ConfigureNotificationServices(this IServiceCollection services)
        {
            services.AddSignalR();  
            services.AddScoped<IHubClient, NotificationHub>();
            services.AddSingleton<IPresenceTracker,PresenceTracker>();

            return services;
        }
    }
}
