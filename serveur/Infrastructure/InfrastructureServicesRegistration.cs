using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Stripe;

namespace Infrastructure
{
    public static class InfrastructureServicesRegistration
    {
        public static IServiceCollection ConfigureInfrastructureServices(this IServiceCollection services,IConfiguration configuration)
        {
            StripeConfiguration.ApiKey =configuration.GetValue<string>("AppSettings:SecretKey");
            services.AddScoped<IPaymentService, StripeService>()
                .AddScoped<CustomerService>()
				.AddScoped<ChargeService>()
				.AddScoped<TokenService>()
                .AddScoped<CardService>()
                .AddScoped<PayoutService>()
                .AddScoped<AccountService>()
                .AddScoped<AccountLinkService>()
                .AddScoped<TransferService>();
                
            return services;
        }
    }
}
