using System;
using System.Threading;
using System.Threading.Tasks;
using Stripe;

public class StripeService : IPaymentService
{
        private readonly ChargeService _chargeService;
        private readonly CardService _cardService;
        private readonly CustomerService _customerService;
        private readonly TokenService _tokenService;
        private readonly TransferService _transferService;
        private readonly AccountService _accountService;
        private readonly AccountLinkService _accountLinkService;

		public StripeService(ChargeService chargeService,CustomerService customerService,TokenService tokenService,CardService cardService,
        TransferService transferService,AccountService accountService,AccountLinkService accountLinkService)
		{
            _chargeService = chargeService;
            _customerService = customerService;
            _tokenService = tokenService;
            _cardService=cardService;
            _transferService=transferService;
            _accountService=accountService;
            _accountLinkService=accountLinkService;
		}

         /// <summary>
        /// Create a new customer at Stripe through API using customer and card details from records.
        /// </summary>
        /// <param name="customer">Stripe Customer</param>
        /// <param name="ct">Cancellation Token</param>
        /// <returns>Stripe Customer</returns>
        public async  Task<PaymentCustomer> AddCustomerAsync(AddCustomer customer, CancellationToken ct)
        { 
        try {
        // Set Stripe Token options based on customer data
            TokenCreateOptions tokenOptions = new TokenCreateOptions
            {
                Card = new TokenCardOptions
                {
                    Name = customer.Name,
                    Number = customer.CreditCard.CardNumber,
                    ExpYear = customer.CreditCard.ExpirationYear,
                    ExpMonth = customer.CreditCard.ExpirationMonth,
                    Cvc = customer.CreditCard.Cvc
                }
            };

            // Create new Stripe Token
            Token stripeToken = await _tokenService.CreateAsync(tokenOptions, null, ct);

            // Set Customer options using
            CustomerCreateOptions customerOptions = new CustomerCreateOptions
            {
                Name = customer.Name,
                Email = customer.Email,
                Source = stripeToken.Id
            };

            // Create customer at Stripe
            Customer createdCustomer = await _customerService.CreateAsync(customerOptions, null, ct);
            // Return the created customer at stripe
            return new PaymentCustomer(createdCustomer.Name, createdCustomer.Email,createdCustomer.Id);   
                 }

                 catch(Exception e)
                    {
                        throw ;
                    }
                 }

        /// <summary>
        /// Add a new payment at Stripe using Customer and Payment details.
        /// Customer has to exist at Stripe already.
        /// </summary>
        /// <param name="payment">Stripe Payment</param>
        /// <param name="ct">Cancellation Token</param>
        /// <returns><Stripe Payment/returns>
        public async  Task<Payment> AddPaymentAsync(AddPayment payment, CancellationToken ct)
        {
            // Set the options for the payment we would like to create at Stripe
                ChargeCreateOptions paymentOptions = new ChargeCreateOptions {
                    Customer = payment.CustomerId,
                    ReceiptEmail = payment.ReceiptEmail,
                    Description = payment.Description,
                    Currency = payment.Currency,
                    Amount = payment.Amount*100
                };

                // Create the payment
                var createdPayment = await _chargeService.CreateAsync(paymentOptions, null, ct);

                // Return the payment to requesting method
                return new Payment(
                createdPayment.CustomerId,
                createdPayment.ReceiptEmail,
                createdPayment.Description,
                createdPayment.Currency,
                createdPayment.Amount,
                createdPayment.Id);   
                     }

        public async Task<AddPaymentCard> GetCardInfosFromClientIdAsync(string clientId, CancellationToken ct)
        {
            var result =await  _customerService.GetAsync(clientId,null,null,ct);
            var card= await _cardService.GetAsync(clientId,result.DefaultSourceId,null,null,ct);
            return new AddPaymentCard{CardNumber="************"+card.Last4,
            ExpirationMonth=card.ExpMonth.ToString(),
            ExpirationYear=card.ExpYear.ToString(),
            Name=card.Name,
            Cvc="***"};

        }

public async  Task<AddPaymentCard> UpdateCustomerCardAsync(string clientId,AddPaymentCard newCardInfos, CancellationToken ct)
{
    try {
 // Set Stripe Token options based on customer data
            TokenCreateOptions tokenOptions = new TokenCreateOptions
            {
                Card = new TokenCardOptions
                {
                    Name = newCardInfos.Name,
                    Number = newCardInfos.CardNumber,
                    ExpYear = newCardInfos.ExpirationYear,
                    ExpMonth = newCardInfos.ExpirationMonth,
                    Cvc = newCardInfos.Cvc
                }
            };

            // Create new Stripe Token
            Token stripeToken = await _tokenService.CreateAsync(tokenOptions, null, ct);

           var customer =await  _customerService.GetAsync(clientId,null,null,ct);

            // Set Customer options using
            CustomerUpdateOptions customerOptions = new CustomerUpdateOptions
            {
                Name = customer.Name,
                Email = customer.Email,
                Source = stripeToken.Id
            };

            var result= _customerService.UpdateAsync(customer.Id,customerOptions,null,ct);
            return new AddPaymentCard{CardNumber="************"+newCardInfos.CardNumber.Substring(13),ExpirationMonth=newCardInfos.ExpirationMonth,
             ExpirationYear=newCardInfos.ExpirationYear,Name=newCardInfos.Name, Cvc="***"};
            }
            catch(Exception)
            {
                throw ;
            }

}

public async Task<bool> PayOutMoney(string chargeId,string destionationAccountId,string amount,string description, CancellationToken ct)
{
    
    try {
        var options = new TransferCreateOptions
            {
            Amount = (long?)(long.Parse(amount)*100*0.95),
            Currency = "cad",
            SourceTransaction=chargeId,
            Destination = destionationAccountId,
            };
        var resultat= await _transferService.CreateAsync(options);
    return true;
       }
    catch(Exception)
    {
        return false;
    }
}

public async Task<string> AddAccountAsync(AddStripeAccount account, CancellationToken ct)
{
try {
        var options = new AccountCreateOptions
        {
        Type = account.Type,
        Country = "CA",
        Email = account.Email,
        DefaultCurrency="CAD",
        BusinessType="individual",
        Individual= new AccountIndividualOptions 
        {
            Email=account.Email,
            FirstName=account.FirstName,
            LastName=account.LastName,
            Gender=account.Gender
        },
        Capabilities = new AccountCapabilitiesOptions
        {
            CardPayments = new AccountCapabilitiesCardPaymentsOptions
            {
            Requested = true,
            },
            Transfers = new AccountCapabilitiesTransfersOptions
            {
            Requested = true,
            },
        },
        };
        var result=await _accountService.CreateAsync(options,null,ct);
        return result.Id;
    }
    catch(Exception)
    {
        throw ;
    }
}

public async Task<string> AddLinkAccountAsync(string accountId,ClientApp _clientApp, CancellationToken ct)
{
    try {
        AccountLinkCreateOptions options = new AccountLinkCreateOptions
        {
            Account=accountId,
            RefreshUrl=_clientApp.RefreshUrl,
            ReturnUrl=_clientApp.ReturnUrl,
            Type="account_onboarding"
        };
        var accountLink= await _accountLinkService.CreateAsync(options,null,ct);
        return accountLink.Url;
        }
        catch(Exception)
        {
            throw ;
        }
}

public async Task<bool> istheUserRegisteredCompletely(string accountId, CancellationToken ct)
{
    try 
    {     
    var account =await _accountService.GetAsync(accountId,null,null,ct);
    return account.DetailsSubmitted;
    }

  catch(Exception){throw ;}
}

}