
namespace Core.Application.DTOs
{
    public  class AdressPayPal
    {
     public string street_address {get;set;}
     public  string locality {get;set;}  
     public  string region {get;set;}    
     public  string postal_code {get;set;}    
     public  string country {get;set;}      
     }   

    }
