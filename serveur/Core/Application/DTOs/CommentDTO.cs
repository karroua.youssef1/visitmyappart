using Core.Application.DTOs.Common;
using Domain.Enums;

namespace Core.Application.DTOs
{
    public class CommentDto:BaseDto
    {
    public string Content {get;set;}
    public int SenderId {get;set;}
    public string SenderName {get;set;}
    public int ReceiverId {get;set;}
    public string  ReceiverName {get;set;}
    public CommentTypeEnum type {get;set;}

    }
}
