using System.Collections.Generic;
using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class VisitorDto:BaseDto
    {
        
        List<ReviewDto> reviews {get;set;}
        public List<RegionDto> PrefferedRegions {get;set;}

        public string  Description { get; set; }
    }
}
