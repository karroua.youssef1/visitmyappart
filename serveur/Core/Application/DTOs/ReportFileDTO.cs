using System;

namespace Core.Application.DTOs
{
    public  class ReportFileDto:FileDto
    {
     public int ReportId {get;set;}
     public  ReportDto Report {get;set;}
    public string Base64Bytes 
    {  
        get 
        {
          return Convert.ToBase64String(this.Bytes) ;
        }
     }    
    }
     

    }
