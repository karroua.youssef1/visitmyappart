using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class RegionDto:BaseDto
    {
        public double x {get;set;}

        public double y {get;set;}

        public double radius {get;set;}
            }
}
