using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public class CityDto : BaseDto
    {
        public double Lat {get;set;}
        public double Lng {get;set;}
        public string Name {get;set;}

    }
}
