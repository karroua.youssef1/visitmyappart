using System;

namespace Core.Application.DTOs
{
    public  class ProfilPictureDto:FileDto
    {
     public int UserId {get;set;}
     public virtual UserDto User {get;set;}    
     public string Base64Bytes {get {
      return Convert.ToBase64String(this.Bytes) ;
     }
     }   

    }
}
