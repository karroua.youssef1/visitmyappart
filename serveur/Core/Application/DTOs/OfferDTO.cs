using System;
using System.Collections.Generic;
using Core.Application.DTOs.Common;
using Domain.Enums;

namespace Core.Application.DTOs
{
    public  class OfferDto:BaseDto
    {
      public OfferDto()
      {
        Bits= new List<BitDto>();
      }
      public string Titre {get;set;}

      public AdressDto Adress {get;set;}
        public double x { get; set; }
        public double y { get; set; }
        public DateTime  LimitDate  {get;set;}
        public int PriceOffered {get;set;}
        public UserDto client {get;set;}
        public int numberOfBits {get {
          int result=0;
          result= Bits==null ? 0 :this.Bits.Count;
return result;
       }

        }   

        public UserDto visitor {get;set;}
        public string description  {get;set;}
        public string link {get;set;}

        public bool isDeal {get;set;}
        
       public List<BitDto> Bits {get;set;}   
        public OfferStateEnum State {get;set;}
     
    }
}
