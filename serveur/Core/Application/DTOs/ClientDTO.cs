using System.Collections.Generic;
using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class ClientDto:BaseDto
    {
       public List<OfferDto> offers {get;set;}

    }
}
