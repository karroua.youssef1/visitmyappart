using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class FileDto:BaseDto
    {
     public byte[] Bytes { get; set; }
     public string Name { get; set; }
     public string FileExtension { get; set; }
     public decimal Size { get; set; }
     public string Type {get;set;}
    }
}
