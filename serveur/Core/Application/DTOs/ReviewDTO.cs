using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class ReviewDto:BaseDto
    {
       public float Rating {get;set;}

        public string Comment {get;set;}

        public UserDto client {get;set;}
        public int ClientId {get;set;}

        public string ClientFullName {get {return client.Name;}}
        public UserDto visitor {get;set;}
        public int VisitorId {get;set;}

    }
}
