using System.Collections.Generic;
using Core.Application.DTOs.Common;
using Domain.Enums;

namespace Core.Application.DTOs
{
    public  class BitDto:BaseDto
    {
        public int OffreId {get;set;}
        public int VisitorId {get;set;}

       public int  ClientId {get;set;}

       public string VisitorName {get;set;}

       public ProfilPictureDto ProfilPic {get;set;}
       public BitStateEnum State {get;set;}
       public List<CommentDto> Comments {get;set;}
       public ReportDto Report {get;set;}
       public bool isThereAreport {get {
          bool result = Report==null ? false : true;
           return result;
       }}

    }
}
