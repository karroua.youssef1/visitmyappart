using System.Collections.Generic;

namespace Core.Application.DTOs
{
    public  class PayPalUserDTO
    {
     public string user_id {get;set;}
     public  string name {get;set;}  
     public  string given_name {get;set;}    
     public  string family_name {get;set;}    
     public  string payer_id {get;set;}    
     public  AdressPayPal adress {get;set;}    
     public  bool verified_account {get;set;}  
     public  List<PayPalEmail> emails {get;set;}    
  
     }   

    
}
