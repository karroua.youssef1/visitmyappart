using System.Collections.Generic;
using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class UserDto:BaseDto
    {
        public string UserName { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public CityDto City { get; set; }
        public string Country { get; set; }

        public string PhoneNumber {get;set;}

        public string Password {get;set;}

        public string Role {get;set;}

        public string Description {get;set;}

        public List<ReviewDto> ClientReviews {get;set;}
        public List<ReviewDto> VisitorReviews {get;set;}
        public RegionDto PrefferedRegion {get;set;}
        public  ProfilPictureDto profilPicture {get;set;}
        public  bool VisitingPreferencesCompleted {get;set;}
        public string CustomerStripeId {get;set;}
        public string AccountStripeId {get;set;}

         public bool PaymentInformationsCompleted {get;set;}

        public string Token {get;set;}

        public int ReviewsCount {get {
            if(VisitorReviews!=null) return VisitorReviews.Count;
            else {return 0;}
     }}
        public float ReviewAverage  {get { return getReviewAverage();  }}

        private float getReviewAverage()
        {
            float result=0;
            if(this.VisitorReviews!=null)
            {int i=0;
           foreach(var review in VisitorReviews)
           {
            result=result+review.Rating;
            i++;
           }
           result=result/i;
            }

            return result;
        }

    }
}
