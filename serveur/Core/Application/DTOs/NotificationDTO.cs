using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class NotificationDto:BaseDto
    {
      public string Titre {get;set;}

        public string Message {get;set;}
        public int SenderId {get;set;}

        public bool isSendAllUsers {get;set;}

        public int? ReceiverId {get;set;}
        
        public string link {get;set;}

        public bool isOpen {get;set;}     
    }
}
