using Core.Application.DTOs.Common;

namespace Core.Application.DTOs
{
    public  class AdressDto : BaseDto
    {
        public int OffreId {get;set;}
        public virtual OfferDto Offer {get;set;}
        public double x {get;set;}
        public double y {get;set;}
        public string AdressContent {get;set;}
        public string MapsUrl {get;set;}

    }
}
