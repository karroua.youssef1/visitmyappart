using System.Collections.Generic;
using Core.Application.DTOs.Common;
using Domain.Enums;

namespace Core.Application.DTOs
{
    public  class ReportDto:BaseDto
    {
        public string content {get;set;}
        public int senderId {get;set;}
        public List<ReportFileDto> FileAttachments {get;set;}
        public int bitId {get;set;}
        public ReportStateEnum state {get;set;}
        public int reportFilesExist {get;set;}
           
     }
}
