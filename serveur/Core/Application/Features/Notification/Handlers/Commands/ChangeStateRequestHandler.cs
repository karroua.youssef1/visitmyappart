using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Features.Notification.Requests.Commands;

namespace Core.Application.Features.Notification.Handlers.Commands
{
    public class ChangeStateRequestHandler : IRequestHandler<ChangeStateCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChangeStateRequestHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        public async Task<bool> Handle(ChangeStateCommand command, CancellationToken cancellationToken)
        {
            bool result=false;
            try
            {
             _unitOfWork.NotificationRepository.UpdateNotificationState(command.idNotif);
               await _unitOfWork.Save() ;
               result=true;
            }
            catch (System.Exception)
            {
                
                throw;
            }
           
              return result;  

            }
        }
    }

