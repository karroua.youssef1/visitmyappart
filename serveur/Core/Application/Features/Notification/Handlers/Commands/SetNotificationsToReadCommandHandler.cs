using AutoMapper;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Features.Notification.Requests.Commands;

namespace Core.Application.Features.Notification.Handlers.Commands
{
    public class SetNotificationsToReadCommandHandler : IRequestHandler<SetNotificationsToReadCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public SetNotificationsToReadCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        public async Task<bool> Handle(SetNotificationsToReadCommand command, CancellationToken cancellationToken)
        {
            bool result=false;
            try
            {
                 await _unitOfWork.NotificationRepository.UpdateNotificationsStateToRead(command.idUser);
               await _unitOfWork.Save() ;
               result=true;
            }
            catch (System.Exception)
            {
                
                throw;
            }
           
              return result;  

            }
        }
    }

