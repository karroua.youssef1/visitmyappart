using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Features.Notification.Requests.Queries;

namespace Core.Application.Features.Notification.Handlers.Queries
{
    public class NotificationsByUserIdRequestHandler : IRequestHandler<NotificationsByUserIdRequest, List<NotificationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;


        public NotificationsByUserIdRequestHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;

        }

        public async Task<List<NotificationDto>> Handle(NotificationsByUserIdRequest query, CancellationToken cancellationToken)
        {
            List<NotificationDto> resultat = new List<NotificationDto>(); 
            List<Domain.Notification> notifs = await _unitOfWork.NotificationRepository.GetNotificationsById(query.idUser);
                 _mapper.Map(notifs, resultat);

                 return resultat;  

            }
        }
    }

