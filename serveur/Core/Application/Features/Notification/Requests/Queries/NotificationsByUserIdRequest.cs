using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;

namespace Core.Application.Features.Notification.Requests.Queries
{
    public class NotificationsByUserIdRequest : IRequest<List<NotificationDto>>
    {
        public int idUser { get; set; }

    }
}
