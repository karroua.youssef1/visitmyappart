using MediatR;

namespace Core.Application.Features.Notification.Requests.Commands
{
    public class ChangeStateCommand : IRequest<bool>
    {
        public int idNotif { get; set; }

    }
}
