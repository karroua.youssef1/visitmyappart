using MediatR;

namespace Core.Application.Features.Notification.Requests.Commands
{
    public class SetNotificationsToReadCommand : IRequest<bool>
    {
        public int idUser { get; set; }

    }
}
