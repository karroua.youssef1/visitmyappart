using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;
using Core.Application.Models;

namespace Core.Application.Features.Account.Requests.Queries
{
    public class GetHomeVisitorsRequest : IRequest<Response<List<UserDto>>>
    {
    }
}
