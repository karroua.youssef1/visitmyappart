using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Account.Requests.Queries
{
    public class GetUserByIdRequest : IRequest<Response<UserDto>>
    {
        public int idUser { get; set; }

    }
}
