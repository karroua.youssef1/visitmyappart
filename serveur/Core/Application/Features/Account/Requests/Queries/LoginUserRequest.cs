using Core.Application.DTOs;
using MediatR;
namespace Core.Application.Features.Account.Requests.Queries
{
    public class LoginUserRequest : IRequest<UserDto>
    {
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
