using Core.Application.DTOs;
using MediatR;
using Newtonsoft.Json;

namespace Core.Application.Features.Account.Requests.Commands
{
    [JsonObject(Title="GeneralInfosModel")]

    public class EditGeneralInformationsCommand : IRequest<UserDto>
    {
        public int id { get; set; }
        public CityDto city {get;set;}
        public string country {get;set;}

    }
}
