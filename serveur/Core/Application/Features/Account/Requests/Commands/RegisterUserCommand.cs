using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Account.Requests.Commands
{
    public class RegisterUserCommand : IRequest<bool>
    {
        public UserDto user { get; set; }
    }
}
