using Core.Application.DTOs;
using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Account.Requests.Commands
{
    public class UpdateUserCommand : IRequest<Response<bool>>
    {
        public UserDto user { get; set; }       

    }
}
