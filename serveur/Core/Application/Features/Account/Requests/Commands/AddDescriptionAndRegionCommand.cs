using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Account.Requests.Commands
{
    public class AddDescriptionAndRegionCommand : IRequest<UserDto>
    {
        public int id { get; set; }
        public string description {get;set;}
        public RegionDto region {get;set;}
        public ProfilPictureDto profilPicture {get;set;}

    }
}
