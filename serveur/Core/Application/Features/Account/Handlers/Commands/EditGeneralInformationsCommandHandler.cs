using AutoMapper;
using Core.Application.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.Features.Account.Requests.Commands;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class EditGeneralInformationsCommandHandler : IRequestHandler<EditGeneralInformationsCommand, UserDto>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;


        public EditGeneralInformationsCommandHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(EditGeneralInformationsCommand command, CancellationToken cancellationToken)
        {
            UserDto response= new UserDto();
            var user = await _userManager.FindByIdAsync(command.id.ToString());
            if (user == null) {return null;}
            else      
            {            command.city.Id=user.City.Id;
                _mapper.Map(command.city,user.City);
               user.Country=command.country;
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {

                return null;

            }
            else
            {
                _mapper.Map(user,response);
             
              return response;

            }
            }
                
         
        }
    }
}
