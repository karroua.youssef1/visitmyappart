using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.Features.Account.Requests.Commands;
using Core.Application.Models;
using Core.Application.Helpers;
using System;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;


        public UpdateUserCommandHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(UpdateUserCommand command, CancellationToken cancellationToken)
        {
            var result=new Response<bool>();
            try {
            var userExists = await _userManager.FindByNameAsync(command.user.UserName);
            if (userExists != null)
                result=GeneralHelper.SetResponseResult<bool>(false,false,"User not found");
            User user = new User();
           _mapper.Map(command.user, user);
            var identity = await _userManager.UpdateAsync(user);
            if (!identity.Succeeded)
            {
            result=GeneralHelper.SetResponseResult<bool>(false,false,"Error while updating");
            }
            else
            {
            result=GeneralHelper.SetResponseResult<bool>(true,true,"user updated");

            }
            return result;
            }
            catch(Exception e)
            {
               return GeneralHelper.SetResponseResult<bool>(false,false,"error message :"+e.Message);
            }
        }
    }
}
