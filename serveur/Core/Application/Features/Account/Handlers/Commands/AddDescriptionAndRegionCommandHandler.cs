using AutoMapper;
using Core.Application.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain;
using Domain.Users;
using Domain.Files;
using Core.Application.Features.Account.Requests.Commands;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class AddDescriptionAndRegionCommandHandler : IRequestHandler<AddDescriptionAndRegionCommand, UserDto>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public AddDescriptionAndRegionCommandHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(AddDescriptionAndRegionCommand command, CancellationToken cancellationToken)
        {
            UserDto response= new UserDto();
            var user = await _userManager.FindByIdAsync(command.id.ToString());
            if (user == null)
            {return null;}
            else      
            {
                if( user.PrefferedRegion==null)
                {
                    user.PrefferedRegion= new Region();
                }
              
               user.Description=command.description;
               Region temp =user.PrefferedRegion;
               this.MapRegionValues(ref temp,command.region);
              if(command.profilPicture!=null)
              {
                  if(user.profilPicture==null)
                {
                    user.profilPicture = new ProfilPicture();
                }
               ProfilPicture tempPic = user.profilPicture;
              this.MapProfilPicValues(ref tempPic,command.profilPicture);
              }
              user.VisitingPreferencesCompleted=true;

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {

                return null;

            }
            else
            {
                _mapper.Map(user,response);
             
              return response;

            }
            }
                
         
        }

        private void MapRegionValues(ref Region region,RegionDto dto)
        {
               region.x = dto.x;
               region.y = dto.y;
               region.radius = dto.radius;
        }
        private void MapProfilPicValues(ref ProfilPicture profilpic,ProfilPictureDto dto)
        {
            profilpic.Bytes =dto.Bytes;
            profilpic.Name =dto.Name ;
            profilpic.FileExtension =dto.FileExtension ;
            profilpic.Size =dto.Size;
            profilpic.UserId=dto.UserId;
        }
    }
}
