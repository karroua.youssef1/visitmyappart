using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.Features.Account.Requests.Commands;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, bool>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<CustomIdentityRole> _roleManager;
        private readonly IPaymentService _paymentService;

        public RegisterUserCommandHandler(
             IMapper mapper,UserManager<User> userManager,RoleManager<CustomIdentityRole> roleManager,IPaymentService paymentService)
        {
            _userManager = userManager;
            _mapper = mapper;
            _roleManager = roleManager;
           _paymentService=paymentService;

        }

        public async Task<bool> Handle(RegisterUserCommand command, CancellationToken cancellationToken)
        {
            var userExists = await _userManager.FindByEmailAsync(command.user.Email);
            if (userExists != null)
                return false;

            User user = new User();
           _mapper.Map(command.user, user);
           if(user.Role.ToLower()=="visitor")
           {
           var addStripeAccount=new AddStripeAccount{Country=user.Country,Type="express",
           Email=user.Email,FirstName=user.FirstName,LastName=user.LastName,Gender=user.Gender};
          var accountStripeId= await _paymentService.AddAccountAsync(addStripeAccount,cancellationToken);
           user.AccountStripeId=accountStripeId;
           }
            var result = await _userManager.CreateAsync(user, command.user.Password);
            if (!result.Succeeded)
            {
                return false;

            }
            else
            {
             if (await _roleManager.RoleExistsAsync(command.user.Role))
               {
             await _userManager.AddToRoleAsync(user, command.user.Role); 
               }
              return true;

            }
        }
    }
}
