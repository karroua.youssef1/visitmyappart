using AutoMapper;
using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System;
using Domain.Users;
using Core.Application.Features.Account.Requests.Queries;
using Core.Application.Helpers;
using Core.Application.Models;
using System.Linq;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class GetHomeVisitorsRequestHandler : IRequestHandler<GetHomeVisitorsRequest, Response<List<UserDto>>>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public GetHomeVisitorsRequestHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;

        }

        public async Task<Response<List<UserDto>>> Handle(GetHomeVisitorsRequest query, CancellationToken cancellationToken)
        {
            
            Response<List<UserDto>> result = new Response<List<UserDto>>(); 
            try
            {
            var users =  _userManager.Users.Where(u=>u.Role=="Visitor").ToList();
            List<UserDto> usersDto = new List<UserDto>();
            if(users!=null)
            {    
                  _mapper.Map(users, usersDto);
                  usersDto=usersDto.OrderByDescending(u => u.ReviewAverage).Take(3).ToList();
                result= GeneralHelper.SetResponseResult(usersDto,true,"Home Visitors found");               
                      } 
            else{
              result= GeneralHelper.SetResponseResult<List<UserDto>>(null,false,"Home Visitors not found");
            }
            return result;
               }
                 catch(Exception e)
             
             {
              result= GeneralHelper.SetResponseResult<List<UserDto>>(null,false,e.Message);
                return result;
             }

            }
        }
    }

