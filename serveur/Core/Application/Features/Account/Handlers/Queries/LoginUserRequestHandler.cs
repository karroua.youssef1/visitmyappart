using AutoMapper;
using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.Features.Account.Requests.Queries;
using System.Security.Claims;
using Core.Application.Helpers;
using System;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class LoginUserRequestHandler : IRequestHandler<LoginUserRequest, UserDto>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;


        public LoginUserRequestHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(LoginUserRequest query, CancellationToken cancellationToken)
        {
            try {
            UserDto result = new UserDto(); 
            var user = await _userManager.FindByEmailAsync(query.Email);
            if(user!=null && await _userManager.CheckPasswordAsync(user, query.Password))
            {      
                 var myUser = await _userManager.FindByEmailAsync(user.Email);
                 var tokenString = GeneralHelper.GetToken(new List<Claim>(),myUser.Id);
               _mapper.Map(myUser, result);
               result.Token=tokenString;
                return result;
                          } 
            else{
                return null;
            }

            }
            catch(Exception e)

            {
                return null;
            }

            }
        }
    }

