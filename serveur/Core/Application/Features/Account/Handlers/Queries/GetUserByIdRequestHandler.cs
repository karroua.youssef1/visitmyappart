using AutoMapper;
using Core.Application.DTOs;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System;
using Domain.Users;
using Core.Application.Features.Account.Requests.Queries;
using Core.Application.Helpers;
using Core.Application.Models;

namespace Core.Application.Features.Account.Handlers.Commands
{
    public class GetUserByIdRequestHandler : IRequestHandler<GetUserByIdRequest, Response<UserDto>>
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public GetUserByIdRequestHandler(
             IMapper mapper,UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<Response<UserDto>> Handle(GetUserByIdRequest query, CancellationToken cancellationToken)
        {
            
            Response<UserDto> result = new Response<UserDto>(); 
            try
            {
            var user = await _userManager.FindByIdAsync(query.idUser.ToString());
            if(user!=null)
            {    UserDto temp= new UserDto();
                  _mapper.Map(user, temp);
                result= GeneralHelper.SetResponseResult<UserDto>(temp,true,"Utilisateur found");               
                      } 
            else{
              result= GeneralHelper.SetResponseResult<UserDto>(null,false,"Utilisateur introuvable");
            }
            return result;
               }
                 catch(Exception e)
             
             {
              result= GeneralHelper.SetResponseResult<UserDto>(null,false,e.Message);
                return result;
             }

            }
        }
    }

