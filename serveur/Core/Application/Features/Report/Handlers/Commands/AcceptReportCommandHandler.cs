using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Reports.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Domain;
using Core.Application.Models;
using System;
using Domain.Users;
using Domain.Enums;
using Core.Application.Helpers;

namespace Core.Application.Features.Reports.Handlers.Commands
{
    public class AcceptReportCommandHandler : IRequestHandler<AcceptReportCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        
        private readonly IPaymentService _paymentService ;


        public AcceptReportCommandHandler(IMapper mapper,UserManager<User> userManager,IUnitOfWork unitOfWork,IPaymentService paymentService)
        {
            _userManager = userManager;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _paymentService=paymentService;
        }

        public async Task<Response<bool>> Handle(AcceptReportCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;
             Report myReport= new Report(); 

            try{
                 var visitor = await _userManager.FindByIdAsync(command.bit.VisitorId.ToString());
               myReport=_mapper.Map(command.bit.Report,myReport);
             this.AcceptReportState(myReport); 
             Offer offer= await UpdateOfferState(command.bit.OffreId);  
            var isDone= await  _paymentService.PayOutMoney(offer.ChargeId,visitor.AccountStripeId,offer.PriceOffered.ToString(),"pay out",cancellationToken);
            if(!isDone) return   GeneralHelper.SetResponseResult(false,false,"A problem occured during payment,please try again later");
             Domain.Notification notif=GeneralHelper.CreateNotification<BitDto>("Report Accepted","The Report has been accepted",command.bit.VisitorId,offer.ClientId);
             if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
               result= GeneralHelper.SetResponseResult(true,true,"Report Accepted succefuly");
                }
            
           _unitOfWork.Dispose();
             return result;
             }
             catch(Exception e)
             
             {
              result= GeneralHelper.SetResponseResult(false,false,e.Message);
                return result;
             }
            
        }

        private void AcceptReportState(Report myReport)
        {
             myReport.State=ReportStateEnum.Accepted;
            _unitOfWork.ReportRepository.Update(myReport);        
        }

       async private Task<Offer> UpdateOfferState(int OffreId)
        {
                Offer offer = await _unitOfWork.OfferRepository.GetOfferById(OffreId);
                offer.State=OfferStateEnum.Done;
                _unitOfWork.OfferRepository.Update(offer);
                return offer;
        }

       
    }
}
