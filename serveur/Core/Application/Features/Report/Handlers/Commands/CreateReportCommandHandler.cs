using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Reports.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Models;
using System;

using Core.Application.Helpers;

namespace Core.Application.Features.Reports.Handlers.Commands
{
    public class CreateReportCommandHandler : IRequestHandler<CreateReportCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public CreateReportCommandHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(CreateReportCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;

            try{
                if(command.report.content==""|| command.report.FileAttachments.Count==0){ result= GeneralHelper.SetResponseResult<bool>(false,false,"report content is required and at least one file must be attached to the report ");}
                else
                {
            Report myReport = new Report();
            myReport=_mapper.Map(command.report,myReport);
            
            if(_unitOfWork.ReportRepository.Add(myReport)!=null)
            {
                Bit bit = await _unitOfWork.BitRepository.GetBitById(myReport.BitId);
            Domain.Notification notif = new Domain.Notification();
             notif=GeneralHelper.CreateNotification<BitDto>("Report Created","The Report has been created for your offer",bit.VisitorId,bit.Offer.ClientId);
             if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
                 result= GeneralHelper.SetResponseResult(true,true,"Report Created succefuly");
                }
            }
           _unitOfWork.Dispose();
           }
             return result;
             }
             catch(Exception e)
             
             {
               result= GeneralHelper.SetResponseResult(false,false,e.Message);
                return result;
             }
            
        }

       
    }
}
