using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Reports.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Models;
using System;
using Domain.Enums;
using Core.Application.Helpers;

namespace Core.Application.Features.Reports.Handlers.Commands
{
    public class RejectReportCommandHandler : IRequestHandler<RejectReportCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public RejectReportCommandHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(RejectReportCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;
               Report myReport= new Report(); 

            try{
                Bit myBit = Map(command.bit);
               myReport=_mapper.Map(command.bit.Report,myReport);
             UpdateBitAndComments(myBit);
             RejectReportState(myReport); 
             Offer offer= await UpdateOfferState(myBit.OffreId);    
             Domain.Notification notif=GeneralHelper.CreateNotification<BitDto>("Report Rejected","The Report has been rejected n our team will review this and come back yo you ",command.bit.VisitorId,offer.ClientId);
             if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
                result= GeneralHelper.SetResponseResult(true,true,"Report Rejected succefuly");
                }
           _unitOfWork.Dispose();
             return result;
             }
             catch(Exception e)
             
             {
            result= GeneralHelper.SetResponseResult(false,false,e.Message);

                return result;
             }
            
        }
            
        private Bit Map( BitDto dto)
        {
            Bit resultat = new Bit();
             _mapper.Map(dto, resultat);
             resultat.Visitor=null;
             resultat.Offer=null;
             return resultat;
        }

       async private void  UpdateBitAndComments(Bit myBit)
            {
                 _unitOfWork.BitRepository.Update(myBit);
                foreach (Comment item in myBit.Comments)
                {
                    if(item.Id==0 && item.Content !="")
                    {
                        await _unitOfWork.CommentRepository.Add(item);
                    }
                }
            }

        private void RejectReportState(Report myReport)
        {
             myReport.State=ReportStateEnum.Rejected;
            _unitOfWork.ReportRepository.Update(myReport);        
        }

       async private Task<Offer> UpdateOfferState(int OffreId)
        {
                Offer offer = await _unitOfWork.OfferRepository.GetOfferById(OffreId);
                offer.State=OfferStateEnum.Done;
                 _unitOfWork.OfferRepository.Update(offer);
                return offer;
        }

       
    }
}
