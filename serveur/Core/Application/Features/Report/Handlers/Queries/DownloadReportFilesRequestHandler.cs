using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Reports.Requests.Queries;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain.Files;
using Core.Application.Models;
using System;
using Core.Application.Helpers;



namespace Core.Application.Features.Reports.Handlers.Queries
{
    public class DownloadReportFilesRequestHandler : IRequestHandler<DownloadReportFilesRequest, Response<List<ReportFileDto>>>
    {
         private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public DownloadReportFilesRequestHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response<List<ReportFileDto>>> Handle(DownloadReportFilesRequest request, CancellationToken cancellationToken)
        {
             try
            {
            var reportFiles = new List<ReportFile>();
            var reportFilesDto = new List<ReportFileDto>();
                reportFiles = await _unitOfWork.ReportRepository.GetReportFilesByReportId(request.idReport);
                 _mapper.Map(reportFiles, reportFilesDto);
              return GeneralHelper.SetResponseResult(reportFilesDto,true,"report files found");               
            }
         catch(Exception e)
             
             {
              return GeneralHelper.SetResponseResult<List<ReportFileDto>>(null,false,e.Message);
             }
        }
    }
}
