using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Reports.Requests.Commands
{
    public class CreateReportCommand : IRequest<Response<bool>>
    {
        public ReportDto report { get; set; }

    }
}
