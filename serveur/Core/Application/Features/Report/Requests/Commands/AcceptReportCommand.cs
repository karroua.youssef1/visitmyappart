using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Reports.Requests.Commands
{
    public class AcceptReportCommand : IRequest<Response<bool>>
    {
        public BitDto bit { get; set; }

    }
}
