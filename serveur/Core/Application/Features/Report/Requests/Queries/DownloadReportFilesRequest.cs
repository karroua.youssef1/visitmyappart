using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;
using Core.Application.Models;

namespace Core.Application.Features.Reports.Requests.Queries
{
    public class DownloadReportFilesRequest : IRequest<Response<List<ReportFileDto>>>
    {
        public int idReport { get; set; }
    }
}
