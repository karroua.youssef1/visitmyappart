using AutoMapper;
using Core.Application.Features.Offers.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using System;

namespace Core.Application.Features.Offers.Handlers.Commands
{
    public class UpdateOfferCommandHandler : IRequestHandler<UpdateOfferCommand, bool>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public  UpdateOfferCommandHandler(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle( UpdateOfferCommand command, CancellationToken cancellationToken)
        { 
            try{
                  Offer myOffer = new Offer();
          _mapper.Map(command.offerDto, myOffer);
            myOffer.client=null;
             _unitOfWork.OfferRepository.Update(myOffer);
            _unitOfWork.AdressRepository.Update(myOffer.Adress);
            await _unitOfWork.Save();
            _unitOfWork.Dispose();
              return true;
            }
            catch(Exception)
            {
              return false;
            }
            
        }
    }
}
