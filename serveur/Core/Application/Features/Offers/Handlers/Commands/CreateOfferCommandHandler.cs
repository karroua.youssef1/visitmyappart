using AutoMapper;
using Core.Application.Features.Offers.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;

namespace Core.Application.Features.Offers.Handlers.Commands
{
    public class CreateOfferCommandHandler : IRequestHandler<CreateOfferCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOfferCommandHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Handle(CreateOfferCommand command, CancellationToken cancellationToken)
        { Offer myOffer = new Offer();
        command.offer.State=OfferStateEnum.Open;
        _mapper.Map(command.offer, myOffer);
        myOffer.client=null;
            var offer = _unitOfWork.OfferRepository.Add(myOffer);
            int result =0;
            if (offer != null)
            {
                await _unitOfWork.Save();
                 result=offer.Result.Id;

            }
           _unitOfWork.Dispose();
             return result;
        }
    }
}
