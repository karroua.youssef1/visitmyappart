using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Offers.Requests.Queries;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetAllOffersRequestHandler : IRequestHandler<GetAllOffersRequest, List<OfferDto>>
    {
         private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetAllOffersRequestHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }

        public async Task<List<OfferDto>> Handle(GetAllOffersRequest request, CancellationToken cancellationToken)
        {
            var offersDTO = new List<OfferDto>();
               var offers = await _unitOfWork.OfferRepository.GetAll() as List<Offer>;
                 _mapper.Map(offers, offersDTO);
                 return offersDTO;
        }
    }
}
