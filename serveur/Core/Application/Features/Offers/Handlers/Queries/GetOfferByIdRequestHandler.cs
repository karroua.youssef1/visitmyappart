using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Offers.Requests.Queries;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetOfferByIdRequestHandler : IRequestHandler<GetOfferByIdRequest, OfferDto>
    {
         private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetOfferByIdRequestHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }

        public async Task<OfferDto> Handle(GetOfferByIdRequest request, CancellationToken cancellationToken)
        {
            var offer = new Offer();
            var offerDTO = new OfferDto();

                offer = await _unitOfWork.OfferRepository.Get(request.idOffer);
                 _mapper.Map(offer, offerDTO);
                _mapper.Map(offer.Bits, offerDTO.Bits);
                 return offerDTO;
        }
    }
}
