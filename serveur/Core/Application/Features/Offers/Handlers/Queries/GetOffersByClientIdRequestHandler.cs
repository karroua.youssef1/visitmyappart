using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Offers.Requests.Queries;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetOffersByClientIdRequestHandler : IRequestHandler<GetOffersByClientIdRequest, List<OfferDto>>
    {
         private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetOffersByClientIdRequestHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }

        public async Task<List<OfferDto>> Handle(GetOffersByClientIdRequest request, CancellationToken cancellationToken)
        {
            var offers = new List<Offer>();
            var offersDTO = new List<OfferDto>();
                offers = await _unitOfWork.OfferRepository.GetClientOffers(request.idUser);
                 _mapper.Map(offers, offersDTO);
                 return offersDTO;
        }
    }
}
