using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Offers.Requests.Queries;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System;
using Core.Application.Helpers;
using Core.Application.Models;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetHomeOffersRequestHandler : IRequestHandler<GetHomeOffersRequest, Response<List<OfferDto>>>
    {
         private readonly IUnitOfWork _unitOfWork;

        private readonly IMapper _mapper;

        public GetHomeOffersRequestHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }

        public async Task<Response<List<OfferDto>>> Handle(GetHomeOffersRequest request, CancellationToken cancellationToken)
        { 
        try
        {
            var offersDTO = new List<OfferDto>();
               var offers = await _unitOfWork.OfferRepository.GetAnumberOfOffersOrderByDate(3);
                           _mapper.Map(offers, offersDTO);
               return GeneralHelper.SetResponseResult(offersDTO,true,"Report Accepted succefuly");
        }
        catch(Exception  e)
        {
            return GeneralHelper.SetResponseResult<List<OfferDto>>(null,false,e.Message);

        }
        }
      
    }
}
