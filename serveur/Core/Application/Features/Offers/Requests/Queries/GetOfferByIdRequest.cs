using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Offers.Requests.Queries
{
    public class GetOfferByIdRequest : IRequest<OfferDto>
    {
        public int idOffer { get; set; }
    }
}
