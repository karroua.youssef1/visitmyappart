using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;

namespace Core.Application.Features.Offers.Requests.Queries
{
    public class GetAllOffersRequest : IRequest<List<OfferDto>>
    {
        
    }
}
