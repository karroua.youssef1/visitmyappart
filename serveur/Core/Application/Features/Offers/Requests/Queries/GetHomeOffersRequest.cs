using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;
using Core.Application.Models;


namespace Core.Application.Features.Offers.Requests.Queries
{
    public class GetHomeOffersRequest : IRequest<Response<List<OfferDto>>>
    {
        
    }
}
