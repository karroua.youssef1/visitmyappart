using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;

namespace Core.Application.Features.Offers.Requests.Queries
{
    public class GetOffersByClientIdRequest : IRequest<List<OfferDto>>
    {
        public int idUser { get; set; }
    }
}
