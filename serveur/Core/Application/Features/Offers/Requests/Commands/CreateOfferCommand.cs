using Core.Application.DTOs;
using MediatR;


namespace Core.Application.Features.Offers.Requests.Commands
{
    public class CreateOfferCommand : IRequest<int>
    {
        public OfferDto offer { get; set; }

    }
}
