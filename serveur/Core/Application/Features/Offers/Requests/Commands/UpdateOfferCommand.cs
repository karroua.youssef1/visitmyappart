using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Offers.Requests.Commands
{
    public class UpdateOfferCommand : IRequest<bool>
    {
        public OfferDto offerDto { get; set; }

    }
}
