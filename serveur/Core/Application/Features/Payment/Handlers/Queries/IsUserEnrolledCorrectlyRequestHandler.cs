using Core.Application.Features.Payments.Requests.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System;
using Core.Application.Helpers;
using Core.Application.Models;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.DTOs;
using AutoMapper;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class IsUserEnrolledCorrectlyRequestHandler : IRequestHandler<IsUserEnrolledCorrectlyRequest, Response<UserDto>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;


        public IsUserEnrolledCorrectlyRequestHandler(UserManager<User> userManager,IPaymentService paymentService,IMapper mapper)
        {
            _userManager = userManager;
            _paymentService = paymentService;
            _mapper=mapper;

        }

        public async Task<Response<UserDto>> Handle(IsUserEnrolledCorrectlyRequest request, CancellationToken cancellationToken)
        {
            try {
                var response= new Response<UserDto>();
            var user = await _userManager.FindByIdAsync(request.userId);
        if (user ==null && user.AccountStripeId==null)
        {
            response= GeneralHelper.SetResponseResult<UserDto>(null,false,"an error occured");
        }
        else
        {
            var result = await _paymentService.istheUserRegisteredCompletely(user.AccountStripeId,cancellationToken);
            user.PaymentInformationsCompleted=result;
            var identityResult = await _userManager.UpdateAsync(user);

            if (!identityResult.Succeeded)
            {
                response= GeneralHelper.SetResponseResult<UserDto>(null,false,"an error occured while updating the user");
            }
            else
           { var model= new UserDto();
             _mapper.Map(user,model);
            response= GeneralHelper.SetResponseResult(model,result,"istheUserRegisteredCompletely !");
            }
        
        }
          return response;
           }
           catch(Exception e)
           {

              return GeneralHelper.SetResponseResult<UserDto>(null,false,"an error occured :"+e.Message);
           }
        }
    }
}
