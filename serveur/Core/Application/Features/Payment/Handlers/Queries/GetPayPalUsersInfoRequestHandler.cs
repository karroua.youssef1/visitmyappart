using Core.Application.DTOs;
using Core.Application.Features.Paypal.Requests.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Models;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Core.Application.Helpers;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetPayPalUsersInfoRequestHandler : IRequestHandler<GetPayPalUsersInfoRequest, Response<PayPalUserDTO>>
    {
        static readonly HttpClient client = new HttpClient();
        private readonly IConfiguration _configuration;
        public GetPayPalUsersInfoRequestHandler(IConfiguration configuration)
        {
            _configuration=configuration;
        }

        public async Task<Response<PayPalUserDTO>> Handle(GetPayPalUsersInfoRequest request, CancellationToken cancellationToken)
        { Response<PayPalUserDTO> result= new Response<PayPalUserDTO>();
           string acessTokenEndPoint =_configuration["AppSettings:Access_Token_EndPoint"].ToString();
           string UserInfosEndPoint = _configuration["AppSettings:User_Infos_EndPoint"].ToString();
          string access="";
          PayPalUserDTO infos=new PayPalUserDTO();
           HttpResponseMessage responseToken = await client.GetAsync(acessTokenEndPoint);
           if (responseToken.IsSuccessStatusCode)
            {
                access = await responseToken.Content.ReadAsAsync<string>();
            }
            if(access != null && access != string.Empty)
            {
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue($"Bearer",$"{access}");
           
             HttpResponseMessage responseUserInfos = await client.GetAsync(acessTokenEndPoint);
           if (responseUserInfos.IsSuccessStatusCode)
            {
                infos = await responseUserInfos.Content.ReadAsAsync<PayPalUserDTO>();
            }
            result = GeneralHelper.SetResponseResult(infos,true,"I got the infos");
            
            }
            else
            {
             result = GeneralHelper.SetResponseResult(infos,false,"access token is empty");

            }
            
                 return result;
        }

        
    }
}
