using Core.Application.Features.Payments.Requests.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System;
using Core.Application.Helpers;
using Core.Application.Models;
using Microsoft.AspNetCore.Identity;
using Domain.Users;

namespace Core.Application.Features.Offers.Handlers.Queries
{
    public class GetCreditCardInfoRequestHandler : IRequestHandler<GetCreditCardInfoRequest, Response<AddPaymentCard>>
    {
        private readonly UserManager<User> _userManager;

        private readonly IPaymentService _paymentService;

        public GetCreditCardInfoRequestHandler(UserManager<User> userManager,IPaymentService paymentService)
        {
            _userManager = userManager;
            _paymentService = paymentService;

        }

        public async Task<Response<AddPaymentCard>> Handle(GetCreditCardInfoRequest request, CancellationToken cancellationToken)
        {
            try {
            var user = await _userManager.FindByIdAsync(request.id.ToString());
  if (user ==null || user.CustomerStripeId==null)
  {
    return GeneralHelper.SetResponseResult<AddPaymentCard>(null,false,"an error occured");
  }
  else
  {
           var result = await _paymentService.GetCardInfosFromClientIdAsync(user.CustomerStripeId,cancellationToken);
            return GeneralHelper.SetResponseResult(result,true,"Card is here !");
  }
           }
           catch(Exception e)
           {

              return GeneralHelper.SetResponseResult<AddPaymentCard>(null,false,"an error occured :"+e.Message);
           }
        }
    }
}
