using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Features.Payments.Requests.Commands;
using Core.Application.Models;
using System;
using Core.Application.Helpers;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Core.Application.DTOs;

namespace Core.Application.Features.Payments.Handlers.Commands
{
    public class AddOrUpdateCustomerCommandHandler : IRequestHandler<AddOrUpdateCustomerCommand,Response<UserDto>>
    {
        private readonly IMapper _mapper;
        private readonly IPaymentService _paymentService;
        private readonly UserManager<User> _userManager;
        public AddOrUpdateCustomerCommandHandler(IMapper mapper,IPaymentService paymentService,UserManager<User> userManager)
        {
            _mapper = mapper;
            _paymentService=paymentService;
            _userManager=userManager;
        }

        public async Task<Response<UserDto>> Handle(AddOrUpdateCustomerCommand command, CancellationToken cancellationToken)
        {   
            
        try{
                var finalResult = new Response<UserDto>();
               var user = await _userManager.FindByIdAsync(command.addCustomer.Id);
                if(user==null)
             {
             
             finalResult= GeneralHelper.SetResponseResult<UserDto>(null,false,"An error occured");
             }
             else {
                var userdto=new UserDto();
               if(user?.CustomerStripeId==null)
                 {
                     var newCustomer  = await _paymentService.AddCustomerAsync(command.addCustomer, cancellationToken);
                     user.CustomerStripeId=newCustomer.CustomerId;
                    user.PaymentInformationsCompleted=true;
                    _mapper.Map(user,userdto);
                    var result = await _userManager.UpdateAsync(user);
                    
             if(result.Succeeded) finalResult= GeneralHelper.SetResponseResult(userdto,true,"Customer added successfuly");
             }
             else
             {    _mapper.Map(user,userdto);

                  await _paymentService.UpdateCustomerCardAsync(user.CustomerStripeId,command.addCustomer.CreditCard,cancellationToken);
                  finalResult= GeneralHelper.SetResponseResult(userdto,true,"Customer updated successfuly");

             }
             }
                
             
            return finalResult;
        }
        catch(Exception e)
        {
             return GeneralHelper.SetResponseResult<UserDto>(null,false,"An error occured :"+e.Message);

        }
                

        }
    }
}
