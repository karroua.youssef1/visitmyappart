using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Features.Payments.Requests.Commands;
using Core.Application.Models;
using System;
using Core.Application.Helpers;
using Microsoft.AspNetCore.Identity;
using Domain.Users;
using Microsoft.Extensions.Options;

namespace Core.Application.Features.Payments.Handlers.Commands
{
    public class AddAccountLinkCommandHandler : IRequestHandler<AddAccountLinkCommand,Response<string>>
    {
        private readonly IPaymentService _paymentService;
        private readonly UserManager<User> _userManager;
        private readonly ClientApp _clientApp;

        public AddAccountLinkCommandHandler(
             IOptions<ClientApp> clientApp,IPaymentService paymentService,UserManager<User> userManager)
        {
            _paymentService=paymentService;
            _userManager=userManager;
            _clientApp=clientApp.Value;
        }

        public async Task<Response<string>> Handle(AddAccountLinkCommand command, CancellationToken cancellationToken)
        {   

        try{
               var user = await _userManager.FindByIdAsync(command.userId);
            var redirectUrl=  await _paymentService.AddLinkAccountAsync(user.AccountStripeId,_clientApp,cancellationToken);
            return  GeneralHelper.SetResponseResult(redirectUrl,true,"Account link created succesfuly");
            
        }
        catch(Exception e)
        {
             return GeneralHelper.SetResponseResult("",false,"An error occured :"+e.Message);

        }
                

        }
    }
}
