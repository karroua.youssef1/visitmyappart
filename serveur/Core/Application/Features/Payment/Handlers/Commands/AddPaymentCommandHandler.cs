using Core.Application.Features.Payments.Requests.Commands;
using Core.Application.Helpers;
using Core.Application.Models;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Application.Features.Payments.Handlers.Commands
{
    public class AddPaymentCommandHandler : IRequestHandler<AddPaymentCommand, Response<bool>>
    {
        private readonly IPaymentService _paymentService;


        public AddPaymentCommandHandler(IPaymentService paymentService)
        {
            _paymentService=paymentService;
        }

        public async Task<Response<bool>> Handle(AddPaymentCommand command, CancellationToken cancellationToken)
        {    
            try{
             Payment createdPayment = await _paymentService.AddPaymentAsync(
                command.addPayment,
               cancellationToken);
                
              return GeneralHelper.SetResponseResult(true,true,"Payment added successfuly");
                }
            catch(Exception e)
            {
             return GeneralHelper.SetResponseResult(false,false,"An error occured :"+e.Message);

            }
    
        }
    }
}
