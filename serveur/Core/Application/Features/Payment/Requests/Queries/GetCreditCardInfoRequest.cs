using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Payments.Requests.Queries
{
    public class GetCreditCardInfoRequest : IRequest<Response<AddPaymentCard>>
    {
        public int id { get; set; }
    }
}
