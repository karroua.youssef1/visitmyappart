using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Paypal.Requests.Queries
{
    public class GetPayPalUsersInfoRequest : IRequest<Response<PayPalUserDTO>>
    {
        public string authorizationCode { get; set; }
    }
}
