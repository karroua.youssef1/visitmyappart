using Core.Application.DTOs;
using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Payments.Requests.Queries
{
    public class IsUserEnrolledCorrectlyRequest : IRequest<Response<UserDto>>
    {
        public string userId { get; set; }
    }
}
