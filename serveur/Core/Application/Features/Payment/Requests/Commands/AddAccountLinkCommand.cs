using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Payments.Requests.Commands
{
    public class AddAccountLinkCommand : IRequest<Response<string>>
    {
        public string userId { get; set; }

    }
}
