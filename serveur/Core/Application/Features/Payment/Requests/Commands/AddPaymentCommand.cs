using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Payments.Requests.Commands
{
    public class AddPaymentCommand : IRequest<Response<bool>>
    {
        public AddPayment addPayment { get; set; }

    }
}
