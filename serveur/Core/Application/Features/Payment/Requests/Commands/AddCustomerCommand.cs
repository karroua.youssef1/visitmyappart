using Core.Application.DTOs;
using Core.Application.Models;
using MediatR;

namespace Core.Application.Features.Payments.Requests.Commands
{
    public class AddOrUpdateCustomerCommand : IRequest<Response<UserDto>>
    {
        public AddCustomer addCustomer { get; set; }

    }
}
