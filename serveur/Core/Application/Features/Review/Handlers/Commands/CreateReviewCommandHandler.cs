using AutoMapper;
using Core.Application.Features.Reviews.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Models;
using System;
using Core.Application.Helpers;

namespace Core.Application.Features.Reviews.Handlers.Commands
{
    public class CreateReviewCommandHandler : IRequestHandler<CreateReviewCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;


        public CreateReviewCommandHandler(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<int>> Handle(CreateReviewCommand command, CancellationToken cancellationToken)
        {           
              Response<int> result =new Response<int> () ;

            try{
            Review myReview = new Review();
            _mapper.Map(command.review,myReview);
           var reviewAdded= _unitOfWork.ReviewRepository.Add(myReview);
            if(reviewAdded!=null)
            {
                await _unitOfWork.Save();
                result= GeneralHelper.SetResponseResult(reviewAdded.Id,true,"Review Created succefuly");
                
            }
           _unitOfWork.Dispose();
             return result;
             }
             catch(Exception e)
             
             {
               result= GeneralHelper.SetResponseResult(0,false,e.Message);
                return result;
             }
            
        }

       
    }
}
