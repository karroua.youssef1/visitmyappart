using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Reviews.Requests.Commands
{
    public class CreateReviewCommand : IRequest<Response<int>>
    {
        public ReviewDto review { get; set; }

    }
}
