using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Features.Bits.Requests.Queries;
using System.Collections.Generic;

namespace Core.Application.Features.Bits.Handlers.Queries
{
    public class GetBitsByUserIdRequestHandler : IRequestHandler<GetBitsByUserIdRequest, List<BitDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;


        public GetBitsByUserIdRequestHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {  
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<BitDto>> Handle(GetBitsByUserIdRequest command, CancellationToken cancellationToken)
        { 
            List<BitDto> result= new List<BitDto>();
              List<Bit> bits = await  _unitOfWork.BitRepository.GetBitsByUserId(command.id);
              _mapper.Map(bits,result);
           _unitOfWork.Dispose();
             return result;
        }
  
    }
}
