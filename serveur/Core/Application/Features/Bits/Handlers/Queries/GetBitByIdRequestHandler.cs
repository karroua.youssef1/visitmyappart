using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Features.Bits.Requests.Queries;

namespace Core.Application.Features.Bits.Handlers.Queries
{
    public class GetBitByIdRequestHandler : IRequestHandler<GetBitByIdRequest, BitDto>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;


        public GetBitByIdRequestHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        { 
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<BitDto> Handle(GetBitByIdRequest command, CancellationToken cancellationToken)
        { 
            BitDto result= new BitDto();
              Bit bit = await  _unitOfWork.BitRepository.Get(command.id);
              _mapper.Map(bit,result);
           _unitOfWork.Dispose();
             return result;
        }


    

       
    }
}
