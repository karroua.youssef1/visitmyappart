using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Features.Bits.Requests.Queries;

namespace Core.Application.Features.Bits.Handlers.Queries
{
    public class GetBitsByOfferRequestHandler : IRequestHandler<GetBitsByOfferRequest, List<BitDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;


        public GetBitsByOfferRequestHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {   
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<List<BitDto>> Handle(GetBitsByOfferRequest command, CancellationToken cancellationToken)
        { 
            List<BitDto> result= new List<BitDto>();
              List<Bit> bits = await  _unitOfWork.BitRepository.GetBitsByOfferId(command.idOffer);
              _mapper.Map(bits,result);
           _unitOfWork.Dispose();
             return result;
        }


    

       
    }
}
