using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Core.Application.Models;
using System;
using Domain.Enums;
using Core.Application.Helpers;

namespace Core.Application.Features.Bits.Handlers.Commands
{
    public class CreateBitCommandHandler : IRequestHandler<CreateBitCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public CreateBitCommandHandler(
             IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(CreateBitCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;

            try{
            Bit myBit = this.Map(command.bit);
            List<Bit> bitsOfOffer=  await _unitOfWork.BitRepository.GetBitsByOfferId(myBit.OffreId);
            if(bitsOfOffer.Exists(x=>x.VisitorId==myBit.VisitorId && x.State!=BitStateEnum.Rejected ))
            {
                result.isOk=false;
                result.ResponseModel=false;
                result.Message="you have already sent a bit for this offer";
                return result;
            }
            Domain.Notification notif = new Domain.Notification();
            var bit = _unitOfWork.BitRepository.Add(myBit);
            if (bit != null)
            {    notif=GeneralHelper.CreateNotification<BitDto>("Bit created","Your Bit has been created ",command.bit.VisitorId,command.bit.ClientId);
                if((_unitOfWork.NotificationRepository.Add(notif))!=null)
                {await _unitOfWork.Save();
                 result.isOk=true;
                result.ResponseModel=true;
                result.Message="Bit Created succefuly";
                }

            }
           _unitOfWork.Dispose();
             return result;
             }
             catch(Exception e)
             
             {
              result.isOk=false;
                result.ResponseModel=false;
                result.Message=e.Message;
                return result;
             }
            
        }


        private Bit Map( BitDto dto)
        {
            Bit resultat = new Bit();
            _mapper.Map(dto, resultat);
             resultat.Visitor=null;
             resultat.Offer=null;
             return resultat;
        }

       
    }
}
