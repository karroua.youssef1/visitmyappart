using AutoMapper;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Helpers;
using Domain;
using Core.Application.Models;
using System;

namespace Core.Application.Features.Bits.Handlers.Commands
{
    public class DeleteBitCommandHandler : IRequestHandler<DeleteBitCommand, Response<bool>>
    {
        private readonly IUnitOfWork _unitOfWork;


        public DeleteBitCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(DeleteBitCommand command, CancellationToken cancellationToken)
        {           

             try{
                Bit myBit =  await _unitOfWork.BitRepository.Get(command.Id);
                foreach(var comm in myBit.Comments)
                {

                    _unitOfWork.CommentRepository.Delete(comm);

                }
                 _unitOfWork.BitRepository.Delete(myBit);
            
                await _unitOfWork.Save();
                _unitOfWork.Dispose();
                return GeneralHelper.SetResponseResult(true,true,"Bit Deleted succefuly");
      
             }
             catch(Exception e)
             
             {
                return GeneralHelper.SetResponseResult(false,false,e.Message);
             }
            
        }


    

       
    }
}
