using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Core.Application.Helpers;
using Domain;
using Core.Application.Models;
using System;
using Domain.Users;
using Domain.Enums;

namespace Core.Application.Features.Bits.Handlers.Commands
{
    public class AcceptBitCommandHandler : IRequestHandler<AcceptBitCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IPaymentService _paymentService;


        public AcceptBitCommandHandler(
             IMapper mapper,UserManager<User> userManager,IUnitOfWork unitOfWork,IPaymentService paymentService)
        {
            _userManager = userManager;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _paymentService=paymentService;
        }

        public async Task<Response<bool>> Handle(AcceptBitCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;

            try{
                Bit myBit = this.Map(command.bit);
                 Offer offer = await _unitOfWork.OfferRepository.Get(myBit.OffreId);
                var client = await _userManager.FindByIdAsync(offer.ClientId.ToString());
                var payment= await _paymentService.AddPaymentAsync(new AddPayment{Amount=(long)offer.PriceOffered,Currency="CAD"
                ,CustomerId=offer.client.CustomerStripeId,Description="transfert",ReceiptEmail=offer.client.Email},cancellationToken);
                myBit.State=BitStateEnum.Accepted;
                _unitOfWork.BitRepository.Update(myBit);
            foreach (Comment item in myBit.Comments)
            {
                if(item.Id==0 && item.Content !="")
                {
                      await _unitOfWork.CommentRepository.Add(item);
                }
            }            
               offer.State=  OfferStateEnum.Sealed; 
               offer.ChargeId=payment.PaymentId;
                 _unitOfWork.OfferRepository.Update(offer);
              Domain.Notification notif = new Domain.Notification();
                 notif=GeneralHelper.CreateNotification<BitDto>("Bit accepted","Your Bit has been accepted",command.bit.ClientId,command.bit.VisitorId);
                if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
                 result.isOk=true;
                result.ResponseModel=true;
                result.Message="Bit Accepted succefuly";
                }

            
           _unitOfWork.Dispose();
           

             return result;
             }
             catch (Exception)
            {
              result.isOk=false;
                result.ResponseModel=false;
                result.Message="On error has occured , please try again later";
                return result;
             }
            
        }


        private Domain.Notification CreateNotification(BitDto bit)

        {
            Domain.Notification resultat = new Domain.Notification
            {
                Titre = "Your Bit has been accepted",
                Message = "Hey,Your Bit has been accepted",
                SenderId = bit.VisitorId,
                ReceiverId = bit.ClientId,
                isOpen = false
            };

            return resultat;
        }

        private Bit Map( BitDto dto)
        {
            Bit resultat = new Bit();
             _mapper.Map(dto, resultat);
             resultat.Visitor=null;
             resultat.Offer=null;
             return resultat;
        }

       
    }
}
