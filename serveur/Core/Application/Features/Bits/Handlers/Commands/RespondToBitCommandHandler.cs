using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Helpers;
using Domain;
using Core.Application.Models;
using System;

namespace Core.Application.Features.Bits.Handlers.Commands
{
    public class RespondToBitCommandHandler : IRequestHandler<RespondToBitCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public RespondToBitCommandHandler(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(RespondToBitCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;

            try{
                Bit myBit = Map(command.bit);
                _unitOfWork.BitRepository.Update(myBit);
            foreach (Comment item in myBit.Comments)
            {
                if(item.Id==0 && item.Content !="")
                {
                      await _unitOfWork.CommentRepository.Add(item);
                }
            }
            Domain.Notification notif = new Domain.Notification();
                 notif=GeneralHelper.CreateNotification<BitDto>("Bit responded to","Your Bit has been responded to",command.bit.ClientId,command.bit.VisitorId);
                if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
                 result.isOk=true;
                result.ResponseModel=true;
                result.Message="Bit Responded to succefuly";
                }    
           _unitOfWork.Dispose();
             return result;
             }
             catch(Exception)
             
             {
              result.isOk=false;
                result.ResponseModel=false;
                result.Message="On error has occured , please try again later";
                return result;
             }
            
        }

        private Bit Map( BitDto dto)
        {
            Bit resultat = new Bit();
             _mapper.Map(dto, resultat);
             resultat.Visitor=null;
             resultat.Offer=null;
             return resultat;
        }

       
    }
}
