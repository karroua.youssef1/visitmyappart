using AutoMapper;
using Core.Application.DTOs;
using Core.Application.Features.Bits.Requests.Commands;
using Core.Application.Contracts.Persistance;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Core.Application.Helpers;
using Domain;
using Core.Application.Models;
using System;
using Domain.Enums;

namespace Core.Application.Features.Bits.Handlers.Commands
{
    public class RejectBitCommandHandler : IRequestHandler<RejectBitCommand, Response<bool>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;


        public RejectBitCommandHandler(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<bool>> Handle(RejectBitCommand command, CancellationToken cancellationToken)
        {           
              Response<bool> result =new Response<bool> () ;

            try{
                Bit myBit = this.Map(command.bit);
                myBit.State=BitStateEnum.Rejected;
             _unitOfWork.BitRepository.Update(myBit);
            foreach (Comment item in myBit.Comments)
            {
                if(item.Id==0 && item.Content !="")
                {
                      await _unitOfWork.CommentRepository.Add(item);
                }
            }
            Domain.Notification notif = new Domain.Notification();
                 notif=GeneralHelper.CreateNotification<BitDto>("Bit rejected","Your Bit has been rejected",command.bit.ClientId,command.bit.VisitorId);
                if(_unitOfWork.NotificationRepository.Add(notif)!=null)
                {await _unitOfWork.Save();
                 result.isOk=true;
                result.ResponseModel=true;
                result.Message="Bit Rejected succefuly";
                }

            
            _unitOfWork.Dispose();
             return result;
             }
             catch(Exception)
             
             {
              result.isOk=false;
                result.ResponseModel=false;
                result.Message="On error has occured , please try again later";
                return result;
             }
            
        }


        private Bit Map( BitDto dto)
        {
            Bit resultat = new Bit();
             _mapper.Map(dto, resultat);
             resultat.Visitor=null;
             resultat.Offer=null;
             return resultat;
        }

       
    }
}
