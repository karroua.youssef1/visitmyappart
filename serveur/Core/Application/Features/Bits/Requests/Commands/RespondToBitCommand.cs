using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Bits.Requests.Commands
{
    public class RespondToBitCommand : IRequest<Response<bool>>
    {
        public BitDto bit { get; set; }

    }
}
