using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Bits.Requests.Commands
{
    public class DeleteBitCommand : IRequest<Response<bool>>
    {
        public int Id { get; set; }

    }
}
