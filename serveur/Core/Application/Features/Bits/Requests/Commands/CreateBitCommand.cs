using Core.Application.DTOs;
using MediatR;
using Core.Application.Models;

namespace Core.Application.Features.Bits.Requests.Commands
{
    public class CreateBitCommand : IRequest<Response<bool>>
    {
        public BitDto bit { get; set; }

    }
}
