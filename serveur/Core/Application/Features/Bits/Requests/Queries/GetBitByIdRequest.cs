using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Bits.Requests.Queries
{
    public class GetBitByIdRequest : IRequest<BitDto>
    {
        public int id { get; set; }

    }
}
