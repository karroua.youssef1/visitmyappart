using System.Collections.Generic;
using Core.Application.DTOs;
using MediatR;

namespace Core.Application.Features.Bits.Requests.Queries
{
    public class GetBitsByUserIdRequest : IRequest<List<BitDto>>
    {
        public int id { get; set; }

    }
}
