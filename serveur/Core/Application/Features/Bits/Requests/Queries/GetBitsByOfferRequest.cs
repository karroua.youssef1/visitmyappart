using Core.Application.DTOs;
using MediatR;
using System.Collections.Generic;

namespace Core.Application.Features.Bits.Requests.Queries
{
    public class GetBitsByOfferRequest : IRequest<List<BitDto>>
    {
        public int idOffer { get; set; }

    }
}
