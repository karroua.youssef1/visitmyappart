
namespace Core.Application.Models
{
    public class Response<T> 
    {
       public string Message  {get;set;}
       public bool isOk {get;set;}
       public T ResponseModel  {get;set;}
    }
}
