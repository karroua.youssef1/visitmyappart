
public class Payment
{

        public Payment( string customerId,string receiptEmail,string description,string currency,long amount,string paymentId)
        {
                CustomerId=customerId;
                Amount=amount;
                ReceiptEmail=receiptEmail;
                Currency=currency;
                PaymentId=paymentId;
                Description=description;
        }
        public string CustomerId {get;set;}
        public string ReceiptEmail {get;set;}
        public string Description {get;set;}
        public string Currency {get;set;}
        public long Amount {get;set;}
        public string PaymentId {get;set;}
}