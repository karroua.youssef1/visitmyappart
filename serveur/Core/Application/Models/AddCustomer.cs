using Newtonsoft.Json;

[JsonObject(Title="PaymentInfos")]
public class AddCustomer {
	public string Id {get;set;}
    public string Email {get;set;}
	public string Name {get;set;}

	[JsonProperty("CreditCard")]

	public AddPaymentCard CreditCard {get;set;}
}