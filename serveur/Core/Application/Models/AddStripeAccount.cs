using Newtonsoft.Json;

[JsonObject(Title="PaymentInfos")]
public class AddStripeAccount {
	public string Type {get;set;}
    public string Email {get;set;}
	public string Country {get;set;}
    public string FirstName {get;set;}

    public string LastName {get;set;}

    public string Gender {get;set;}


}