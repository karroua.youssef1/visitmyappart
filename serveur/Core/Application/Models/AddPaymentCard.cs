
public class AddPaymentCard {
   public string Name {get;set;}
   private string expirationYear;
   private string expirationMonth;

	public string CardNumber {get;set;}
	public string ExpirationYear
	 {
    get { return expirationYear; }
    set {
		if(value.Length==4)
		 expirationYear = value.Substring(2);
		 else
		 {
          expirationYear = value;
		 }		 
		 }
     }
	public string ExpirationMonth 
	 {
    get { return expirationMonth; }
    set {
		if(value.Length==1)
		 expirationMonth = "0"+value;
		 else
		 {
			expirationMonth = value;
		 }		 
		 }
     }
	public string Cvc {get;set;}

	
}