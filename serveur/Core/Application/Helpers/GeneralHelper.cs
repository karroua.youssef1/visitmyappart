using Core.Application.DTOs;
using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using  System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using Core.Application.Models;




namespace Core.Application.Helpers
{
    public static class GeneralHelper
    {

        public static Domain.Notification CreateNotification<T>(string title,string message,int SenderId,int ReceiverId) 
        {
            Domain.Notification resultat = new Domain.Notification
            {
                Titre = title,
                Message = message,
                SenderId = SenderId,
                ReceiverId = ReceiverId,
                isOpen = false
            };
            return resultat;

        }

         public static ProfilPictureDto mapFileFormtoProfilPictureDto(List<IFormFile> files,int idUser)  
        {
            if(files.Count==0)
            {
                return null;
            }
            ProfilPictureDto result= new ProfilPictureDto();
            IFormFile file =files[0];
                    if (file.Length > 0)
                            {
                                //Getting FileName
                                result.Name = Path.GetFileName(file.FileName);
                                //Getting file Extension
                                result.FileExtension = Path.GetExtension(file.FileName);
                                // concatenating  FileName + FileExtension
                            using (var target = new MemoryStream())
                                {
                                    file.CopyTo(target);
                                    result.Bytes = target.ToArray();
                                }
                                result.Size=file.Length;
                                result.UserId=idUser;
                                result.Type=file.ContentType;

                            }
                return result;

        }

   public static Response<T> SetResponseResult<T>(T responseModel, bool isOk,string message)
   {

            Response<T> result = new Response<T>
            {
                isOk = isOk,
                ResponseModel = responseModel,
                Message = message
            };
            return result;

   }

   public static string GetToken(List<Claim> claims,int id)
   {

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("superSecretKey@345");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
           string tokenString= tokenHandler.WriteToken(token);
    // var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
    //         var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
    //         var tokeOptions = new JwtSecurityToken(
    //             issuer: "https://localhost:5001",
    //             audience: "https://localhost:5001",
    //             claims: claims,
    //             expires: DateTime.Now.AddMinutes(5),
    //             signingCredentials: signinCredentials
    //         );
           // string tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
            return tokenString;
   }

         public static List<ReportFileDto> mapFilesFormtoReportFilesDto(List<IFormFile> files)  
        {
            if(files.Count==0)
            {
                return null;
            }
            List<ReportFileDto> result= new List<ReportFileDto>();
            foreach (IFormFile file in files)
            {ReportFileDto f = new ReportFileDto();
                if (file.Length > 0)
                            {
                                //Getting FileName
                                f.Name = Path.GetFileName(file.FileName);
                                //Getting file Extension
                                f.FileExtension = Path.GetExtension(file.FileName);
                                // concatenating  FileName + FileExtension
                            using (var target = new MemoryStream())
                                {
                                    file.CopyTo(target);
                                    f.Bytes = target.ToArray();
                                }
                                f.Size=file.Length;
                                f.Type=file.ContentType;
                                result.Add(f);

                            }
            }
                    
                return result;

        }


            public static T  DeserializeToModel<T>(string form ) where T :  class
            { 
                 T result = 
                JsonSerializer.Deserialize<T>(form);
                return result;
            }
       
    }
}
