using AutoMapper;
using Core.Application.DTOs;
using Domain;
using Domain.Users;
using Domain.Files;

namespace Core.Application.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Offer, OfferDto>()
            .ForMember(dest => dest.Adress, opt => opt.MapFrom(src => src.Adress))
            .ForMember(dest => dest.Bits, opt => opt.Ignore());


            CreateMap<OfferDto, Offer>()
            .ForMember(dest => dest.Adress, opt => opt.MapFrom(src => src.Adress));

            CreateMap<Adress, AdressDto>()
            .ReverseMap();
             CreateMap<City, CityDto>()
            .ReverseMap();
            CreateMap<User, UserDto>().ReverseMap();
             CreateMap<Comment, CommentDto>()
            .ForMember(dest => dest.ReceiverName, opt => opt.MapFrom(src => src.Receiver.Name));

            CreateMap<CommentDto, Comment>();

             CreateMap<Bit, BitDto>()
             .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.Visitor.Name))
             .ForMember(dest => dest.ProfilPic, opt => opt.MapFrom(src => src.Visitor.profilPicture))
             .ForMember(dest => dest.ClientId, opt => opt.MapFrom(src => src.Offer.ClientId))
             .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comments))
             .ReverseMap();

              CreateMap<Review, ReviewDto>().ReverseMap();
            CreateMap<ReportFileDto, ReportFile>().ReverseMap();
            CreateMap<Report, ReportDto>()
             .ForMember(dest => dest.content, opt => opt.MapFrom(src => src.Content))
             .ForMember(dest => dest.senderId, opt => opt.MapFrom(src => src.SenderId))
             .ForMember(dest => dest.bitId, opt => opt.MapFrom(src => src.BitId))
             .ForMember(dest => dest.state, opt => opt.MapFrom(src => src.State))
             .ForMember(dest => dest.reportFilesExist, opt => opt.MapFrom(src => src.FileAttachments.Count > 0 ? true : false))
             .ForMember(dest => dest.FileAttachments, opt => opt.Ignore());


             CreateMap<ReportDto, Report>()
             .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.content))
             .ForMember(dest => dest.SenderId, opt => opt.MapFrom(src => src.senderId))
             .ForMember(dest => dest.BitId, opt => opt.MapFrom(src => src.bitId))
             .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.state));

               CreateMap<Region, RegionDto>().ReverseMap();
              CreateMap<Domain.Notification, NotificationDto>().ReverseMap();
            CreateMap<FileDto, File>().ReverseMap();
            CreateMap<ProfilPictureDto, ProfilPicture>().ReverseMap();
        }
    }
}
