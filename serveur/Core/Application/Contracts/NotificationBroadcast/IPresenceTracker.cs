using System.Threading.Tasks;  
using System.Collections.Generic;
  
namespace Core.Application.Contracts.Notifications  
{  
    public interface IPresenceTracker  
    {  
         Task<bool> UserConnected(int idUser, string connectionId);
         Task<bool> UserDisconnected(int idUser, string connectionId);
         Task<int[]> GetOnlineUsers();
          Task<List<string>> GetConnectionsForUser(int idUser);

    }  
}  