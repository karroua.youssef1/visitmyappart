using System.Threading.Tasks;  
using System.Collections.Generic;

namespace Core.Application.Contracts.Notifications  
{  
    public interface IHubClient  
    {  
         Task BroadcastNotification(Domain.Notification notif); 
         Task BroadcastNotificationToUser(Domain.Notification notification) ;
         Task BroadcastNotificationToSpecificUsers(Domain.Notification notif,List<int> userIds);
           Task OnConnectedAsync(int id);
         Task OnDisconnectedAsync(int id);
    }  
}  