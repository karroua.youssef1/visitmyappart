using Core.Application.Contracts.Persistance.Common;
using Domain.Files;


namespace Core.Application.Contracts.Persistance
{
    public interface IReportFileRepository : IGenericRepository<ReportFile>
    {
          



    }
}
