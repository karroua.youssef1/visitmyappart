using Core.Application.Contracts.Persistance.Common;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Files;

namespace Core.Application.Contracts.Persistance
{
    public interface IReportRepository : IGenericRepository<Report>
    {
          
       Task<List<ReportFile>> GetReportFilesByReportId(int reportId);



    }
}
