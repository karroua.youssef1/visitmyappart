using Core.Application.Contracts.Persistance.Common;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Contracts.Persistance
{
    public interface IBitRepository : IGenericRepository<Bit>
    {
          Task<List<Bit>> GetBitsByOfferId(int offerId);
          Task<Bit> GetBitById(int id);
          Task<List<Bit>> GetBitsByUserId(int id);
    }
}
