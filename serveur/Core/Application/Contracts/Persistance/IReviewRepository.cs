using Core.Application.Contracts.Persistance.Common;
using Domain;

namespace Core.Application.Contracts.Persistance
{
    public interface IReviewRepository : IGenericRepository<Review>
    {



    }
}
