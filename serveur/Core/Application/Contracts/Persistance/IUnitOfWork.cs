using System;
using System.Threading.Tasks;

namespace Core.Application.Contracts.Persistance
{
    public interface IUnitOfWork : IDisposable
    {
        IClientRepository ClientRepository { get; }
        IVisitorRepository VisitorRepository { get; }
        IOfferRepository OfferRepository { get; }
        IBitRepository BitRepository {get;}
        IReviewRepository ReviewRepository {get;}
        IRegionRepository RegionRepository {get;}
        INotificationRepository NotificationRepository {get;}
        ICommentRepository CommentRepository {get;}
        IReportRepository ReportRepository {get;}
        IReportFileRepository ReportFileRepository {get;}
        IAdressRepository AdressRepository {get;}
        Task Save();
    }
}
