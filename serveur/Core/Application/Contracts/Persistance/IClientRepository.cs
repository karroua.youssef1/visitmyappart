using Core.Application.Contracts.Persistance.Common;
using Domain.Users;

namespace Core.Application.Contracts.Persistance
{
    public interface IClientRepository : IGenericRepository<User>
    {
        
    }
}
