using Core.Application.Contracts.Persistance.Common;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Contracts.Persistance
{
    public interface INotificationRepository : IGenericRepository<Notification>
    {
        Task<List<Notification>> GetNotificationsById(int userId);
         void UpdateNotificationState(int id);
         Task UpdateNotificationsStateToRead(int idUser);
        
    }
}
