using Core.Application.Contracts.Persistance.Common;
using Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Application.Contracts.Persistance
{
    public interface IOfferRepository : IGenericRepository<Offer>
    {
      Task<List<Offer>> GetOffers();
      Task<List<Offer>> GetClientOffers(int clientId);
      Task<Offer> GetOfferById(int id);
      Task<List<Offer>> GetAnumberOfOffersOrderByDate(int number);





    }
}
