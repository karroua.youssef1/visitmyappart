using System.Threading;
using System.Threading.Tasks;

public interface IPaymentService {
Task<PaymentCustomer> AddCustomerAsync(AddCustomer customer, CancellationToken ct);
Task<AddPaymentCard> UpdateCustomerCardAsync(string clientId, AddPaymentCard newCardInfos, CancellationToken ct);

Task<Payment> AddPaymentAsync(AddPayment payment, CancellationToken ct);
Task<AddPaymentCard> GetCardInfosFromClientIdAsync(string clientId, CancellationToken ct);
Task<bool> PayOutMoney(string chargeId,string destionationAccountId,string amount,string description, CancellationToken ct);

Task<string> AddAccountAsync(AddStripeAccount account, CancellationToken ct);
Task<string> AddLinkAccountAsync(string accountId,ClientApp _clientApp, CancellationToken ct);
Task<bool> istheUserRegisteredCompletely(string accountId, CancellationToken ct);



}