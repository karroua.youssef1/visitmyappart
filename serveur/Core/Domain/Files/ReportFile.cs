using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Files
{
    public  class ReportFile : File
    {
    public int ReportId {get;set;}
    [ForeignKey("ReportId")]
     public virtual Report Report {get;set;}
     
    }
}
