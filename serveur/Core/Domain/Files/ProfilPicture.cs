using Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Files
{
    public  class ProfilPicture : File
    {
    public int UserId {get;set;}
    [ForeignKey("UserId")]
     public virtual User User {get;set;}
     
    }
}
