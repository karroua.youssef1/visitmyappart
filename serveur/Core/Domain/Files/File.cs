using Domain.Common;
namespace Domain.Files
{
    public  class File : Base
    {
      public byte[] Bytes { get; set; }
     public string Name { get; set; }
     public string FileExtension { get; set; }
     public decimal Size { get; set; }
     public string Type {get;set;}
    }
}
