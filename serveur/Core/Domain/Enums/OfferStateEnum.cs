
namespace Domain.Enums
{
public enum OfferStateEnum {
    Open,
    Sealed,
    Done
}
}