
namespace Domain.Enums
{
public enum CommentTypeEnum {
    Neutral,
    OfferAccepted,
    OfferRejected,
    ReportRejected,
    ReportAccepted
}
}