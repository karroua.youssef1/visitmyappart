using System.Collections.Generic;
using Domain.Common;
using Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Enums;
namespace Domain
{
    public  class Bit : Base
    {
        public Bit()
        {
          Comments=new List<Comment>();
        }
        public int OffreId {get;set;}
        [ForeignKey("OffreId")]
        public virtual Offer Offer {get;set;}
        public int VisitorId {get;set;}
         [ForeignKey("VisitorId")]
        public virtual User Visitor {get;set;}
        public virtual List<Comment> Comments {get;set;}
        public BitStateEnum State {get;set;}
        public virtual Report Report {get;set;}
        
        

        
    }
}
