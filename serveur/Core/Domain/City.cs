using Domain.Common;

namespace Domain
{
    public  class City : Base
    {
        public double Lat {get;set;}
        public double Lng {get;set;}
        public string Name {get;set;}

    }
}
