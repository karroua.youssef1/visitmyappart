using Domain.Common;

namespace Domain
{
    public  class Region : Base
    {
        public double x {get;set;}

        public double y {get;set;}

        public double radius {get;set;}
        
    }
}
