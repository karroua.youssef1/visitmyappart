using System.Collections.Generic;
using Domain.Common;
using Domain.Files;
using Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public  class Report : Base
    {
        public string Content {get;set;}
        public int SenderId {get;set;}
        public virtual List<ReportFile> FileAttachments {get;set;}
        [ForeignKey("BitId")]
        public virtual Bit Bit {get;set;}
        public int BitId {get;set;}
        public ReportStateEnum State {get;set;}
           
    }
}
