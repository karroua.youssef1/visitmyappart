using Domain.Common;
using Domain.Users;
using Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;
namespace Domain
{
    public  class Comment : Base
    {
        public string Content {get;set;}
        public int SenderId {get;set;}

        [ForeignKey("SenderId")]
        public virtual User Sender {get;set;}
        public int ReceiverId {get;set;}
        [ForeignKey("ReceiverId")]
        public virtual User Receiver {get;set;}
        public CommentTypeEnum type {get;set;}
        
        
    }
}
