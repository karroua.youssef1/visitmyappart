using Microsoft.AspNetCore.Identity;

namespace Domain.Users
{
    public class CustomIdentityRole : IdentityRole<int>
    {
    public CustomIdentityRole() : base()   {}

    public CustomIdentityRole(string roleName) : base(roleName){}

    }
}
