namespace Domain.Users
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string Visitor = "Visitor";
        public const string Client = "Client";

    }
}