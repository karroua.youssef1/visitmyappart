using System;
using System.Collections.Generic;
using Domain.Files;
using Microsoft.AspNetCore.Identity;

namespace Domain.Users
{
    public class User : IdentityUser<int>
    {

        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }

        public string Name { 
            get {return this.FirstName + " " + this.LastName;}
      }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public virtual City City { get; set; }
        public string Country { get; set; }
        public string Role {get;set;}
        public virtual List<Review> ClientReviews {get;set;}
        public virtual List<Review> VisitorReviews {get;set;}
        public virtual List<Offer> Clientoffers {get;set;}
        public virtual List<Offer> Visitoroffers {get;set;}
         public string  Description { get; set; }
        public virtual Region PrefferedRegion {get;set;}
        public virtual ProfilPicture profilPicture {get;set;}
        public string CustomerStripeId {get;set;}
        public  bool VisitingPreferencesCompleted {get;set;}
        public string AccountStripeId {get;set;}
  
        public  bool PaymentInformationsCompleted {get;set;}


    }
}
