using Domain.Common;
using Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public  class Notification : Base
    {
        public string Titre {get;set;}

        public string Message {get;set;}
        public int SenderId {get;set;}

        [ForeignKey("SenderId")]
        public virtual User Sender {get;set;}
        public bool isSendAllUsers {get;set;}

        public int? ReceiverId {get;set;}

        [ForeignKey("ReceiverId")]
        public virtual User Receiver {get;set;}
        
        public string link {get;set;}

        public bool isOpen {get;set;}
        
    }
}
