using System;
using System.Collections.Generic;
using Domain.Common;
using Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Enums;

namespace Domain
{
    public  class Offer : Base
    {
        public string Titre {get;set;}

        public virtual Adress Adress {get;set;}
        public double x { get; set; }
        public double y {get;set;}
        public string Description {get;set;}
        public DateTime  LimitDate  {get;set;}
        public int PriceOffered {get;set;}
        public int ClientId {get;set;}

        [ForeignKey("ClientId")]
        [InverseProperty("Clientoffers")]
        public virtual User client {get;set;}
        public int? VisitorId {get;set;}
         [ForeignKey("VisitorId")]
        [InverseProperty("Visitoroffers")]
        public virtual User visitor {get;set;}
        public string link {get;set;}

        public bool isDeal {get;set;}
        
        public virtual List<Bit> Bits {get;set;}    
         public OfferStateEnum State {get;set;}

         public string ChargeId {get;set;}

        
    }
}
