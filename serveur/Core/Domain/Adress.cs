using Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public  class Adress : Base
    {

        public int OffreId {get;set;}
        [ForeignKey("OffreId")]
        public virtual Offer Offer {get;set;}
        public double x {get;set;}
        public double y {get;set;}
        public string AdressContent {get;set;}
        public string MapsUrl {get;set;}

    }
}
