using Domain.Common;
using Domain.Users;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public  class Review : Base
    {
        public float Rating {get;set;}

        public string Comment {get;set;}

        [ForeignKey("ClientId")]
        [InverseProperty("ClientReviews")]
        public virtual User client {get;set;}
        
        public int ClientId {get;set;}
       [ForeignKey("VisitorId")]
        [InverseProperty("VisitorReviews")]

        public virtual User Visitor {get;set;}

        public int VisitorId {get;set;}

        
    }
}
